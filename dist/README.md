## README

# Procedure for Installing from the files in the distribuition (dist) directory

## Server Configuration

The GLASS.conf should be modified with your local node credentials and inserted in the config folder of the glassfish domain.

Example: copy to this directory
cp GLASS.conf /opt/epos/payara5/glassfish/domains/domain1/config

If you are simply updating the application this step can be omitted.

## Application Configuration

Note: Deploying the application will remove existing versions.

# Pre-Deployment Configuration

1 - Update the swagger.json so that the server addresses match you local installation.

2 - The .war file should be updated once the swagger.json are modified accordingly to your preferences. 

The command is :

jar -uvf GlassFramework.war swagger.json

4 - The war file may then be deployed on the server using the CLI (or server admin GUI if available). 

5 To deploy using simple CLI tool just copy the .war file to the GlassFish/Payara domain autodeploy folder

example.  cp GlassFramwork.war  /opt/epos/payara5/glassfish/domains/domain1/autodeploy/

# Post Deployment Configuration

Alternatively you can simply install the GlassFramework.war file and then edit the swagger.json file that can be found in the
application directory after the web server has deployed the application.

Example: Deploy using autodeploy the edit the file (swagger.json) that can be found here
/opt/epos/payara5/glassfish/domains/domain1/applications/GlassFramework/


# TroubleShooting Installation

In case of problems inspect the server logs.

The cache file of the application may cause problems and it may help to remove it and rebuild from scratch.

If you are updating from an old version (before 1.5.x) it is advisable to stop the application after installation, remove the Glass cache file and restart the application.

For Payara/GlassFish the procedure is

asadmin stop-domain domain1
rm /opt/epos/payara5/glassfish/domains/domain1/config/cache.json
asadmin start-domain domain1

The cache file will then be rebuilt. Progress may be seen in the developer information page
https://...../GlassFramework/version.html

# Compatibility

- Compatible with version gnss-europe-v1.3.0 of the Database (https://gitlab.com/gpseurope/database)
