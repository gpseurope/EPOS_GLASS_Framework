/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geometry;

/**
 *
 * @author crocker
 * Class for line methods that may or may not be needed
 * 
 */
public class Line {

    /*
     * ANGLE 2D
     * Return the angle between two vectors on a plane.
     * The angle is from vector 1 to vector 2, positive anticlockwise
     * The result is between -pi -> pi
     */
    public static double angle2D(double x1, double y1, double x2, double y2) {
        double dtheta;
        double theta1;
        double theta2;
        double TWOPI = Math.PI * 2;
        theta1 = Math.atan2(y1, x1);
        theta2 = Math.atan2(y2, x2);
        dtheta = theta2 - theta1;
        while (dtheta > Math.PI) {
            dtheta -= TWOPI;
        }
        while (dtheta < -Math.PI) {
            dtheta += TWOPI;
        }
        return dtheta;
    }
    
}
