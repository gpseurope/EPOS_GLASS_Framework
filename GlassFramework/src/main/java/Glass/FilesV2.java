package Glass;

import Configuration.SiteConfig;
import CustomClasses.*;
import org.json.simple.JSONArray;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@SuppressWarnings("Duplicates")
@Path("files")
public class FilesV2 {

    @EJB
    private
    SiteConfig siteConfig = lookupSiteConfigBean();

    // =========================================================================
    //  FIELDS
    // =========================================================================
    @Context
    private UriInfo context;

    public FilesV2() {
        /* Gives access to network and connectivity */
        //String myIP = siteConfig.getLocalIpValue();
        //String DBConnectionString = siteConfig.getDBConnectionString();
    }

    @SuppressWarnings("Duplicates")
    private SiteConfig lookupSiteConfigBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    /**
     * <b>GET FILES V2</b> - Returns the files(s) that match the marker.
     *
     * <p>
     * <b>object_instance - </b> the value of the criteria <i>(e.g. for country criteria may be CASC)</i>
     * </p>
     *
     *
     * <p>
     * <b>output_format - </b> the format of the output
     * <ul>
     *     <li>csv</li>
     *     <li>json</li>
     *     <li>script<b>ª</b></li>
     *     <li>xml</li>
     * </ul>
     *
     * <b>ª</b> - this format is not valid for all object_type
     * </p>
     *
     * @param format   xml, json, csv or script
     * @param instance CASC, ABBS, AMAR00PRT, ..
     * @return Files
     * @throws IOException
     */
    @Produces({MediaType.TEXT_PLAIN + ";charset=utf-8", MediaType.APPLICATION_JSON + ";charset=utf-8", MediaType.APPLICATION_XML + ";charset=utf-8", MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8"})
    @Path("station-marker/{instance}/{format}")
    @GET
    public Response getFilesbyMarker(
            @PathParam("instance") String instance,
            @PathParam("format") String format,
            @DefaultValue("0") @QueryParam("filtervalidatedfiles") String filtervalidatedfiles,
            @DefaultValue("") @QueryParam("page") String page,
            @DefaultValue("") @QueryParam("perpage") String perPage
    ) {
        Stations stations = new Stations();
        int pageNumber = 0, perPageNumber = 50;
        Tuple2<Integer, Integer> validate;
        int bad_files_int;
        try {
            bad_files_int = Validator.ValidateBadFileExcluder(filtervalidatedfiles);
            validate = Validator.ValidatePageEntries(page.trim(), perPage.trim());
            pageNumber = validate.getFirst();
            perPageNumber = validate.getSecond();
        } catch (ValidatorException e) {
            return Response.status(e.responseStatus).entity(e.getMessage()).build();
        }

        if (instance == null)
            return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();

        Tuple2<Integer, String> r = new Tuple2<>();
        String media_type = MediaType.TEXT_PLAIN;
        format = format.trim().toLowerCase();
        switch (format) {
            case "csv":
                media_type = GlassConstants.TEXT_CSV;
                break;
            case "xml":
                media_type = MediaType.APPLICATION_XML;
                break;
            case "json":
                media_type = MediaType.APPLICATION_JSON;
                break;
        }
        if (format.equalsIgnoreCase("url_list")) format = "script";
        r = stations.getFilesMarker(instance, format, pageNumber, perPageNumber, bad_files_int);
        return Response.ok(r.getSecond())
                .header("Content-Type", media_type)
                .header("x-content-type-options", "nosniff")
                .header("x-frame-options", "SAMEORIGIN")
                .header("x-pagination-current-page", pageNumber)
                .header("x-pagination-per-page", perPage)
                .header("x-pagination-total-count", r.getFirst())
                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                .build();

    }

    @Produces({MediaType.TEXT_PLAIN + ";charset=utf-8", MediaType.APPLICATION_JSON + ";charset=utf-8", MediaType.APPLICATION_XML + ";charset=utf-8", MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8"})
    @Path("md5/{instance}/{format}")
    @GET
    public Response getFilesbyMD5(
            @PathParam("instance") String instance,
            @PathParam("format") String format,
            @DefaultValue("0") @QueryParam("filtervalidatedfiles") String filtervalidatedfiles,
            @DefaultValue("") @QueryParam("page") String page,
            @DefaultValue("") @QueryParam("perpage") String perPage
    ) {
        Stations stations = new Stations();
        String date_from = "", date_to = "";
        int pageNumber = 0, perPageNumber = 50;
        Tuple2<Integer, Integer> validate;
        int bad_files_int;
        int total_pages = 0;

        try {
            bad_files_int = Validator.ValidateBadFileExcluder(filtervalidatedfiles);
            validate = Validator.ValidatePageEntries(page.trim(), perPage.trim());
            pageNumber = validate.getFirst();
            perPageNumber = validate.getSecond();
        } catch (ValidatorException e) {
            return Response.status(e.responseStatus).entity(e.getMessage()).build();
        }

        if (instance == null)
            return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();

        Tuple2<Integer, String> r = new Tuple2<>();
        String media_type = MediaType.TEXT_PLAIN;
        format = format.trim().toLowerCase();
        switch (format) {
            case "csv":
                media_type = GlassConstants.TEXT_CSV;
                break;
            case "xml":
                media_type = MediaType.APPLICATION_XML;
                break;
            case "json":
                media_type = MediaType.APPLICATION_JSON;
                break;
        }

        r = stations.getFilesMD5(instance, format, pageNumber, perPageNumber, bad_files_int);
        return Response.ok(r.getSecond())
                .header("Content-Type", media_type)
                .header("x-content-type-options", "nosniff")
                .header("x-frame-options", "SAMEORIGIN")
                .header("x-pagination-current-page", pageNumber)
                .header("x-pagination-per-page", perPage)
                .header("x-pagination-total-count", r.getFirst())
                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                .build();

    }
    @Produces({MediaType.TEXT_PLAIN + ";charset=utf-8", MediaType.APPLICATION_JSON + ";charset=utf-8", MediaType.APPLICATION_XML + ";charset=utf-8", MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8"})
    @Path("name/{instance}/{format}")
    @GET
    public Response getFilesbyFileName(
            @PathParam("instance") String instance,
            @PathParam("format") String format,
            @DefaultValue("0") @QueryParam("filtervalidatedfiles") String filtervalidatedfiles,
            @DefaultValue("") @QueryParam("page") String page,
            @DefaultValue("") @QueryParam("perpage") String perPage,
            @DefaultValue("") @QueryParam("md5") String md5checksum
    ) {
        Stations stations = new Stations();
        int pageNumber = 0, perPageNumber = 50;
        Tuple2<Integer, Integer> validate;
        int bad_files_int;

        try {
            bad_files_int = Validator.ValidateBadFileExcluder(filtervalidatedfiles);
            validate = Validator.ValidatePageEntries(page.trim(), perPage.trim());
            pageNumber = validate.getFirst();
            perPageNumber = validate.getSecond();
        } catch (ValidatorException e) {
            return Response.status(e.responseStatus).entity(e.getMessage()).build();
        }

        if (instance == null)
            return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();

        Tuple2<Integer, String> r = new Tuple2<>();
        String media_type = MediaType.TEXT_PLAIN;
        format = format.trim().toLowerCase();
        switch (format) {
            case "csv":
                media_type = GlassConstants.TEXT_CSV;
                break;
            case "xml":
                media_type = MediaType.APPLICATION_XML;
                break;
            case "json":
                media_type = MediaType.APPLICATION_JSON;
                break;
        }
        if (format.equalsIgnoreCase("url_list")) format = "script";
        if (!md5checksum.equals("")) {
            r = stations.getFilesFileNameWithMd5(instance, md5checksum, format, pageNumber, perPageNumber, bad_files_int);
            return Response.ok(r.getSecond())
                    .header("Content-Type", media_type)
                    .header("x-pagination-current-page", pageNumber)
                    .header("x-pagination-per-page", perPage)
                    .header("x-pagination-total-count", r.getFirst())
                    .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                    .build();
        }
        r = stations.getFilesFileName(instance, format, pageNumber, perPageNumber, bad_files_int);
        return Response.ok(r.getSecond())
                .header("Content-Type", media_type)
                .header("x-pagination-current-page", pageNumber)
                .header("x-pagination-per-page", perPage)
                .header("x-pagination-total-count", r.getFirst())
                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                .build();

    }

    @Produces({MediaType.TEXT_PLAIN + ";charset=utf-8", MediaType.APPLICATION_JSON + ";charset=utf-8", MediaType.APPLICATION_XML + ";charset=utf-8", MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8"})
    @Path("combination/{instance}/{format}")
    @GET
    public Response getFilesbyCombination(
            @PathParam("instance") String instance,
            @PathParam("format") String format,
            @DefaultValue("0") @QueryParam("filtervalidatedfiles") String filtervalidatedfiles,
            @DefaultValue("") @QueryParam("page") String page,
            @DefaultValue("") @QueryParam("perpage") String perPage
    ) throws ParseException {
        Stations stations = new Stations();
        int pageNumber = 0, perPageNumber = 50;
        Tuple2<Integer, Integer> validate;
        int bad_files_int;

        try {
            bad_files_int = Validator.ValidateBadFileExcluder(filtervalidatedfiles);
            validate = Validator.ValidatePageEntries(page.trim(), perPage.trim());
            pageNumber = validate.getFirst();
            perPageNumber = validate.getSecond();
        } catch (ValidatorException e) {
            return Response.status(e.responseStatus).entity(e.getMessage()).build();
        }

        if (instance == null)
            return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();

        Tuple2<Integer, String> r = new Tuple2<>();
        String media_type = MediaType.TEXT_PLAIN;
        format = format.trim().toLowerCase();
        switch (format) {
            case "csv":
                media_type = GlassConstants.TEXT_CSV;
                break;
            case "xml":
                media_type = MediaType.APPLICATION_XML;
                break;
            case "json":
                media_type = MediaType.APPLICATION_JSON;
                break;
        }
        if (format.equalsIgnoreCase("url_list")) format = "script";
        r = stations.getFilesCombination(instance, format, pageNumber, perPageNumber, bad_files_int);
        return Response.ok(r.getSecond())
                .header("Content-Type", media_type)
                .header("x-content-type-options", "nosniff")
                .header("x-frame-options", "SAMEORIGIN")
                .header("x-pagination-current-page", pageNumber)
                .header("x-pagination-per-page", perPage)
                .header("x-pagination-total-count", r.getFirst())
                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                .build();

    }

    @Produces({MediaType.TEXT_PLAIN + ";charset=utf-8", MediaType.APPLICATION_JSON + ";charset=utf-8", MediaType.APPLICATION_XML + ";charset=utf-8", MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8"})
    @Path("combinationT3/{instance}/{format}")
    @GET
    public Response getFilesbyCombinationt3(
            @PathParam("instance") String instance,
            @PathParam("format") String format,
            @DefaultValue("0") @QueryParam("filtervalidatedfiles") String filtervalidatedfiles,
            @DefaultValue("") @QueryParam("page") String page,
            @DefaultValue("") @QueryParam("perpage") String perPage
    ) throws ParseException {
        Stations stations = new Stations();
        int pageNumber = 0, perPageNumber = 50;
        Tuple2<Integer, Integer> validate;
        int bad_files_int;
        int total_pages = 0;

        try {
            bad_files_int = Validator.ValidateBadFileExcluder(filtervalidatedfiles);
            validate = Validator.ValidatePageEntries(page.trim(), perPage.trim());
            pageNumber = validate.getFirst();
            perPageNumber = validate.getSecond();
        } catch (ValidatorException e) {
            return Response.status(e.responseStatus).entity(e.getMessage()).build();
        }

        if (instance == null)
            return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();

        Tuple2<Integer, String> r = new Tuple2<>();
        String media_type = MediaType.TEXT_PLAIN;
        format = format.trim().toLowerCase();
        switch (format) {
            case "csv":
                media_type = GlassConstants.TEXT_CSV;
                break;
            case "xml":
                media_type = MediaType.APPLICATION_XML;
                break;
            case "json":
                media_type = MediaType.APPLICATION_JSON;
                break;
        }
        if (format.equalsIgnoreCase("url_list")) format = "script";
        r = stations.getFilesCombinationT3(instance, format, pageNumber, perPageNumber, bad_files_int);
        return Response.ok(r.getSecond())
                .header("Content-Type", media_type)
                .header("x-content-type-options", "nosniff")
                .header("x-frame-options", "SAMEORIGIN")
                .header("x-pagination-current-page", pageNumber)
                .header("x-pagination-per-page", perPage)
                .header("x-pagination-total-count", r.getFirst())
                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                .build();

    }

    @Produces({MediaType.TEXT_PLAIN + ";charset=utf-8", MediaType.APPLICATION_JSON + ";charset=utf-8", MediaType.APPLICATION_XML + ";charset=utf-8", MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8"})
    @Path("{type}/{instance}/{format}/{page}/{perpage}")
    @GET
    public Response Files(//@Context HttpHeaders headers,
                          //@HeaderParam("accept") String accept,
                          @PathParam("format") String format,
                          @PathParam("type") String type,
                          @PathParam("instance") String instance,
                          @DefaultValue("0") @QueryParam("filtervalidatedfiles") String filtervalidatedfiles,
                          @PathParam("page") String page,
                          @PathParam("perpage") String perPage,
                          @DefaultValue("") @QueryParam("md5") String md5checksum) throws IOException, ParseException, SQLException, ClassNotFoundException {

        Stations stations = new Stations();
        String date_from = "", date_to = "";
        int pageNumber = 0, perPageNumber = 50;
        Tuple2<Integer, Integer> validate;
        int bad_files_int;
        int total_pages = 0;

        try {
            bad_files_int = Validator.ValidateBadFileExcluder(filtervalidatedfiles);
            validate = Validator.ValidatePageEntries(page.trim(), perPage.trim());
            pageNumber = validate.getFirst();
            perPageNumber = validate.getSecond();
        } catch (ValidatorException e) {
            return Response.status(e.responseStatus).entity(e.getMessage()).build();
        }

        if (type == null || instance == null)
            return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();

        Tuple2<Integer, String> r = new Tuple2<>();
        String media_type = MediaType.TEXT_PLAIN;
        type = type.trim().toLowerCase();
        format = format.trim().toLowerCase();
        /*
         Return csv, xml, or script - need to be correctly written else json will be returned.
         */
        switch (format) {
            case "csv":
                media_type = GlassConstants.TEXT_CSV;
                break;
            case "xml":
                media_type = MediaType.APPLICATION_XML;
                break;
            case "json":
                media_type = MediaType.APPLICATION_JSON;
                break;
        }

        if (format.equalsIgnoreCase("url_list")) format = "script";
        switch (type) {
            case "name":
                r = stations.getFilesName(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("Current-Page", pageNumber)
                        .header("Total-per-Page", perPageNumber)
                        .header("Total-Elements", r.getFirst())
                        .header("Total-Pages", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "station-marker":
            case "marker":
                r = stations.getFilesMarker(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-content-type-options", "nosniff")
                        .header("x-frame-options", "SAMEORIGIN")
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "network":
                r = stations.getFilesStationNetwork(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "type":
                r = stations.getFilesStationType(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "antenna":
                r = stations.getFilesAntennaType(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "receiver":
                r = stations.getFilesReceiverType(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "radome":
                r = stations.getFilesRadomeType(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "country":
                r = stations.getFilesCountry(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "state":
                r = stations.getFilesState(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "city":
                r = stations.getFilesCity(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "agency":
                r = stations.getFilesAgency(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "combination":
                r = stations.getFilesCombination(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();

            case "combinationt3":
                r = stations.getFilesCombinationT3(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "sampling_frequency":
                r = stations.getSamplingFrequency(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();


            case "length":
                r = stations.getLength(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();

            case "coordinates_data":
                r = stations.getFilesCoordinatesData(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();


            /* default type is json */
            case "format":
                r = stations.getFilesFormat(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber)).build();
            case "coordinates":
                String minLat = "", maxLat = "", minLon = "", maxLon = "";

                try {
                    String[] params = instance.toLowerCase().split("&");

                    if (params.length != 4)
                        throw new Exception("Invalid parameters[1]");

                    for (int i = 0; i < 4; i++) {
                        if (params[i].contains("minlat"))
                            minLat = params[i];
                        else if (params[i].contains("maxlat"))
                            maxLat = params[i];
                        else if (params[i].contains("minlon"))
                            minLon = params[i];
                        else if (params[i].contains("maxlon"))
                            maxLon = params[i];
                        else
                            throw new Exception("Invalid parameters[2]");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();
                }

                /* default type is json */
                r = stations.getFilesCoordinates(
                        Float.parseFloat(minLat.split("=")[1]),
                        Float.parseFloat(maxLat.split("=")[1]),
                        Float.parseFloat(minLon.split("=")[1]),
                        Float.parseFloat(maxLon.split("=")[1]),
                        format,
                        pageNumber,
                        perPageNumber,
                        bad_files_int
                );
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            // Reference date
            case "date_range":
                try {
                    String[] params = instance.toLowerCase().split("&");

                    if (params.length != 2)
                        throw new Exception("Invalid parameters[1]");

                    for (int i = 0; i < 2; i++) {
                        if (params[i].contains("date_from"))
                            date_from = params[i].split("=")[1];
                        else if (params[i].contains("date_to"))
                            date_to = params[i].split("=")[1];
                        else
                            throw new Exception("Invalid parameters[2]");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();
                }

                // default type is json
                r = stations.getFilesDateRange(date_from, date_to, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();

            case "station_dates":
                try {
                    String[] params = instance.toLowerCase().split("&");

                    if (params.length != 2)
                        throw new Exception("Invalid parameters[1]");

                    for (int i = 0; i < 2; i++) {
                        if (params[i].contains("date_from"))
                            date_from = params[i].split("=")[1];
                        else if (params[i].contains("date_to"))
                            date_to = params[i].split("=")[1];
                        else
                            throw new Exception("Invalid parameters[2]");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();
                }

                /* default type is json */
                r = stations.getFilesStationDates(date_from, date_to, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
           case "published_date":
                try {
                    String[] params = instance.toLowerCase().split("&");

                    if (params.length != 2)
                        throw new Exception("Invalid parameters[1]");

                    for (int i = 0; i < 2; i++) {
                        if (params[i].contains("date_from"))
                            date_from = params[i].split("=")[1];
                        else if (params[i].contains("date_to"))
                            date_to = params[i].split("=")[1];
                        else
                            throw new Exception("Invalid parameters[2]");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();
                }

                r = stations.getFilesDatePublished(date_from, date_to, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "md5":
                r = stations.getFilesMD5(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "file_name":
                if (!md5checksum.equals("")) {
                    r = stations.getFilesFileNameWithMd5(instance, md5checksum, format, pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", media_type)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                            .build();
                }
                r = stations.getFilesFileName(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();
            case "data_center_acronym":
                r = stations.getFilesbyAcronymDataCenter(instance, format, pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", media_type)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                        .build();

            default:
                Response.status(Response.Status.BAD_REQUEST).entity("Invalid object type.").build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();
    }

    /*
    From the swagger
    date_from/to of the files, including. Format YYYY-MM-DD e.g. 2020-01-01
     */
    @Path("info")
    @GET
    public Response FilesInfo(
            @DefaultValue("") @QueryParam("marker_long_name") String marker_long_name,
            @DefaultValue("") @QueryParam("status") String status,
            @DefaultValue("") @QueryParam("date_from") String date_from,
            @DefaultValue("") @QueryParam("date_to") String date_to,
            @DefaultValue("") @QueryParam("datacenter_acronym") String datacenter_acronym,
            @DefaultValue("") @QueryParam("page") String page,
            @DefaultValue("") @QueryParam("perpage") String perPage
    ) {
        int pageNumber = 0, perPageNumber = 50;
        List<Integer> statusInts = new ArrayList<>();
        Tuple2<Integer, Integer> validate;

        try {
            validate = Validator.ValidatePageEntries(page.trim(), perPage.trim());
            pageNumber = validate.getFirst();
            perPageNumber = validate.getSecond();
            //date validations
            date_from = date_from.trim();
            date_to = date_to.trim();
            if (!date_from.isEmpty()) {
                Validator.ValidateLocalDateFormat(date_from);
            }
            if (!date_to.isEmpty()) {
                Validator.ValidateLocalDateFormat(date_to);
            }

            // status validation
            if (!status.trim().equals("")) {
                statusInts = Validator.ValidateStatus(status);
            }

        } catch (ValidatorException e) {
            return Response.status(e.responseStatus).entity(e.getMessage()).build();
        }

        DataBaseT2Connection dbt2c = new DataBaseT2Connection();
        ArrayList<FilesInfo> raw_result;
        try {
            raw_result = dbt2c.getFilesInfo(marker_long_name, statusInts, datacenter_acronym, date_from, date_to, pageNumber, perPageNumber);
        } catch (OutOfMemoryError e) {
            return Response.serverError().entity("Server has no available resources to process this. Try limiting the conditions.").build();
        }

        if (raw_result.isEmpty()) {
            return Response.ok("[]", MediaType.APPLICATION_JSON).build();
        }

        ParsersV2 parser = new ParsersV2();

        String result = parser.getFilesInfoJSON(raw_result);

        return Response.ok(result, MediaType.APPLICATION_JSON).build();
    }

    /*
    End Point to return rinex files with a QC report containing a NULL multipath
    Inputs
    marker (9 char)
    Optional : ?date_from=yyyy-mm-dd2&date_to=yyyy-mm-dd
    Optional pageNumber -->
    page number using 1-based indexing 0 is the default and means get everything
    perpage number of records per page  Default value : 50
    Returns json on successful processing
     */
    @Produces({MediaType.APPLICATION_JSON + ";charset=utf-8"})
    @Path("/QC/MultiPathNULL/{marker}")
    @GET
    public Response QCMultiPathZero(
            @PathParam("marker") String markerIn,
            @DefaultValue("") @QueryParam("page") String page,
            @DefaultValue("") @QueryParam("perpage") String perPage,
            @DefaultValue("") @QueryParam("date_from") String date_from,
            @DefaultValue("") @QueryParam("date_to") String date_to) {

        int pageNumber = 0, perPageNumber = 50;
        String marker = "";
        String dates = "";
        Tuple2<Integer, Integer> validate;
        try {
            marker = Validator.ValidateMarker9(markerIn);  /* Marker must be 9chars - no Wildcards */
            validate = Validator.ValidatePageEntries(page.trim(), perPage.trim());
            pageNumber = validate.getFirst();
            perPageNumber = validate.getSecond();

            date_from = date_from.trim();
            date_to = date_to.trim();
            if (!date_from.isEmpty()) {
                Validator.ValidateLocalDateFormat(date_from);
                dates += " and rf.reference_date >= '" + date_from + "' ";
            }
            if (!date_to.isEmpty()) {
                Validator.ValidateLocalDateFormat(date_to);
                dates += " and  rf.reference_date <= '" + date_to + "' ";
            }
        } catch (ValidatorException e) {
            return Response.status(e.responseStatus).entity(e.getMessage()).build();
        }

        JsonCache myCache = new JsonCache();
        List<markerinfo> marker_and_id;
        try {
            marker_and_id = myCache.getMarkerandKeyByValue(marker);
        } catch (IOException ex) {
            // 500 Internal Server Error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Cache Lookup Failed\n" + ex.getMessage()).build();
        }

        if (marker_and_id.size() != 1)
            return Response.status(Response.Status.BAD_REQUEST).entity("{ \"Invalid Request\":\" " + marker + ": marker not found or must be unique 9char\" }").build();

        int id = marker_and_id.get(0).id;

        DataBaseT3QCConnection dbtcqc = new DataBaseT3QCConnection();

        try {
            ArrayList res = dbtcqc.getFilesT3QCcodmpth(id, dates, pageNumber, perPageNumber);
            Parsers parser = new Parsers();
            JSONArray obj = parser.getFileT3FullJSON(res);
            return Response.ok(obj.toJSONString(), MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }
    /**
     * <b>GET high_rates_files V2</b> - Returns the high_rates files(s) that match the marker.
     *
     * <p>
     * <b>object_instance - </b> the value of the criteria <i>(e.g. for country criteria may be CASC)</i>
     * </p>
     *
     *
     * <p>
     * <b>output_format - </b> the format of the output
     * <ul>
     *     <li>csv</li>
     *     <li>json</li>
     *     <li>script<b>ª</b></li>
     *     <li>xml</li>
     * </ul>
     *
     * <b>ª</b> - this format is not valid for all object_type
     * </p>
     *
     * @param format   xml, json, csv or script
     * @param instance CASC, ABBS, AMAR00PRT, ..
     * @return Files
     * @throws IOException
     */
    @Produces({MediaType.TEXT_PLAIN + ";charset=utf-8", MediaType.APPLICATION_JSON + ";charset=utf-8", MediaType.APPLICATION_XML + ";charset=utf-8", MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8"})
    @Path("highrate/station-marker/{instance}/{format}")
    @GET
    public Response getHRFilesbyMarker(
            @PathParam("instance") String instance,
            @PathParam("format") String format,
            @DefaultValue("") @QueryParam("page") String page,
            @DefaultValue("") @QueryParam("perpage") String perPage
    ) {
        Stations stations = new Stations();
        int pageNumber = 0, perPageNumber = 50;
        Tuple2<Integer, Integer> validate;
        int bad_files_int = 0;
        try {
          //  bad_files_int = Validator.ValidateBadFileExcluder(filtervalidatedfiles);
            validate = Validator.ValidatePageEntries(page.trim(), perPage.trim());
            pageNumber = validate.getFirst();
            perPageNumber = validate.getSecond();
        } catch (ValidatorException e) {
            return Response.status(e.responseStatus).entity(e.getMessage()).build();
        }

        if (instance == null)
            return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();
        try {
            if(!Validator.ValidateHR()){
              return Response.status(Response.Status.BAD_REQUEST).entity("This Node does not contain stations with associated High Rate Rinex Files")
                      .type(MediaType.TEXT_PLAIN)
                      .build();
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        Tuple2<Integer, String> r = new Tuple2<>();
        String media_type = MediaType.TEXT_PLAIN;
        format = format.trim().toLowerCase();
        switch (format) {
            case "csv":
                media_type = GlassConstants.TEXT_CSV;
                break;
            case "xml":
                media_type = MediaType.APPLICATION_XML;
                break;
            case "json":
                media_type = MediaType.APPLICATION_JSON;
                break;
        }
        if (format.equalsIgnoreCase("url_list")) format = "script";
        r = stations.getHRFilesMarker(instance, format, pageNumber, perPageNumber, bad_files_int);
        return Response.ok(r.getSecond())
                .header("Content-Type", media_type)
                .header("x-content-type-options", "nosniff")
                .header("x-frame-options", "SAMEORIGIN")
                .header("x-pagination-current-page", pageNumber)
                .header("x-pagination-per-page", perPage)
                .header("x-pagination-total-count", r.getFirst())
                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                .build();

    }
    /**
     * <b>GET high_rates_files V2</b> - Returns the high_rates files(s) that match the marker.
     *
     * <p>
     * <b>station_marker - </b> the value of the station-marker <i>(e.g. CASC, ABBS, AMAR00PRT)</i>
     * </p>
     *
     *
     * <p>
     * <b>output_format - </b> the format of the output
     * <ul>
     *     <li>csv</li>
     *     <li>json</li>
     *     <li>script<b>ª</b></li>
     *     <li>xml</li>
     * </ul>
     *
     * <b>ª</b> - this format is not valid for all object_type
     * </p>
     *
     * @param format   xml, json, csv or script
     * @param station_marker CASC, ABBS, AMAR00PRT, ..
     * @param published_date
     * @return Files
     * @throws IOException
     */
    @Produces({MediaType.TEXT_PLAIN + ";charset=utf-8", MediaType.APPLICATION_JSON + ";charset=utf-8", MediaType.APPLICATION_XML + ";charset=utf-8", MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8"})
    @Path("highrate/{station_marker}/{published_date}/{format}")
    @GET
    public Response getHRFilesbyMarkerandPD(
            @PathParam("station_marker") String station_marker,
            @PathParam("published_date") String published_date,
            @PathParam("format") String format,
            @DefaultValue("") @QueryParam("page") String page,
            @DefaultValue("") @QueryParam("perpage") String perPage
    ) {
        Stations stations = new Stations();
        int pageNumber = 0, perPageNumber = 50;
        Tuple2<Integer, Integer> validate;
        int bad_files_int = 0;
        try {
            //  bad_files_int = Validator.ValidateBadFileExcluder(filtervalidatedfiles);
            validate = Validator.ValidatePageEntries(page.trim(), perPage.trim());
            pageNumber = validate.getFirst();
            perPageNumber = validate.getSecond();
        } catch (ValidatorException e) {
            return Response.status(e.responseStatus).entity(e.getMessage()).build();
        }

        if (station_marker == null || published_date == null)
            return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();
        try {
            if(!Validator.ValidateHR()){
                return Response.status(Response.Status.BAD_REQUEST).entity("This Node does not contain stations with associated High Rate Rinex Files")
                        .type(MediaType.TEXT_PLAIN)
                        .build();
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        Tuple2<Integer, String> r = new Tuple2<>();
        String media_type = MediaType.TEXT_PLAIN;
        format = format.trim().toLowerCase();
        switch (format) {
            case "csv":
                media_type = GlassConstants.TEXT_CSV;
                break;
            case "xml":
                media_type = MediaType.APPLICATION_XML;
                break;
            case "json":
                media_type = MediaType.APPLICATION_JSON;
                break;
        }
        String date_from="",date_to="";
        try {
            String[] params = published_date.toLowerCase().split("&");

            if (params.length != 2)
                throw new Exception("Invalid parameters[1]");

            for (int i = 0; i < 2; i++) {
                if (params[i].contains("date_from"))
                    date_from = params[i].split("=")[1];
                else if (params[i].contains("date_to"))
                    date_to = params[i].split("=")[1];
                else
                    throw new Exception("Invalid parameters[2]");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();
        }

        r = stations.getHRFilesMarkerPD(station_marker,date_from,date_to, format, pageNumber, perPageNumber, bad_files_int);
        return Response.ok(r.getSecond())
                .header("Content-Type", media_type)
                .header("x-content-type-options", "nosniff")
                .header("x-frame-options", "SAMEORIGIN")
                .header("x-pagination-current-page", pageNumber)
                .header("x-pagination-per-page", perPage)
                .header("x-pagination-total-count", r.getFirst())
                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(), perPageNumber))
                .build();

    }



}
