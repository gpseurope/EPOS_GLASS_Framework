/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Glass;

import javax.ws.rs.core.MediaType;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

/**
 *
 * @author Paul Crocker
 * 
 * CONSTANTS Should always be place in a separate Class for easy management
 * Class Can Not be Instanced
 * 
 */
public class GlassConstants {

    public static final char SPACE = ' ';
    public static final char NEWLINE = '\n';
    public static final String FRAMEWORKCONFIGNAME ="java:global/GlassFramework/SiteConfig!Configuration.SiteConfig";

    public static int DB_ITEM_ATTRIBUTE_ANTENNA_ID = 0;
    public static int DB_ITEM_ATTRIBUTE_RADOME_ID = 0;
    public static int DB_ITEM_ATTRIBUTE_RECEIVER_ID = 0;

    public static int PRODUCTS_TIMESERIES  = 0;
    public static int PRODUCTS_COORDINATES = 1;
    public static int PRODUCTS_VELOCITES   = 2;
    public static int PRODUCTS_STRAIN      = 3;
    public static int PRODUCTS_ALLTIMESERIES  = 4;
    public static int PRODUCTS_SPECTRAL = 5;

    //constant to determine if we use the cache in the getStationsShortV2 function or not
    public static boolean UseCacheIngetStationsShortV2 = false;

     //new compile time flag to indicate if we are compiling for the products portal or not
    public static boolean PRODUCTSPORTAL = false;


    /* Optimizing Constants for Glass Features - Set to False to Turn Off*/
    /* Optimization for using the view_estimated_coordinates table */
    public static final boolean PRODUCT_OPTIMIZE_1 =true;
    /* Optimizing Constants for Glass Features - Set to False to Turn Off*/
    /* Optimization for using the view_stations_with_products table */
    public static final boolean PRODUCT_OPTIMIZE_2 =true;


    /*
        Products Portal
     */
    public static final String  PowerDensityPlotsPath= "/media/plots/";
    public static final String  TimeSeriesPlotsPath=  "/mnt/NAS/public/graphs/";
    public static final boolean oldDBVersion = false; // True if it's using DB Version < 1.3.0

    public static final int _ENU=0, _XYZ=1;

    public GlassConstants() {
        //This class should not be instanced
        //This assert is in case someone tries to instance this class :)
        throw new AssertionError();
    }

    public static void setItemAttributeIDs(Connection c) throws SQLException {
        DataBaseConnectionV2 dbv2 = new DataBaseConnectionV2();
        try {
            Map<String, Integer> itemTypeIDs = dbv2.getAttributeIDs(c);
            DB_ITEM_ATTRIBUTE_ANTENNA_ID = itemTypeIDs.get("antenna");
            DB_ITEM_ATTRIBUTE_RADOME_ID = itemTypeIDs.get("radome");
            DB_ITEM_ATTRIBUTE_RECEIVER_ID = itemTypeIDs.get("receiver");

        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("SQl exception on the first database call made. setItemAttributeIDs");
            System.err.println("It failed .. Stop The Application... Check database credentials");
            throw e;
        }
    }

    public static int getDbItemAttributeAntennaId() {
        return DB_ITEM_ATTRIBUTE_ANTENNA_ID;
    }

    public static int getDbItemAttributeRadomeId() {
        return DB_ITEM_ATTRIBUTE_RADOME_ID;
    }

    public static int getDbItemAttributeReceiverId() {
        return DB_ITEM_ATTRIBUTE_RECEIVER_ID;
    }


    /* For csv type as per rfc 7111 */
    public final static String TEXT_CSV = "text/csv";
    public final static MediaType TEXT_CSV_TYPE = new MediaType("text", "csv");
}
