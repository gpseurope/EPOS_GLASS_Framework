package Glass;

import EposTables.Coordinates;
import java.util.ArrayList;

/*
* REQUEST
* This class is responsible for storing the data fo a URI request.
*/
public class Request {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private String request_string;                  // Stores the value of the request string
    public ArrayList<String> marker_list;           // Stores the requests for marker
    public ArrayList<String> site_list;             // Stores the requests for site
    public ArrayList<String> height_list;           // Stores the requests for height
    public ArrayList<String> altitude_list;         // Stores the requests for altitude
    public ArrayList<String> latitude_list;         // Stores the requests for latitude
    public ArrayList<String> longitude_list;        // Stores the requests for longitude
    public ArrayList<String> installed_list;        // Stores the requests for installed date
    public ArrayList<String> removed_list;          // Stores the requests for removed date
    public ArrayList<String> network_list;          // Stores the requests for network
    public ArrayList<String> agency_list;           // Stores the requests for agency
    public ArrayList<String> antennaT_list;         // Stores the requests for antenna type
    public ArrayList<String> receiverT_list;        // Stores the requests for receiver type
    public ArrayList<String> radomeT_list;          // Stores the requests for radome type
    public ArrayList<String> country_list;          // Stores the requests for country
    public ArrayList<String> state_list;            // Stores the requests for state
    public ArrayList<String> city_list;             // Stores the requests for city
    public ArrayList<Coordinates> coordinates_list; // Stores the requests for coordinates
    public ArrayList<String> satellite_list;        // Stores the requests for satellite system
    public ArrayList<String> station_type_list;     // Stores the requests for station type
    public ArrayList<String> inverse_networks_list; // Stores the requests for inverse networks
    public float minLat, maxLat;                    // Stores the request for latitude rectangle
    public float minLon, maxLon;                    // Stores the request for longitude rectangle
    public float lat, lon, radius;                  // Stores the request for circle coordinates
    public String coordinates_type;                 // Stores the type of coordinate request
    public float minAlt, maxAlt;                    // Stores the request for altitude
    public float minLatitude, maxLatitude;          // Stores the request for laltitude
    public float minLongitude, maxLongitude;        // Stores the request for longitude
    public ArrayList<String> date_range_list;       // Stores the requests for date range
    public ArrayList<String> publish_date_list;     // Stores the requests for publish date
    public ArrayList<String> file_type_list;        // Stores the requests for file type
    public ArrayList<String> sampling_frequency_list; // Stores the requests for sampling frequency
    public ArrayList<String> data_availability_list; // Stores the requests for data availability
    public ArrayList<String> sampling_window_list; // Stores the requests for sampling window
    public String minInstall, maxInstall;                    // Stores the request for installed dates
    public String minRemoved, maxRemoved;                    // Stores the request for removed dates
    public float minimumObservation;                          // Stores the requests for Minimun observation of the station


    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * REQUEST
    * This is the default constructor for the Request class.
    */
    public Request()
    {
        this.request_string = "";
        this.marker_list = new ArrayList();
        this.site_list = new ArrayList();
        this.height_list = new ArrayList();
        this.latitude_list = new ArrayList();
        this.longitude_list = new ArrayList();
        this.altitude_list = new ArrayList();
        this.installed_list = new ArrayList();
        this.removed_list = new ArrayList();
        this.network_list = new ArrayList();
        this.agency_list = new ArrayList();
        this.antennaT_list = new ArrayList();
        this.receiverT_list = new ArrayList();
        this.radomeT_list = new ArrayList();
        this.country_list = new ArrayList();
        this.state_list = new ArrayList();
        this.city_list = new ArrayList();
        this.coordinates_list = new ArrayList();
        this.satellite_list = new ArrayList();
        this.station_type_list = new ArrayList();
        this.inverse_networks_list = new ArrayList();
        this.coordinates_type = "";
        this.date_range_list = new ArrayList();
        this.publish_date_list = new ArrayList();
        this.file_type_list = new ArrayList();
        this.data_availability_list = new ArrayList();
        this.sampling_frequency_list = new ArrayList();
        this.sampling_window_list = new ArrayList();
    }
    
    /*
    * REQUEST
    * This constructor for the Request class sets the value for the request string.
    */
    public Request(String request_string)
    {
        this.request_string = request_string;
        this.marker_list = new ArrayList();
        this.site_list = new ArrayList();
        this.height_list = new ArrayList();
        this.latitude_list = new ArrayList();
        this.longitude_list = new ArrayList();
        this.altitude_list = new ArrayList();
        this.installed_list = new ArrayList();
        this.removed_list = new ArrayList();
        this.network_list = new ArrayList();
        this.agency_list = new ArrayList();
        this.antennaT_list = new ArrayList();
        this.receiverT_list = new ArrayList();
        this.radomeT_list = new ArrayList();
        this.country_list = new ArrayList();
        this.state_list = new ArrayList();
        this.city_list = new ArrayList();
        this.coordinates_list = new ArrayList();
        this.satellite_list = new ArrayList();
        this.station_type_list = new ArrayList();
        this.inverse_networks_list = new ArrayList();
        this.coordinates_type = "";
        this.date_range_list = new ArrayList();
        this.publish_date_list = new ArrayList();
        this.file_type_list = new ArrayList();
        this.data_availability_list = new ArrayList();
        this.sampling_frequency_list = new ArrayList();
        this.sampling_window_list = new ArrayList();
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET REQUEST STRING
    * Returns the request_string value.
    */
    public String getRequest_string() {
        return request_string;
    }

    /*
    * SET REQUEST STRING
    * Sets the request_string value.
    */
    public void setRequest_string(String request_string) {
        this.request_string = request_string;
    }
    
    // =========================================================================
    //  PARSING REQUEST
    // =========================================================================
    /*
    * PARSE REQUEST
    * This method parses the rrequest string and stores the values on the respective
    * ArrayLists for the usage in the queries.
    */
    public void parseRequest()
    {
        if (request_string.equalsIgnoreCase("") == false)
        {      
            // *****************************************************************
            // STEP 1
            // Divide the request string by the & character
            // *****************************************************************
            String[] parameters = request_string.split("&");
            
            // *****************************************************************
            // STEP 2
            // Handle each parameter
            // *****************************************************************
            for (int i=0; i<parameters.length; i++)
            {
                // *************************************************************
                // STEP 3
                // Separate keyword from values
                // *************************************************************
                String[] individual = parameters[i].split(":");
                
                // *************************************************************
                // STEP 4
                // Store request data
                // *************************************************************
                switch (individual[0]) {
                    
                    case "marker":
                        String[] marker_values = individual[1].split(",");
                        for (int j=0; j<marker_values.length; j++)
                            marker_list.add(marker_values[j]);
                        break;
                        
                    case "site":
                        String[] site_values = individual[1].split(",");
                        for (int j=0; j<site_values.length; j++)
                            site_list.add(site_values[j]);
                        break;
                        
                    case "height":
                        String[] height_values = individual[1].split(",");
                        for (int j=0; j<height_values.length; j++)
                            height_list.add(height_values[j]);
                        break;
                        
                    case "latitude":
                        minLatitude = 0.0f;
                        maxLatitude = 0.0f;
                        String[] latitude_values = individual[1].split(",");
                        for (int j=0; j<latitude_values.length; j++)
                        {
                            if(latitude_values[j].equalsIgnoreCase("0")){
                                latitude_values[j]="0.00000000000001";
                            }
                            latitude_list.add(latitude_values[j]);
                        } 
                        if(latitude_values.length==2){
                            if(latitude_values[0].length()!=0 && latitude_values[1].length()!=0){
                                minLatitude = Float.parseFloat(latitude_values[0]);
                                maxLatitude = Float.parseFloat(latitude_values[1]);
                            }else if(latitude_values[0].length()==0 && latitude_values[1].length()!=0){
                                minLatitude = 0.0f;
                                maxLatitude = Float.parseFloat(latitude_values[1]);
                            }    
                        }else if(latitude_values[0].length()!=0){
                            minLatitude = Float.parseFloat(latitude_values[0]);
                            maxLatitude = 0.0f;
                        }   
                        break;
                        
                    case "longitude":
                        minLongitude = 0.0f;
                        maxLongitude = 0.0f;
                        String[] longitude_values = individual[1].split(",");
                        for (int j=0; j<longitude_values.length; j++)
                        {
                            if(longitude_values[j].equalsIgnoreCase("0")){
                                longitude_values[j]="0.00000000000001";
                            }
                            longitude_list.add(longitude_values[j]);
                        }    
                        if(longitude_values.length==2){
                            if(longitude_values[0].length()!=0 && longitude_values[1].length()!=0){
                                minLongitude = Float.parseFloat(longitude_values[0]);
                                maxLongitude = Float.parseFloat(longitude_values[1]);
                            }else if(longitude_values[0].length()==0 && longitude_values[1].length()!=0){    
                                minLongitude = 0.0f;
                                maxLongitude = Float.parseFloat(longitude_values[1]);
                            }    
                        }else if(longitude_values[0].length()!=0){
                            minLongitude = Float.parseFloat(longitude_values[0]);
                            maxLongitude = 0.0f;
                        }  
                        break;    
                        
                    case "altitude":
                        minAlt = 0.0f;
                        maxAlt = 0.0f;
                        String[] altitude_values = individual[1].split(",");
                        for (int j=0; j<altitude_values.length; j++)
                        {
                            if(altitude_values[j].equalsIgnoreCase("0")){
                                altitude_values[j]="0.00000000000001";
                            }
                            altitude_list.add(altitude_values[j]);
                        } 
                        if(altitude_values.length==2){
                            if(altitude_values[0].length()!=0 && altitude_values[1].length()!=0){
                                minAlt = Float.parseFloat(altitude_values[0]);
                                maxAlt = Float.parseFloat(altitude_values[1]);
                            }else if(altitude_values[0].length()==0 && altitude_values[1].length()!=0){
                                minAlt = 0.0f;
                                maxAlt = Float.parseFloat(altitude_values[1]);
                            }     
                        }else if(altitude_values[0].length()!=0){
                            minAlt = Float.parseFloat(altitude_values[0]);
                            maxAlt = 0.0f;
                        }    
                        break;    
                        
                    case "installed_date":
                        String[] installed_values = individual[1].split(",");
                        for (int j=0; j<installed_values.length; j++)
                        {    
                            installed_list.add(installed_values[j]);
                        }
                        if(installed_values.length==2){
                            if(installed_values[0].length()!=0 && installed_values[1].length()!=0){
                                minInstall = installed_values[0];
                                maxInstall = installed_values[1];
                            }else if(installed_values[0].length()==0 && installed_values[1].length()!=0){
                                minInstall = "";
                                maxInstall = installed_values[1];
                            }     
                        }else if(installed_values[0].length()!=0){
                            minInstall = installed_values[0];
                            maxInstall = "";
                        }
                        break;
                        
                    case "removed_date":
                        String[] removed_values = individual[1].split(",");
                        for (int j=0; j<removed_values.length; j++)
                        {
                            removed_list.add(removed_values[j]);
                        }
                        if(removed_values.length==2){
                            if(removed_values[0].length()!=0 && removed_values[1].length()!=0){
                                minRemoved = removed_values[0];
                                maxRemoved = removed_values[1];
                            }else if(removed_values[0].length()==0 && removed_values[1].length()!=0){
                                minRemoved = "";
                                maxRemoved = removed_values[1];
                            }     
                        }else if(removed_values[0].length()!=0){
                            minRemoved = removed_values[0];
                            maxRemoved = "";
                        }
                        break;
                        
                    case "network":
                        String[] network_values = individual[1].split(",");
                        for (int j=0; j<network_values.length; j++)
                            network_list.add(network_values[j]);
                        break;
                        
                    case "agency":
                        String[] agency_values = individual[1].split(",");
                        for (int j=0; j<agency_values.length; j++)
                            agency_list.add(agency_values[j]);
                        break;
                        
                    case "antenna_type":
                        String[] antenna_values = individual[1].split(",");
                        for(int j=0; j<antenna_values.length; j++)
                            antennaT_list.add(antenna_values[j]);
                        break;
                        
                    case "receiver_type":
                        String[] receiver_values = individual[1].split(",");
                        for (int j=0; j<receiver_values.length; j++)
                            receiverT_list.add(receiver_values[j]);
                        break;
                        
                    case "radome_type":
                        String[] radome_values = individual[1].split(",");
                        for (int j=0; j<radome_values.length; j++)
                            radomeT_list.add(radome_values[j]);
                        break;
                        
                    case "country":
                        String[] country_values = individual[1].split(",");
                        for (int j=0; j<country_values.length; j++)
                            country_list.add(country_values[j]);
                        break;
                        
                    case "state":
                        String[] state_values = individual[1].split(",");
                        for (int j=0; j<state_values.length; j++)
                            state_list.add(state_values[j]);
                        break;
                        
                    case "city":
                        String[] city_values = individual[1].split(",");
                        for (int j=0; j<city_values.length; j++)
                            city_list.add(city_values[j]);
                        break;
                        
                    case "satellite":
                        String[] satellite_values = individual[1].split(",");
                        for (int j=0; j<satellite_values.length; j++)
                            satellite_list.add(satellite_values[j]);
                        break;
                        
                    case "type":
                        String[] station_types_values = individual[1].split(",");
                        for (int j=0; j<station_types_values.length; j++)
                            station_type_list.add(station_types_values[j]);
                        break;

                    case "inverse-networks":
                        String[] inverse_networks = individual[1].split(",");
                        for (int j=0; j<inverse_networks.length; j++)
                            inverse_networks_list.add(inverse_networks[j]);
                        break;
                        
                    case "date_range":
                        String[] date_range_values = individual[1].split(",");
                        for (int j=0; j<date_range_values.length; j++)
                        {
                            date_range_list.add(date_range_values[j].replace("%20", " "));
                            System.out.println(date_range_values[j].replace("%20", " "));
                        }
                        break;
                        
                    case "publish_date":
                        String[] publish_date_values = individual[1].split(",");
                        for (int j=0; j<publish_date_values.length; j++)
                            publish_date_list.add(publish_date_values[j]);
                        break;
                        
                    case "file_type":
                        String[] file_type_values = individual[1].split(",");
                        for (int j=0; j<file_type_values.length; j++)
                            file_type_list.add(file_type_values[j]);
                        break;
                        
                    case "data_availability":
                        String[] data_availabilty_values = individual[1].split(",");
                        for (int j=0; j<data_availabilty_values.length; j++)
                            data_availability_list.add(data_availabilty_values[j]);
                        break;    
                        
                    case "sampling_frequency":
                        String[] sampling_frequency_values = individual[1].split(",");
                        for (int j=0; j<sampling_frequency_values.length; j++)
                            sampling_frequency_list.add(sampling_frequency_values[j]);
                        break;
                        
                    case "sampling_window":
                        String[] sampling_window_values = individual[1].split(",");
                        for (int j=0; j<sampling_window_values.length; j++)
                            sampling_window_list.add(sampling_window_values[j]);
                        break;
                        
                    case "minimum_observation":    
                        minimumObservation = Float.parseFloat(individual[1]);
                        break;     

                    default:
                        if (individual[0].contains("coordinates="))
                        {
                            String[] coordinates_method = individual[0].split("=");

                            // Handle the type of coordinate method
                            switch (coordinates_method[1]) {
                                case "rectangle":
                                    // Set coordinates type
                                    coordinates_type = "rectangle";
                                    // Get values
                                    String[] values = individual[1].split(",");
                                    minLat = Float.parseFloat(values[0]);
                                    maxLat = Float.parseFloat(values[1]);
                                    minLon = Float.parseFloat(values[2]);
                                    maxLon = Float.parseFloat(values[3]);
                                    break;

                                case "circle":
                                    // Set coordinates type
                                    coordinates_type = "circle";
                                    // Get values
                                    String[] values_circle = individual[1].split(",");
                                    lat = Float.parseFloat(values_circle[0]);
                                    lon = Float.parseFloat(values_circle[1]);
                                    radius = Float.parseFloat(values_circle[2]);
                                    break;

                                case "polygon":
                                    // Set coordinates type
                                    coordinates_type = "polygon";
                                    // Get values
                                    String[] values_polygon = individual[1].split(",");
                                    for (int j=0; j<values_polygon.length; j++)
                                    {
                                        String[] coordinates_value = values_polygon[j].split("!");
                                        Float lat_value = Float.parseFloat(coordinates_value[0]);
                                        Float lon_value = Float.parseFloat(coordinates_value[1]);
                                        Coordinates coord = new Coordinates();
                                        coord.setLat(lat_value);
                                        coord.setLon(lon_value);
                                        coordinates_list.add(coord);
                                    }
                                    break;
                            }
                        }
                        break;
                }
            }
        }
        else
        {
            System.out.println("Error: empty resquest string!");
        }
    }
    
    /*
    * REQUEST TO STRING
    * This method prints the values of the ArrayLists.
    */
    public void requestToString()
    {
        String marker_values = "Marker: ";
        for (int i=0; i<marker_list.size(); i++)
            marker_values = marker_values + marker_list.get(i) + " ";
        System.out.println(marker_values);
        
        String site_values = "Site: ";
        for (int i=0; i<site_list.size(); i++)
            site_values = site_values + site_list.get(i) + " ";
        System.out.println(site_values);
        
        String height_values = "Height: ";
        for (int i=0; i<height_list.size(); i++)
            height_values = height_values + height_list.get(i) + " ";
        System.out.println(height_values);
        
        String installed_values = "Installed date: ";
        for (int i=0; i<installed_list.size(); i++)
            installed_values = installed_values + installed_list.get(i) + " ";
        System.out.println(installed_values);
        
        String removed_values = "Removed date: ";
        for (int i=0; i<removed_list.size(); i++)
            removed_values = removed_values + removed_list.get(i) + " ";
        System.out.println(removed_values);
        
        String network_values = "Network: ";
        for (int i=0; i<network_list.size(); i++)
            network_values = network_values + network_list.get(i) + " ";
        System.out.println(network_values);
        
        String agency_values = "Agency: ";
        for (int i=0; i<agency_list.size(); i++)
            agency_values = agency_values + agency_list.get(i) + " ";
        System.out.println(agency_values);
        
        String antenna_values = "Antenna type: ";
        for (int i=0; i<antennaT_list.size(); i++)
            antenna_values = antenna_values + antennaT_list.get(i) + " ";
        System.out.println(antenna_values);
        
        String receiver_values = "Receiver type: ";
        for (int i=0; i<receiverT_list.size(); i++)
            receiver_values = receiver_values + receiverT_list.get(i) + " ";
        System.out.println(receiver_values);
        
        String radome_values = "Radome type: ";
        for (int i=0; i<radomeT_list.size(); i++)
            radome_values = radome_values + radomeT_list.get(i) + " ";
        System.out.println(radome_values);
        
        String country_values = "Country: ";
        for (int i=0; i<country_list.size(); i++)
            country_values = country_values + country_list.get(i) + " ";
        System.out.println(country_values);
        
        String state_values = "State: ";
        for (int i=0; i<state_list.size(); i++)
            state_values = state_values + state_list.get(i) + " ";
        System.out.println(state_values);
        
        String city_values = "City: ";
        for (int i=0; i<city_list.size(); i++)
            city_values = city_values + city_list.get(i) + " ";
        System.out.println(city_values);
        
        String satellite_values = "Satellite: ";
        for (int i=0; i<satellite_list.size(); i++)
            satellite_values = satellite_values + satellite_list.get(i) + " ";
        System.out.println(satellite_values);
        
        String station_type_values = "Station Type: ";
        for (int i=0; i<station_type_list.size(); i++)
            station_type_values = station_type_values + station_type_list.get(i) + " ";
        System.out.println(station_type_values);

        switch (coordinates_type) {
            case "rectangle":
                System.out.println("minLat: " + minLat);
                System.out.println("maxLat: " + maxLat);
                System.out.println("minLon: " + minLon);
                System.out.println("maxLon: " + maxLon);
                break;
                
            case "circle":
                System.out.println("Lat: " + lat);
                System.out.println("Lon: " + lon);
                System.out.println("Radius: " + radius);
                break;
                
            case "polygon":
                String polygon_values = "Polygon: ";
                for (int i=0; i<coordinates_list.size(); i++)
                {
                    polygon_values = polygon_values + coordinates_list.get(i).getLat() +
                            " " + coordinates_list.get(i).getLon() + ", ";
                }
                System.out.println(polygon_values);
                break;
        }
    }
}