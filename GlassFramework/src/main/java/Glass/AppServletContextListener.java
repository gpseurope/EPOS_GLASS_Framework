package Glass;

/**
 *
 * @author Paul
 */

import Configuration.DBConsts;
import Configuration.SiteConfig;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

public class AppServletContextListener implements ServletContextListener
{
    @EJB
    private SiteConfig siteConfig;
    private String DBConnectionString = null;

    static int n;

    @Override
    public void contextInitialized(ServletContextEvent sce){
        System.out.println("Initialized n= "+ (n++));

        try{
            Thread.sleep(50);
        } catch (Exception e){
            ;
        }


        System.out.println("Working Directory = " +  System.getProperty("user.dir"));

        // Create connection with the local database
        // This connection will be used all through the cache initialize process
        Connection c=null;
        try {
            Context ctx = new InitialContext();
            this.siteConfig = (SiteConfig) ctx.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
            DBConnectionString = this.siteConfig.getDBConnectionString();
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection(DBConnectionString, DBConsts.DB_USERNAME, DBConsts.DB_PASSWORD);
            c.setAutoCommit(false);
        }
        catch (Exception e){
            System.err.println("Can't Connect To DataBase. " + DBConnectionString);
            System.err.println("Application will STOP. ");
            e.printStackTrace();
            // Should give up and go home
            // The application will just stop if this happens
            throw new  IllegalArgumentException();
        }

        //Obtain Item Attribute IDs in GlassConstants to diminish the number of queries done to the DB
        //This will call a sql method so effectively testing the connection string
        try {
            GlassConstants.setItemAttributeIDs(c);
        }  catch (SQLException e){
            //looks like a connection to DB error We Should give up and go home
            throw new  IllegalArgumentException();
        }

        //Cache initialization
        //Init the hashmap cache
        //Although this method should only be called once as it's in the initial context it's not clear from the logs if this is true or not
        // we will thus try to avoid multiple threads accessing the code below -
        if( new JsonCache().iscacheInitRunning()==false &&  new JsonCache().iscacheEmpty() )
        {
            System.out.println("Starting thread to initialize the cache");
            new JsonCache(c).start();
        }

        //JsonCache.InitCache(c);  //dont use thread
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Finalizing");

        //https://stackoverflow.com/questions/23545305/tomcat-hot-deployment-jdbc-driver-failed-to-unregister
        Enumeration<Driver> drivers = java.sql.DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            java.sql.Driver driver = drivers.nextElement();
            try {
                java.sql.DriverManager.deregisterDriver(driver);
            } catch (Exception e) {
                //log exception or ignore
            }
        }
    }

}
