package Glass;

import EposTables.*;
import java.util.ArrayList;

/*
* PRODUCT REQUEST
* This class is responsible for storing the data fo a URI request.
*/
public class ProductRequest {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private String request_string;                  // Stores the value of the request string
    public ArrayList<String> marker_list;           // Stores the requests for marker
    public ArrayList<String> analysis_centre_list;  // Stores the requests for analysis_centre
    public ArrayList<String> site_list;             // Stores the requests for site
    public ArrayList<String> date_start_list;       // Stores the requests for start date
    public ArrayList<String> date_end_list;         // Stores the requests for end date
    public ArrayList<String> network_list;          // Stores the requests for network
    public ArrayList<String> agency_list;           // Stores the requests for agency
    public ArrayList<String> software_list;         // Stores the requests for software
    public boolean jump_apply;                      // Stores the requests for jump
    public ArrayList<String> reference_frame_list;  // Stores the requests for reference frame
    public int outliers_apply;                  // Stores the requests for outliers
    public ArrayList<Coordinates> coordinates_list; // Stores the requests for coordinates
    public float minLat, maxLat;                    // Stores the request for latitude rectangle
    public float minLon, maxLon;                    // Stores the request for longitude rectangle
    public float lat, lon, radius;                  // Stores the request for circle coordinates
    public String coordinates_type;                 // Stores the type of coordinate request
    public String epoch_start, epoch_end;            // Stores the request for epoch start and end
    public String sampling_period;            // Stores the request for epoch start and end
    public ArrayList<Float> otl_model_list;         // Stores the type of OTL model
    public ArrayList<String> antenna_model_list;    // Stores the type of antenna model
    public ArrayList<Float> cut_off_angle_list;                // Stores the cut off angle
    public boolean offsets_apply;                   // Stores the request for apply offsets
    public String format;       //Stores the format (xml or json) for the timeseries file
    public String tectonic_plate; // Stores name of tectonic plate (velocities)

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * PRODUCT REQUEST
    * This is the default constructor for the Product Request class.
    */
    public ProductRequest() {
        this.request_string = "";
        this.marker_list = new ArrayList();
        this.site_list = new ArrayList();
        this.date_start_list = new ArrayList();
        this.date_end_list = new ArrayList();
        this.network_list = new ArrayList();
        this.agency_list = new ArrayList();
        this.software_list = new ArrayList();
        this.jump_apply = false;
        this.reference_frame_list = new ArrayList();
        this.outliers_apply = 0;
        this.coordinates_list = new ArrayList();
        this.coordinates_type = "";
        this.otl_model_list = new ArrayList();
        this.antenna_model_list = new ArrayList();
        this.cut_off_angle_list = new ArrayList();
        this.offsets_apply = false;
        this.epoch_start=null;
        this.epoch_end=null;
        this.format="";
        this.tectonic_plate = "";
    }
    
    /*
    * PRODUCT REQUEST
    * This constructor for the Product Request class sets the value for the request string.
    */
    public ProductRequest(String request_string) {
        this.request_string = request_string;
        this.marker_list = new ArrayList();
        this.analysis_centre_list = new ArrayList();
        this.site_list = new ArrayList();
        this.analysis_centre_list = new ArrayList();
        this.date_start_list = new ArrayList();
        this.date_end_list = new ArrayList();
        this.network_list = new ArrayList();
        this.software_list = new ArrayList();
        this.jump_apply = false;
        this.reference_frame_list = new ArrayList();
        this.outliers_apply = 0;
        this.coordinates_list = new ArrayList();
        this.coordinates_type = "";
        this.otl_model_list = new ArrayList();
        this.antenna_model_list = new ArrayList();
        this.cut_off_angle_list = new ArrayList();
        this.offsets_apply = false;
        this.epoch_end=null;
        this.epoch_start=null;
        this.format="";
        this.tectonic_plate = "";
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET REQUEST STRING
    * Returns the request_string value.
    */
    public String getRequest_string() {
        return request_string;
    }

    /*
    * SET REQUEST STRING
    * Sets the request_string value.
    */
    public void setRequest_string(String request_string) {
        this.request_string = request_string;
    }
    
    // =========================================================================
    //  PARSING REQUEST
    // =========================================================================
    /*
    * PARSE REQUEST
    * This method parses the request string and stores the values on the respective
    * ArrayLists for the usage in the queries.
    */
    public void parseRequest()
    {
        if (request_string.equalsIgnoreCase("") == false)
        {      
            // *****************************************************************
            // STEP 1
            // Divide the request string by the & character
            // *****************************************************************
            String[] parameters = request_string.split("&");
            
            // *****************************************************************
            // STEP 2
            // Handle each parameter
            // *****************************************************************
            for (int i=0; i<parameters.length; i++)
            {
                // *************************************************************
                // STEP 3
                // Separate keyword from values
                // *************************************************************
                String[] individual = parameters[i].split("=|\\:");
                //String[] individual = parameters[i].split("=");

                // *************************************************************
                // STEP 4
                // Store request data
                // *************************************************************
                individual[0] = individual[0].toLowerCase();
                switch ( individual[0] ) {
                    case "marker":
                        String[] marker_values = individual[1].split(",");
                        for (int j=0; j<marker_values.length; j++)
                            marker_list.add(marker_values[j].toUpperCase());
                        break;
                        
                    case "site":
                        String[] site_values = individual[1].split(",");
                        for (int j=0; j<site_values.length; j++)
                            site_list.add(site_values[j]);
                        break;

                        
                    case "network":
                        String[] network_values = individual[1].split(",");
                        for (int j=0; j<network_values.length; j++)
                            network_list.add(network_values[j]);
                        break;

                    case "analysis_centre":
                        String[] analysis_centre_values = individual[1].split(",");
                        for (int j=0; j<analysis_centre_values.length; j++)
                            analysis_centre_list.add(analysis_centre_values[j]);
                        break;


                    case "software":
                        String[] software_values = individual[1].split(",");
                        for (int j=0; j<software_values.length; j++)
                            software_list.add(software_values[j]);
                        break;
                        
                    case "jump":
                        if (individual[1].equalsIgnoreCase("on"))
                            jump_apply = true;
                        else
                            jump_apply = false;
                        break;
                        
                    case "reference_frame":
                        String[] reference_frame_values = individual[1].split(",");
                        for (int j=0; j<reference_frame_values.length; j++)
                            reference_frame_list.add(reference_frame_values[j]);
                        break;
                        
                    case "outliers":
                            outliers_apply = Integer.parseInt(individual[1]);
                        break;

                    case "epoch_start":
                        epoch_start = individual[1];
                        break;

                    case "epoch_end":
                        epoch_end = individual[1];
                        break;

                    case "sampling_period":
                        sampling_period = individual[1];
                        break;

                    case "offsets":
                        if (individual[1].equalsIgnoreCase("on"))
                            offsets_apply = true;
                        else
                            offsets_apply = false;
                        break;

                    case "otl_model":
                        String[] otl_model_values = individual[1].split(",");
                        for (int j=0; j<otl_model_values.length; j++)
                            otl_model_list.add(Float.parseFloat(otl_model_values[j]));
                        break;

                    case "antenna_model":
                        String[] antenna_model_values = individual[1].split(",");
                        for (int j=0; j<antenna_model_values.length; j++)
                            antenna_model_list.add(antenna_model_values[j]);
                        break;

                    case "cut_off_angle":
                        String[] cut_off_angle_values = individual[1].split(",");
                        for (int j=0; j<cut_off_angle_values.length; j++)
                            cut_off_angle_list.add(Float.parseFloat(cut_off_angle_values[j]));
                        break;
                    case "format":
                        format = individual[1];
                        break;
                    case "tectonic_plate":
                        tectonic_plate = individual[1];
                        break;

                    default:
                        if (individual[0].contains("coordinates_"))
                        {
                            String[] coordinates_method = individual[0].split("_");

                            // Handle the type of coordinate method
                            switch (coordinates_method[1]) {
                                case "rectangle":
                                    // Set coordinates type
                                    coordinates_type = "rectangle";
                                    // Get values
                                    String[] values = individual[1].split(",");
                                    minLat = Float.parseFloat(values[0]);
                                    maxLat = Float.parseFloat(values[1]);
                                    minLon = Float.parseFloat(values[2]);
                                    maxLon = Float.parseFloat(values[3]);
                                    break;

                                case "circle":
                                    // Set coordinates type
                                    coordinates_type = "circle";
                                    // Get values
                                    String[] values_circle = individual[1].split(",");
                                    lat = Float.parseFloat(values_circle[0]);
                                    lon = Float.parseFloat(values_circle[1]);
                                    radius = Float.parseFloat(values_circle[2]);
                                    break;

                                case "polygon":
                                    // Set coordinates type
                                    coordinates_type = "polygon";
                                    // Get values
                                    String[] values_polygon = individual[1].split(",");
                                    for (int j=0; j<values_polygon.length; j++)
                                    {
                                        String[] coordinates_value = values_polygon[j].split("!");
                                        Float lat_value = Float.parseFloat(coordinates_value[0]);
                                        Float lon_value = Float.parseFloat(coordinates_value[1]);
                                        Coordinates coord = new Coordinates();
                                        coord.setLat(lat_value);
                                        coord.setLon(lon_value);
                                        coordinates_list.add(coord);
                                    }
                                    break;
                            }
                        }
                        break;
                }
            }
        }
        else
        {
            System.out.println("Error: empty resquest string!");
        }
    }
    
    /*
    * REQUEST TO STRING
    * This method prints the values of the ArrayLists.
    */
    public void requestToString()
    {
        String marker_values = "Marker:";
        for (int i=0; i<marker_list.size(); i++)
            marker_values = marker_values + " " + marker_list.get(i);
        System.out.println(marker_values);
        
        String site_values = "Site:";
        for (int i=0; i<site_list.size(); i++)
            site_values = site_values +  " " + site_list.get(i);
        System.out.println(site_values);
        
        String date_start_values = "Start date:";
        for (int i=0; i<date_start_list.size(); i++)
            date_start_values = date_start_values +  " " + date_start_list.get(i);
        System.out.println(date_start_values);
        
        String date_end_values = "End date:";
        for (int i=0; i<date_end_list.size(); i++)
            date_end_values = date_end_values + " " + date_end_list.get(i);
        System.out.println(date_end_values);
        
        String network_values = "Network:";
        for (int i=0; i<network_list.size(); i++)
            network_values = network_values + " " + network_list.get(i);
        System.out.println(network_values);

        String analysis_centre_values = "Analysis_Centre:";
        for (int i=0; i<analysis_centre_list.size(); i++)
            analysis_centre_values = analysis_centre_values + " " + analysis_centre_list.get(i);
        System.out.println(analysis_centre_values);


        String software_values = "Software:";
        for (int i=0; i<software_list.size(); i++)
            software_values = software_values  + " " + software_list.get(i);
        System.out.println(software_values);
        
        String jump_value = "Jump:";
        jump_value = jump_value + jump_apply;
        System.out.println(jump_value);
        
        String reference_frame_values = "Refrence Frame:";
        for (int i=0; i<reference_frame_list.size(); i++)
            reference_frame_values = reference_frame_values  + " " + reference_frame_list.get(i);
        System.out.println(reference_frame_values);
        
        String outliers_value = "Outliers:";
        outliers_value = outliers_value + outliers_apply;
        System.out.println(outliers_value);

        //--- FlyingDutchman's attempt to include tectonic_plate
        System.out.println("Tectonic Plate:" + tectonic_plate);

        switch (coordinates_type) {
            case "rectagle":
                System.out.println("minLat: " + minLat);
                System.out.println("maxLat: " + maxLat);
                System.out.println("minLon: " + minLon);
                System.out.println("maxLon: " + maxLon);
                break;
                
            case "circle":
                System.out.println("Lat: " + lat);
                System.out.println("Lon: " + lon);
                System.out.println("Radius: " + radius);
                break;
                
            case "polygon":
                String polygon_values = "Polygon:";
                for (int i=0; i<coordinates_list.size(); i++)
                {
                    polygon_values = polygon_values + coordinates_list.get(i).getLat() +
                            " " + coordinates_list.get(i).getLon() + ", ";
                }
                System.out.println(polygon_values);
                break;
        }
    }
}
