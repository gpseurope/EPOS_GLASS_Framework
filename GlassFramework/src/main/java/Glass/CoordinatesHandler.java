package Glass;

import EposTables.Coordinates;
import EposTables.Estimated_coordinates;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.StrictMath.atan2;
import static java.lang.StrictMath.sqrt;

/**
 * Created by arodrigues on 9/13/17.
 */
public class CoordinatesHandler {


    /**
     * Converts XYZ coordinates to ENu
     * @param data Array list with all values of estimated_coordinates table for a specific station
     * @param stationsHM Hashmap identified by station ID with its most recent reference_position from reference_position_velocities table
     * @return ArrayList with estimated coordinates in ENU
     */
    public static ArrayList<Estimated_coordinates> xyz2enu(ArrayList<Estimated_coordinates> data, HashMap<Integer, Coordinates> stationsHM){

        double WGS84_A = 6378137;
        double WGS84_B = 6356752.31425;
        double WGS84_F = 298.257223563;
        //double WGS84_F = 1 / 298.257223563;
        double _1e2 = (WGS84_A * WGS84_A - WGS84_B * WGS84_B) / (WGS84_A * WGS84_A); // 1st eccentricity squared
        double _2e2 = (WGS84_A * WGS84_A - WGS84_B * WGS84_B) / (WGS84_B * WGS84_B); // 2nd eccentricity squared

        for (Estimated_coordinates datum : data) {

            double x = datum.getX();
            double y = datum.getY();
            double z = datum.getZ();

            double p = sqrt(x * x + y * y); // distance from minor axis
            double R = sqrt(p * p + z * z); // polar radius

            // parametric latitude
            double tan_beta = (WGS84_B * z) / (WGS84_A * p) * (1 + _2e2 * WGS84_B / R);
            double sin_beta = tan_beta / sqrt(1 + tan_beta * tan_beta);
            double cos_beta = sin_beta / tan_beta;

            // geodetic latitude (Bowring eqn 18)
            double phi = atan2(z + _2e2 * WGS84_B * sin_beta * sin_beta * sin_beta, p - _1e2 * WGS84_A * cos_beta * cos_beta * cos_beta);

            // longitude
            double lambda = atan2(y, x);

            // height above ellipsoid
            //double sin_phi = sin(phi);
            //double cos_phi = cos(phi);
            //double v = WGS84_A / sqrt(1 - _1e2 * sin_phi * sin_phi);
            //double h = p * cos_phi + z * sin_phi - (WGS84_A * WGS84_A / v);

            //double latitude = phi * 180 / Math.PI;
            //double longitude = lambda * 180 / Math.PI;
            //double height = h;

            //phi=phi*Math.PI/180;
            //lambda = lambda*Math.PI/180;
            double cl = cos(lambda);
            double sl = sin(lambda);
            double cb = cos(phi);
            double sb = sin(phi);

            RealMatrix F = MatrixUtils.createRealMatrix(new double[][]{
                    {-sl, -sb * cl, cb * cl},
                    {cl, -sb * sl, cb * sl},
                    {0, cb, sb}
            });

            RealMatrix m = MatrixUtils.createRealMatrix(new double[][]{
                    {x - stationsHM.get(datum.getStation().getId()).getX()},
                    {y - stationsHM.get(datum.getStation().getId()).getY()},
                    {z - stationsHM.get(datum.getStation().getId()).getZ()}
            });

            RealMatrix local_vect = F.transpose().multiply(m);


            double e = local_vect.getEntry(0, 0);
            double n = local_vect.getEntry(1, 0);
            double u = local_vect.getEntry(2, 0);

            datum.setX(e * 1000);
            datum.setY(n * 1000);
            datum.setZ(u * 1000);
            datum.setEpoch(datum.getEpoch());
        }

        return data;
    }



    public static HashMap<String, Double> getLambdaPhi(double x, double y, double z){

        double WGS84_A = 6378137;
        double WGS84_B = 6356752.31425;
        double WGS84_F = 1 / 298.257223563;

        double _1e2 = (WGS84_A * WGS84_A - WGS84_B * WGS84_B) / (WGS84_A * WGS84_A); // 1st eccentricity squared
        double _2e2 = (WGS84_A * WGS84_A - WGS84_B * WGS84_B) / (WGS84_B * WGS84_B); // 2nd eccentricity squared
        double p = sqrt(x * x + y * y); // distance from minor axis
        double R = sqrt(p * p + z * z); // polar radius

        // parametric latitude
        double tan_beta = (WGS84_B * z) / (WGS84_A * p) * (1 + _2e2 * WGS84_B / R);
        double sin_beta = tan_beta / sqrt(1 + tan_beta * tan_beta);
        double cos_beta = sin_beta / tan_beta;

        // geodetic latitude (Bowring eqn 18)
        double phi = atan2(z + _2e2 * WGS84_B * sin_beta * sin_beta * sin_beta, p - _1e2 * WGS84_A * cos_beta * cos_beta * cos_beta);

        // longitude
        double lambda = atan2(y, x);
        

        // height above ellipsoid
        double sin_phi = sin(phi);
        double cos_phi = cos(phi);
        double v = WGS84_A / sqrt(1 - _1e2 * sin_phi * sin_phi);
        double h = p * cos_phi + z * sin_phi - (WGS84_A * WGS84_A / v);

        double latitude = phi * 180 / Math.PI;
        double longitude = lambda * 180 / Math.PI;
        double height = h;

        //phi=phi*Math.PI/100;
        //lambda = lambda*Math.PI/180;
       // double cl = cos(lambda);  double sl = sin(lambda);
        //double cb = cos(phi);	  double sb = sin(phi);


        HashMap<String, Double> hmap = new HashMap<String, Double>();

        hmap.put("lambda", lambda);
        hmap.put("phi", phi);

        return hmap;
    }

    /**
     * Transforms a vector XYZ to a local frame ENU
     * Author: José Manteigueiro & Machiel Bos
     * Date: 17/07/2018
     *
     * If we use reference_position_x,y,z it's more accurate
     * but since they change between solutions and this isn't sensitive
     * we can just use the lat,lon from coordinates table
     * X->E
     * Y->N
     * Z->U
     */

    static ArrayList<Estimated_coordinates> XYZvector2ENUlocalframe(ArrayList<Estimated_coordinates> ec, Coordinates stationCoordinates){

        ArrayList<Estimated_coordinates> results = new ArrayList<>();

        double radian = Math.atan(1.0)/45.0;

        double lon = stationCoordinates.getLon() * radian;
        double lat = stationCoordinates.getLat() * radian;

        double cl = Math.cos(lon);
        double sl  = Math.sin(lon);
        double ct  = Math.cos(lat);
        double st  = Math.sin(lat);

        for (Estimated_coordinates anEc : ec) {
            double dX = anEc.getX();
            double dY = anEc.getY();
            double dZ = anEc.getZ();

            // Rotate and convert metres to millimetres
            double e = (-sl * dX) + (cl * dY);
            double n = (-cl * st * dX) - (st * sl * dY) + (ct * dZ);
            double u = (cl * ct * dX) + (ct * sl * dY) + (st * dZ);

            Estimated_coordinates ec_aux = new Estimated_coordinates();
            ec_aux.setX(e);
            ec_aux.setY(n);
            ec_aux.setZ(u);
            ec_aux.setEpoch(anEc.getEpoch());
            results.add(ec_aux);
        }

        return results;
    }
}
