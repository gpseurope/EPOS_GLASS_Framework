/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Glass;

import Configuration.DBConsts;
import Configuration.SiteConfig;
import CustomClasses.ProductDataAvailability;
import CustomClasses.TectonicPlate;
import CustomClasses.VelocityByPlateAndAc;
import CustomClasses.Geobox;
import CustomClasses.VelocityField;
import EposTables.*;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.net.SocketException;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.core.Response;

import static java.lang.Math.*;
import static java.lang.StrictMath.sqrt;

/**
 *
 * @author crocker, Andre Rodrigues, Rafael Couto
 */

public class DataBaseT4Connection {

    @EJB
    SiteConfig siteConfig;

/*
     * GET PRODUCTS COMBINED : For the EposDemonstrator
     * This method retuns a ArrayList with all the products with the given parameter.
     * The Possible parameters are
    1 - a list of station Markers
    2 - a Date Range
     */


    //these should be initialized only once
    //i will try and use an app wide bean approach so that
    //other threads T1/T2/t3 .. can copy the same approach
    private String myIP = null;     // This String will store the ip address of the machine
    private String DBConnectionString = null;

    private Statement s_station = null;         // Statement for the file station queries on the EPOS database
    private ResultSet rs_station = null;        // Result Set for the file station queries on the EPOS database
    private Statement s_estimated_coord = null; // Statement for the estimated coordinates queries on the EPOS database
    private ResultSet rs_estimated_coord = null;// Result Set for the estimated coordinates queries on the EPOS database
    private Statement s_method_ident = null;    // Statement for the method identification queries on the EPOS database
    private ResultSet rs_method_ident = null;   // Result Set for the method identification queries on the EPOS database
    private Statement s_software = null;        // Statement for the software queries on the EPOS database
    private ResultSet rs_software = null;       // Result Set for the software queries on the EPOS database
    private Statement s_ref_frame = null;       // Statement for the reference frame queries on the EPOS database
    private ResultSet rs_ref_frame = null;      // Result Set for the reference frame queries on the EPOS database
    private Statement s_sol_network = null;     // Statement for the solution network queries on the EPOS database
    private ResultSet rs_sol_network = null;    // Result Set for the solution network queries on the EPOS database
    private Statement s_helmert = null;         // Statement for the helmert queries on the EPOS database
    private ResultSet rs_helmert = null;        // Result Set for the helmert queries on the EPOS database
    private Statement s_outlier = null;         // Statement for the outlier queries on the EPOS database
    private ResultSet rs_outlier = null;        // Result Set for the outlier queries on the EPOS database
    private Statement s_station_type = null;    // Statement for the station type queries on the EPOS database
    private ResultSet rs_station_type = null;   // Result Set for the station type queries on the EPOS database
    private Statement s_agency = null;          // Statement for the agency queries on the EPOS database
    private ResultSet rs_agency = null;         // Result Set for the agency queries on the EPOS database

    public DataBaseT4Connection() {
        this.siteConfig = lookupSiteConfigBean();
        myIP = //"10.0.7.65";
                siteConfig.getLocalIpValue();
        DBConnectionString = siteConfig.getDBConnectionString();
    }

    // =========================================================================
    // IP & CONNECTION WITH DATABASE
    // =========================================================================

    protected Connection NewConnection() throws SocketException, ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        Properties props = new Properties();
        props.setProperty("user",DBConsts.DB_USERNAME);
        props.setProperty("password",DBConsts.DB_PASSWORD);
        Connection c = DriverManager.getConnection(DBConnectionString, props);
        c.setAutoCommit(false);
        return c;
    }

    protected SiteConfig lookupSiteConfigBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    // =========================================================================
    // T4 METHODS
    // =========================================================================

    public ArrayList getProductsTimeSeries(String request) {
        Connection c = null;
        ArrayList<Estimated_coordinates> result = null;
        try {

            result = new ArrayList<>();
            ProductRequest request_holder = new ProductRequest(request);
            // Parse request
            request_holder.parseRequest();

            // *********************************************************************
            // MARKER
            // *********************************************************************
            // Check marker
            Set<Integer> stationIds_marker2 = new HashSet<>();
            String query = null;
            c = NewConnection();

            for (int index = 0; index < request_holder.marker_list.size(); index++) {
                query = request_holder.marker_list.get(index) ;
                stationIds_marker2.addAll(executeIDQueryILIKE(query, c));
            }

            // Merge results
            // In this case there are no results to merger...
            Set<Integer> merged_Ids2 = new HashSet<>();
            merged_Ids2.addAll(stationIds_marker2);

            // Create station query
            if (merged_Ids2.size() > 0) {
                query = "SELECT * FROM estimated_coordinates WHERE ";

                Iterator<Integer> iterator = merged_Ids2.iterator();
                query = query + "id_station=" + iterator.next();
                while (iterator.hasNext()) {
                    query = query + " OR id_station=" + iterator.next();
                }


                /*
                    Check if a Date Range has Been Included
                */

                // Check epoch
                if (    request_holder.date_start_list.size() == 1 &&
                        request_holder.date_end_list.size() == 1)
                {
                    query += " AND epoch>='"
                            + request_holder.date_start_list.get(0) +
                            "' AND epoch<='"
                            + request_holder.date_end_list.get(0) + "' ";
                }

                query = query + " order by id_station, epoch;";

                //Execute the Estimated Coordinates query
                result = executeEstimatedCoordinates(query, c);
            }
            return result;

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;

    }

    private ArrayList executeIDQueryILIKE(String query, Connection c) throws SQLException {

        ArrayList<Integer> idList = new ArrayList<>();

        // *********************************************************************
        // STEP 2
        // Check data from query.
        // *********************************************************************
        // Initialized statement for select

        PreparedStatement prepS =
                c.prepareStatement("SELECT * FROM station WHERE marker ILIKE ? ORDER BY id;");
        prepS.setString(1, query + "%");

        ResultSet rs = prepS.executeQuery();

        // Check if there are any results
        while (rs.next()) {
            idList.add(rs.getInt("id"));
        }

        rs.close();
        prepS.close();

        return idList;
    }




    private ArrayList executeQueryGetStationsByAnalysisCentre(String query, Connection c) throws SQLException {

        ArrayList<Integer> idList = new ArrayList<>();

        // *********************************************************************
        // STEP 2
        // Check data from query.
        // *********************************************************************
        // Initialized statement for select

        PreparedStatement prepS = c.prepareStatement(   "select distinct (estimated_coordinates.id_station), analysis_centers.abbreviation from estimated_coordinates" +
                "   inner join product_files on estimated_coordinates.id_product_files = product_files.id" +
                "   inner join analysis_centers on product_files.id_analysis_centers = analysis_centers.id" +
                "   WHERE analysis_centers.abbreviation ILIKE ?;");

        prepS.setString(1, query+"%");
        ResultSet rs = prepS.executeQuery();

        // Check if there are any results
        while (rs.next()) {
            idList.add(rs.getInt("id_station"));
        }

        rs.close();
        prepS.close();

        return idList;
    }

    /**
     * Get estimated coordinates by
     * @param request_holder
     * @param c
     * @return
     */
    private ArrayList<Estimated_coordinates> executeQueryGetEstimatedCoordinates(ProductRequest request_holder, Connection c) throws SQLException {

        // *********************************************************************
        // START BUILDING QUERY
        // *********************************************************************
        String query = "";

        //ADD CONDITION FOR EPOCH START
        if (request_holder.epoch_start != null)
            query += " AND estimated_coordinates.epoch >= '" + request_holder.epoch_start + "'";

        //ADD CONDITION FOR EPOCH END
        if (request_holder.epoch_end != null)
                query += " AND estimated_coordinates.epoch <= '" + request_holder.epoch_end + "'";

        //ADD CONDITION FOR OUTLIER
        query += " AND estimated_coordinates.outlier = " + request_holder.outliers_apply + "";

        //ADD CONDITION FOR SAMPLING PERIOD
        query += " AND processing_parameters.sampling_period = '" + request_holder.sampling_period + "'";

        //GET STATION ID
            PreparedStatement prepS = c.prepareStatement(   "SELECT id FROM station WHERE marker ILIKE ?");
            prepS.setString(1, request_holder.site_list.get(0)+"%");
            ResultSet rs = prepS.executeQuery();

            int id_station = -1;
            if (rs.next())
                id_station = rs.getInt("id");

            rs.close();
            prepS.close();

        //GET AGENCY ID
            prepS = c.prepareStatement(   "SELECT id FROM analysis_centers WHERE name ILIKE ?");
            prepS.setString(1, request_holder.agency_list.get(0)+"%");
            rs = prepS.executeQuery();

            int id_analysis_centers = -1;
            if (rs.next())
                id_analysis_centers = rs.getInt("id");

            rs.close();
            prepS.close();

        prepS = c.prepareStatement(
            "SELECT epoch, x, y, z, var_xx, var_yy, var_zz, var_xy, var_xz, var_yz, id_method_identification FROM estimated_coordinates" +
                    " INNER JOIN method_identification ON method_identification.id = estimated_coordinates.id_method_identification" +
                    " INNER JOIN processing_parameters ON processing_parameters.id = estimated_coordinates.id_processing_parameters" +
                    " WHERE id_station = ?" +
                    " AND method_identification.id_analysis_centers = ? " +
                    query +
                    " ORDER BY epoch");

        prepS.setInt(1, id_station);
        prepS.setInt(2, id_analysis_centers);
        rs = prepS.executeQuery();


        ArrayList<Estimated_coordinates> results = new ArrayList<>();

        // Check if there are any results
        while (rs.next()) {
            // Create Estimated Coordinates object
            Estimated_coordinates est_coord = new Estimated_coordinates();

            est_coord.setEpoch(rs.getString("epoch"));
            est_coord.setX(rs.getDouble("x"));
            est_coord.setY(rs.getDouble("y"));
            est_coord.setZ(rs.getDouble("z"));
            est_coord.setVar_xx(rs.getDouble("var_xx"));
            est_coord.setVar_yy(rs.getDouble("var_yy"));
            est_coord.setVar_zz(rs.getDouble("var_zz"));
            est_coord.setVar_xy(rs.getDouble("var_xy"));
            est_coord.setVar_xz(rs.getDouble("var_xz"));
            est_coord.setVar_yz(rs.getDouble("var_yz"));

            PreparedStatement prepS2 = c.prepareStatement(   "SELECT id FROM method_identification WHERE id = ?");
            prepS2.setInt(1, rs.getInt("id_method_identification"));
            ResultSet rs2 = prepS2.executeQuery();

            if (rs2.next()) {
                Method_identification method_id = new Method_identification();
                method_id.setId(rs2.getInt("id"));
                est_coord.setMethod_identification(method_id);
            }

            rs2.close();
            prepS2.close();

            // Add to results
            results.add(est_coord);
        }

        rs.close();
        prepS.close();


        if (request_holder.offsets_apply) {
            for (Iterator<Estimated_coordinates> i = results.iterator(); i.hasNext();) {
                Estimated_coordinates item = i.next();

                prepS = c.prepareStatement(
                        "SELECT dx, dy, dz FROM jump" +
                                " WHERE id_station = ?" +
                                " AND id_method_identification = ? " +
                                " AND epoch = '"+item.getEpoch()+"'"
                                );

                prepS.setInt(1, id_station);
                prepS.setInt(2, item.getMethod_identification().getId());
                rs = prepS.executeQuery();

                int dx = 0, dy = 0 , dz = 0;
                if (rs.next()) {
                    dx = rs.getInt("dx");
                    dy = rs.getInt("dy");
                    dz = rs.getInt("dz");
                }

                rs.close();
                prepS.close();

                item.setX(item.getX() + dx);
                item.setY(item.getY() + dy);
                item.setZ(item.getZ() + dz);

            }
        }

        return results;

    }

    /**
     * Checks if station and agency are related
     * @param station site name
     * @param agency agency name
     * @param c connection handler
     * @return true if they are related, false otherwise
     * @throws SQLException
     */
    public boolean executeQueryConfirmStationAndAgency(String station, String agency, Connection c) throws SQLException {

        // *********************************************************************
        // STEP 2
        // Check data from query.
        // *********************************************************************
        // Initialized statement for select

        PreparedStatement prepS = c.prepareStatement(   "SELECT analysis_centers.name FROM station " +
                " inner join station_contacts on station_contacts.id_station = station.id" +
                " inner join contact on contact.id = station_contacts.id_contact" +
                " inner join analysis_centers on analysis_centers.id = contact.analysis_centers" +
                " WHERE station.marker ILIKE ? AND" +
                " analysis_centers.name ILIKE ? ;");

        prepS.setString(1, station+"%");
        prepS.setString(2, agency+"%");
        ResultSet rs = prepS.executeQuery();

        boolean ret = false;
        if (rs.next())
            ret = true;

        rs.close();
        prepS.close();

        return ret;
    }


    private ArrayList executeQueryGetStationsByAntennaModel(String query, Connection c) throws SQLException {

        ArrayList<Integer> idList = new ArrayList<>();

        // *********************************************************************
        // STEP 2
        // Check data from query.
        // *********************************************************************
        // Initialized statement for select

        PreparedStatement prepS = c.prepareStatement(   "" +
                        "SELECT id_analysis_centers FROM estimated_coordinates AS ec " +
                            " INNER JOIN processing_parameters AS pp ON ec.id_processing_parameters = pp.id " +
                            " WHERE pp.antenna_model LIKE ?"
        );
        prepS.setString(1, query+"%");
        ResultSet rs = prepS.executeQuery();

        // Check if there are any results
        while (rs.next()) {
            idList.add(rs.getInt("station_id"));
        }

        rs.close();
        prepS.close();

        return idList;
    }

    private ArrayList executeQueryGetStationsByOTLModel(String query, Connection c) throws SQLException {

        ArrayList<Integer> idList = new ArrayList<>();

        // *********************************************************************
        // STEP 2
        // Check data from query.
        // *********************************************************************
        // Initialized statement for select

        Statement s = c.createStatement();

        String sqlStatement= "" +
                "SELECT id_station FROM estimated_coordinates AS ec " +
                "INNER JOIN processing_parameters as pp on ec.id_processing_parameters = pp.id " +
                "WHERE pp.otl_model = '" + query + "';";

        ResultSet rs = s.executeQuery(sqlStatement);

        // Check if there are any results
        while (rs.next()) {
            idList.add(rs.getInt("id_station"));
        }

        rs.close();
        s.close();

        return idList;
    }

    private ArrayList executeQueryGetStationsByReferenceFrame(String query, Connection c) throws SQLException {

        ArrayList<Integer> idList = new ArrayList<>();

        // *********************************************************************
        // STEP 2
        // Check data from query.
        // *********************************************************************
        // Initialized statement for select

        PreparedStatement prepS = c.prepareStatement(   "" +
                "SELECT DISTINCT id_station FROM estimated_coordinates AS ec " +
                "INNER JOIN method_identification AS mi ON ec.id_method_identification = mi.id " +
                "INNER JOIN reference_frame AS rf ON mi.id_reference_frame = rf.id " +
                "WHERE rf.name like ?"
        );

        prepS.setString(1, query+"%");
        ResultSet rs = prepS.executeQuery();

        // Check if there are any results
        while (rs.next()) {
            idList.add(rs.getInt("id_station"));
        }

        rs.close();
        prepS.close();

        return idList;
    }

    private ArrayList executeQueryGetStationsByCutOffAngle(String query, Connection c) throws SQLException {

        ArrayList<Integer> idList = new ArrayList<>();

        Statement s = c.createStatement();
        ResultSet rs = s.executeQuery("" +
                "SELECT id_station FROM estimated_coordinates AS ec " +
                "INNER JOIN processing_parameters AS pp ON ec.id_processing_parameters = pp.id " +
                "WHERE pp.cut_of_angle = " + Float.parseFloat(query) + ";"
        );

        // Check if there are any results
        while (rs.next()) {
            idList.add(rs.getInt("id_station"));
        }

        rs.close();
        s.close();

        return idList;
    }

    private ArrayList executeQueryGetStationsByEpochInterval(String epoch_start, String epoch_end, Connection c) throws SQLException {

        ArrayList<Integer> idList = new ArrayList<>();

        // *********************************************************************
        // STEP 2
        // Check data from query.
        // *********************************************************************
        // Initialized statement for select

        String query = "";

        //ADD CONDITION FOR EPOCH START
        if (epoch_start != null && !epoch_start.equals("")) {
            query += " WHERE epoch >= '" + epoch_start + "'";

            //ADD CONDITION FOR EPOCH END
            if (epoch_end != null && !epoch_end.equals(""))
                query += " AND epoch <= '" +epoch_end + "'";
        }else {

            //ADD CONDITION FOR EPOCH END
            if (epoch_end != null && !epoch_end.equals(""))
                query += " WHERE epoch <= '" + epoch_end + "'";
        }

        PreparedStatement prepS = c.prepareStatement(  "SELECT DISTINCT id_station FROM estimated_coordinates " + query);


        //prepS.setString(1, "'"+epoch_start+"'");
        //prepS.setString(2, "'"+epoch_end+"'");
        ResultSet rs = prepS.executeQuery();

        // Check if there are any results
        while (rs.next()) {
            idList.add(rs.getInt("id_station"));
        }

        rs.close();
        prepS.close();

        return idList;
    }

    private ArrayList executeEstimatedCoordinates(String query, Connection c) throws SQLException {
        ArrayList<Estimated_coordinates> results = new ArrayList<>();

        // Initialize statement for select
        // Execute the Query
        Statement s = c.createStatement();
        ResultSet rs = s.executeQuery(query);

        // Check if there are any results
        while (rs.next()) {
            // Create Estimated Coordinates object
            Estimated_coordinates est_coord = new Estimated_coordinates();
            est_coord.setId(rs.getInt("id"));

            est_coord.setEpoch(rs.getString("epoch"));
            est_coord.setX(rs.getDouble("x"));
            est_coord.setY(rs.getDouble("y"));
            est_coord.setZ(rs.getDouble("z"));
            est_coord.setVar_xx(rs.getDouble("sxx"));
            est_coord.setVar_yy(rs.getDouble("syy"));
            est_coord.setVar_zz(rs.getDouble("szz"));
            est_coord.setVar_xy(rs.getDouble("sxy"));
            est_coord.setVar_xz(rs.getDouble("sxz"));
            est_coord.setVar_yz(rs.getDouble("syz"));

            // Add to results
            results.add(est_coord);
        }

        rs.close();
        s.close();

        //missing try+finally

        return results;
    }

    ArrayList<Station> getStationsBasedOnParameters(String request) {

        Connection c = null;

        try {

            ProductRequest request_holder = new ProductRequest(request);
            // Parse request
            request_holder.parseRequest();

            String query;
            c = NewConnection();



            // *********************************************************************
            // ANALYSIS_CENTRES
            // *********************************************************************
            // Check analysis_centre
            Set<Integer> stationIds_analysisCentres = new HashSet();
            for (int index = 0; index < request_holder.analysis_centre_list.size(); index++) {
                query = request_holder.analysis_centre_list.get(index) ;
                stationIds_analysisCentres.addAll(executeQueryGetStationsByAnalysisCentre(query, c));
            }


            // *********************************************************************
            // ANTENNA MODEL
            // *********************************************************************
            // Check antenna model
            Set<Integer> stationIds_antenna_model = new HashSet();
            for (int index = 0; index < request_holder.antenna_model_list.size(); index++) {
                query = request_holder.antenna_model_list.get(index) ;
                stationIds_antenna_model.addAll(executeQueryGetStationsByAntennaModel(query, c));
            }

            // *********************************************************************
            // OTL MODEL
            // *********************************************************************
            // Check otl model
            Set<Integer> stationIds_otl_model = new HashSet();
            for (int index = 0; index < request_holder.otl_model_list.size(); index++) {
                query = request_holder.otl_model_list.get(index)+"" ;
                stationIds_otl_model.addAll(executeQueryGetStationsByOTLModel(query, c));
            }

            // *********************************************************************
            // REFERENCE FRAME
            // *********************************************************************
            // Check reference frame
            Set<Integer> stationIds_reference_frame = new HashSet();
            for (int index = 0; index < request_holder.reference_frame_list.size(); index++) {
                query = request_holder.reference_frame_list.get(index)+"" ;
                stationIds_reference_frame.addAll(executeQueryGetStationsByReferenceFrame(query, c));
            }

            // *********************************************************************
            // CUT OFF ANGLE
            // *********************************************************************
            // Check cut off angle
            Set<Integer> stationIds_cut_off_angle = new HashSet();
            for (int index = 0; index < request_holder.cut_off_angle_list.size(); index++) {
                query = request_holder.cut_off_angle_list.get(index)+"" ;
                stationIds_cut_off_angle.addAll(executeQueryGetStationsByCutOffAngle(query, c));
            }

            // *********************************************************************
            // EPOCH INTERVAL
            // *********************************************************************
            // Check epoch interval
            Set<Integer> stationIds_epoch_interval = new HashSet();
            if (request_holder.epoch_start != null || request_holder.epoch_end != null) {
                stationIds_epoch_interval.addAll(executeQueryGetStationsByEpochInterval(request_holder.epoch_start, request_holder.epoch_end, c));
            }

            // *********************************************************************
            // COORDINATES
            // *********************************************************************
            // Check coordinates
            DataBaseConnection dbc = new DataBaseConnection();
            Set<Integer> stationIds_coordinates = new HashSet();
            switch (request_holder.coordinates_type)
            {
                // Handle rectangle queries
                case "rectangle":
                    ArrayList<Station> coord_rec = new ArrayList<>();
                    coord_rec = dbc.getStationCoordinates(request_holder.minLat, request_holder.maxLat,
                            request_holder.minLon, request_holder.maxLon);
                    for (Station aCoord_rec : coord_rec) stationIds_coordinates.add(aCoord_rec.getId());
                    break;

                // Handle circle queries
                case "circle":
                    ArrayList<Station> coord_cir = new ArrayList<>();
                    coord_cir = dbc.getStationCoordinates(request_holder.lat, request_holder.lon,
                            request_holder.radius);
                    for (Station aCoord_cir : coord_cir) stationIds_coordinates.add(aCoord_cir.getId());
                    break;

                // Handle polygon queries
                case "polygon":
                    ArrayList<Station> coord_pol = new ArrayList<>();
                    coord_pol = dbc.getStationCoordinates(request_holder.coordinates_list);
                    for (Station aCoord_pol : coord_pol) stationIds_coordinates.add(aCoord_pol.getId());
                    break;
            }

            // Merge results
            // Count valid parameters
            int counter = 0;
            if (request_holder.analysis_centre_list.size() > 0)
                counter++;
            if (request_holder.antenna_model_list.size() > 0)
                counter++;
            if (request_holder.otl_model_list.size() > 0)
                counter++;
            if (request_holder.reference_frame_list.size() > 0)
                counter++;
            if (request_holder.cut_off_angle_list.size() > 0)
                counter++;
            if (!Objects.equals(request_holder.epoch_start, null) || !Objects.equals(request_holder.epoch_end, null))
                counter++;
            if (request_holder.coordinates_type.contentEquals("rectangle") ||
                    request_holder.coordinates_type.contentEquals("circle") ||
                    request_holder.coordinates_type.contentEquals("polygon"))
                counter++;

            // Merge results
            ArrayList<Integer> merged_Ids = new ArrayList<>();
            merged_Ids.addAll(stationIds_analysisCentres);
            merged_Ids.addAll(stationIds_antenna_model);
            merged_Ids.addAll(stationIds_otl_model);
            merged_Ids.addAll(stationIds_reference_frame);
            merged_Ids.addAll(stationIds_cut_off_angle);
            merged_Ids.addAll(stationIds_epoch_interval);
            merged_Ids.addAll(stationIds_coordinates);

            // Check frequency of results
            ArrayList<Integer> stations_Ids = new ArrayList<>();
            for (int i=0; i<merged_Ids.size(); i++)
            {

                int coiso = Collections.frequency(merged_Ids, merged_Ids.get(i));
                if (Collections.frequency(merged_Ids, merged_Ids.get(i)) == counter)
                    if (!stations_Ids.contains(merged_Ids.get(i)))
                        stations_Ids.add(merged_Ids.get(i));
            }

            // Create station query
            ArrayList<Station> results = new ArrayList<>();

            JsonCache mycache = new JsonCache();



            for (int i=0; i<stations_Ids.size(); i++)
            {
                //check in cache
                Station myStation = mycache.get(stations_Ids.get(i));
                if (myStation != null) {
                    if (myStation.getStationNetworks().size()>0
                            && myStation.getLocation().getCity().getState().getCountry().getName() != null) {
                        results.add(myStation);
                    }

                }else{

                    query = "SELECT * FROM station WHERE ";

                    query = query + "id=" + stations_Ids.get(i);

                    query = query + ";";

                    ArrayList<Station> rq = new ArrayList<>();
                    //Execute query
                    rq = dbc.executeStationQuery(query, c);

                    if(rq.size()>0)
                    {
                        results.add(rq.get(0));
                    }
                }
            }


            return results;


        } catch (SocketException | ClassNotFoundException | SQLException | ParseException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public ArrayList<Station> getAllStationsWithProducts() {

        Connection c = null;
        ArrayList<Station> stations;
        ArrayList<Station> results = new ArrayList<>();

        try {

            c = NewConnection();

            //Create query to get all IDs with products
            StringBuilder sqlStatement= new StringBuilder()
                    .append("SELECT ")
                    .append("s.id ")
                    .append("FROM station AS s ")
                    .append("JOIN location AS l ON s.id_location = l.id ")
                    .append("JOIN coordinates AS coo ON l.id_coordinates = coo.id ")
                    .append("JOIN city AS ci ON l.id_city = ci.id ")
                    .append("JOIN state AS st ON ci.id_state = st.id ")
                    .append("JOIN country AS co ON st.id_country = co.id ")
                    .append("JOIN station_contact AS stco ON stco.id_station = s.id ")
                    .append("JOIN contact AS cont ON stco.id_contact = cont.id ")

                    .append("JOIN agency AS ag ON cont.id_agency = ag.id ")
                    .append("JOIN analysis_centers AS ac ON cont.id_agency = ag.id ")

                    .append("JOIN station_network AS stnet ON stnet.id_station = s.id ")
                    .append("JOIN network AS net ON stnet.id_network = net.id ")
                    .append("WHERE s.id IN ( ");
            if (GlassConstants.PRODUCT_OPTIMIZE_1)
                sqlStatement.append("   SELECT DISTINCT id_station FROM view_estimated_coordinates ");
            else
                sqlStatement.append("   SELECT DISTINCT id_station FROM estimated_coordinates ");
            sqlStatement.append(") ")
                    .append("OR s.id IN ( ")
                    .append("   SELECT DISTINCT id_station FROM reference_position_velocities ")
                    .append(") ")
                    .append("GROUP BY ")
                    .append("   s.id;");

            String queryID = sqlStatement.toString();



            //Execute query
            DataBaseConnection dbc = new DataBaseConnection();

            stations = dbc.executeStationShortQueryID(queryID, c);

            //Search results in cache
            JsonCache mycache = new JsonCache();

            if (stations.size() > 0){
                for (Station station : stations){
                    try {
                        //Check in cache
                        Station myStation = mycache.get(station.getId());
                        if (myStation != null)
                            results.add(myStation);
                        else
                            throw new Exception("@889 DataBAseT4Connection Not in cache: " +station.getId());
                    } catch (Exception e) {

                        System.out.println("Error "+e.getMessage());
                    }
                }
            }



        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;

    }

    /*
        Paul: This code needs reviewing
     */
    public ArrayList<Station> checkPowerSpectral(ArrayList<Station> stations) {

        Connection c;
        try {
            c = NewConnection();

            ArrayList<Analysis_Centers> analysis_centers = executeAnalysisCentersQuery("SELECT * FROM analysis_centers;", c);

            for (int i = 0; i < stations.size(); i++) {

                for (Analysis_Centers analysis_center : analysis_centers) {
                    java.io.File f = new File( "/opt/EPOS/power-spectral-density/plots/"
                            + analysis_center.getAbbreviation()
                            + "/psd_figures/" + stations.get(i).getMarker() + "_0.png");

                    if (f.exists() && !f.isDirectory()) {
                        Boolean _controlFlag= false;

                        for (int j = 0; j < stations.get(i).getPower_spectral_density().size(); j++){
                            try {
                                if (stations.get(i).getPower_spectral_density().get(j).getAbbreviation().equals(analysis_center.getAbbreviation())) {
                                    _controlFlag = true;
                                    break;
                                }
                            } catch (NullPointerException _npe){
                                System.out.println("null pointer with i="+i+ " j="+j);
                                continue;
                            }
                        }
                        if (!_controlFlag){

                            try{
                                stations.get(i).getPower_spectral_density().add(analysis_center);
                            } catch (Exception e){
                                System.out.println("null pointer with i="+i+ " size="+stations.get(i).getPower_spectral_density().size());
                                continue;
                            }
                        }
                    }
                }
            }

        } catch (SocketException | ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return stations;
    }



    public ArrayList<Analysis_Centers> processingAgencyFromT4T5(String table, int id_station) {

        Connection c = null;

        try {

            c = NewConnection();
            String query;

            switch (table) {
                case "timeseries":
                    // Create station query
                {
                    query = new StringBuilder().append("SELECT DISTINCT analysis_centers.* FROM analysis_centers ").append("INNER JOIN method_identification ON method_identification.id_analysis_centers = analysis_centers.id ").append("INNER JOIN estimated_coordinates ON estimated_coordinates.id_method_identification = method_identification.id ").append("WHERE estimated_coordinates.id_station = ").append(id_station).append(" AND method_identification.data_type ILIKE 'timeseries' ORDER BY analysis_centers.abbreviation ASC").toString();
                    break;
                }
                case "coordinates":
                    // Create station query
                {
                    query = new StringBuilder().append("SELECT DISTINCT analysis_centers.* FROM analysis_centers ").append("INNER JOIN method_identification ON method_identification.id_analysis_centers = analysis_centers.id ").append("INNER JOIN estimated_coordinates ON estimated_coordinates.id_method_identification = method_identification.id ").append("WHERE estimated_coordinates.id_station = ").append(id_station).append(" AND method_identification.data_type ILIKE 'coordinates' ORDER BY analysis_centers.abbreviation ASC").toString();
                    break;
                }
                case "velocities": {
                    query = new StringBuilder().append("SELECT DISTINCT analysis_centers.* FROM analysis_centers ").append("INNER JOIN method_identification ON method_identification.id_analysis_centers = analysis_centers.id ").append("INNER JOIN reference_position_velocities ON reference_position_velocities.id_method_identification = method_identification.id ").append("WHERE reference_position_velocities.id_station = ").append(id_station).append(" AND method_identification.data_type ILIKE 'velocities' ORDER BY analysis_centers.abbreviation ASC").toString();
                    break;
                }
                default:
                    return null;
            }

            //Execute query
            DataBaseT4Connection dbc = new DataBaseT4Connection();

            return dbc.executeAnalysisCentersQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    /**
     * EXECUTE ANALYSIS CENTER QUERY
     * This method executes a given query on the agencies inserted on the EPOS
     * database and returns a ArrayList with the results.
     */
    ArrayList<Analysis_Centers> executeAnalysisCentersQuery(String query, Connection c) throws SQLException
    {
        ArrayList<Analysis_Centers> results = new ArrayList<>();

        // Initialized statement for select
        Statement s = c.createStatement();
        ResultSet rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Add analysis center to result ArrayList
            results.add(new Analysis_Centers(
                    rs.getInt("id"),
                    rs.getString("abbreviation"),
                    rs.getString("name"),
                    rs.getString("contact"),
                    rs.getString("url"),
                    rs.getString("email")));
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    ArrayList<Station> getAllStationsWithProducts(String minLat, String maxLat, String minLon, String maxLon) {

        Connection c = null;
        ArrayList<Station> stations = new ArrayList<>();
        ArrayList<Station> results = new ArrayList<>();
        ArrayList<Station> results2 = new ArrayList<>();


        try {

            c = NewConnection();


            //Create query to get all IDs with products
            StringBuilder sqlStatement = new StringBuilder()
                    .append("SELECT ")
                    .append("s.id ")
                    .append("FROM station AS s ")
                    .append("JOIN location AS l ON s.id_location = l.id ")
                    .append("JOIN coordinates AS coo ON l.id_coordinates = coo.id ")
                    .append("JOIN city AS ci ON l.id_city = ci.id ")
                    .append("JOIN state AS st ON ci.id_state = st.id ")
                    .append("JOIN country AS co ON st.id_country = co.id ")
                    .append("JOIN station_contact AS stco ON stco.id_station = s.id ")
                    .append("JOIN contact AS cont ON stco.id_contact = cont.id ")
                    .append("JOIN agency AS ag ON cont.id_agency = ag.id ")
                    .append("JOIN station_network AS stnet ON stnet.id_station = s.id ")
                    .append("JOIN network AS net ON stnet.id_network = net.id ")
                    .append("WHERE (s.id IN ( ");
            if (GlassConstants.PRODUCT_OPTIMIZE_1)
                sqlStatement.append("   SELECT DISTINCT id_station FROM view_estimated_coordinates ");
            else
                sqlStatement.append("   SELECT DISTINCT id_station FROM estimated_coordinates ");
            sqlStatement.append(") ")
                    .append("OR s.id IN ( ")
                    .append("   SELECT DISTINCT id_station FROM reference_position_velocities ")
                    .append(")) ")
                    .append("AND coo.lat >= '")
                    .append(minLat)
                    .append("' AND coo.lat <= '")
                    .append(maxLat)
                    .append("' ")
                    .append("AND coo.lon >= '")
                    .append(minLon)
                    .append("' AND coo.lon <= '")
                    .append(maxLon)
                    .append("' ")
                    .append("GROUP BY ")
                    .append("   s.id;");

            String queryID=sqlStatement.toString();

            //Execute query
            DataBaseConnection dbc = new DataBaseConnection();
            stations = dbc.executeStationShortQueryID(queryID, c);


            //Search results in cache
            JsonCache mycache = new JsonCache();

            if (stations.size() > 0){
                for (Station station : stations){
                    try {
                        //Check in cache
                        Station myStation = mycache.get(station.getId());
                        if (myStation != null)
                            results.add(myStation);
                        else
                            throw new Exception("@115 DataBaseT4Connection Not in cache: "+station.getId());
                    } catch (Exception e) {
                        //Execute the query
                        try {

                            // Create station query

                             sqlStatement= new StringBuilder()
                            .append("SELECT ")
                            .append("s.id, s.marker, s.name, s.date_from, s.date_to, ")
                            .append("coo.lat, coo.lon, coo.altitude, ")
                            .append("co.name AS country, ")
                            .append("st.name AS state, ")
                            .append("ci.name AS city, ")
                            .append("markerlongname(s.marker, s.monument_num, s.receiver_num, s.country_code ), ")
                            .append("array_to_string(array_agg(distinct(ag.name)), ' ') AS agency, ")
                            .append("array_to_string(array_agg(distinct(net.name)), ' ') AS network ")
                            .append("FROM station AS s ")
                            .append("JOIN location AS l ON s.id_location = l.id ")
                            .append("JOIN coordinates AS coo ON l.id_coordinates = coo.id ")
                            .append("JOIN city AS ci ON l.id_city = ci.id ")
                            .append("JOIN state AS st ON ci.id_state = st.id ")
                            .append("JOIN country AS co ON st.id_country = co.id ")
                            .append("JOIN station_contact AS stco ON stco.id_station = s.id ")
                            .append("JOIN contact AS cont ON stco.id_contact = cont.id ")
                            .append("JOIN agency AS ag ON cont.id_agency = ag.id ")
                            .append("JOIN station_network AS stnet ON stnet.id_station = s.id ")
                            .append("JOIN network AS net ON stnet.id_network = net.id ")
                            .append("WHERE (s.id IN ( ");
                         if (GlassConstants.PRODUCT_OPTIMIZE_1)
                             sqlStatement.append("   SELECT DISTINCT id_station FROM view_estimated_coordinates ");
                        else
                             sqlStatement.append("   SELECT DISTINCT id_station FROM estimated_coordinates ");
                        sqlStatement .append(") ")
                            .append("OR s.id IN ( ")
                            .append("   SELECT DISTINCT id_station FROM reference_position_velocities ")
                            .append(")) ")
                            .append("AND coo.lat >= '")
                            .append(minLat)
                            .append("' AND coo.lat <= '")
                            .append(maxLat)
                            .append("' ")
                            .append("AND coo.lon >= '")
                            .append(minLon)
                            .append("' AND coo.lon <= '")
                            .append(maxLon)
                            .append("' ")
                            .append("GROUP BY ")
                            .append("   s.id, s.marker, s.name, s.date_from, s.date_to, ")
                            .append("   coo.lat, coo.lon, coo.altitude, ")
                            .append("   co.name, ")
                            .append("   st.name, ")
                            .append("   ci.name;");

                            String query = sqlStatement.toString();

                            results = dbc.executeStationShortQuery(query, c);
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }
                        break;
                    }
                }
            }

//
//            for (Station station : results) {
//                ArrayList<VelocityField> vf = VelocitiesField("site:" + station.getMarker());
//                if (vf.size() == 0) {
//                    station.setAngle(0.0);
//                    station.setVectorLength(0.0);
//                } else {
//                    station.setAngle(vf.get(0).getAngle());
//                    station.setVectorLength(vf.get(0).getLenghtvector());
//                }
//
//                //Processing agency from estimated coordinates and reference position velocities
//                station.setPA_timeseries(processingAgencyFromT4T5("timeseries", station.getId()));
//                station.setPA_velocities(processingAgencyFromT4T5("velocities", station.getId()));
//                station.setPA_coordinates(processingAgencyFromT4T5("coordinates", station.getId()));
//
//                results2.add(station);
//            }


        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;

    }

    //Confirm if the given station and agency are related

    /*public boolean executeQueryConfirmStationAndAgency(String station, String agency) throws SQLException {

        Connection c = null;
        try {
            c = NewConnection();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        PreparedStatement prepS = c.prepareStatement(   "SELECT agency.name FROM station " +
                " inner join station_contacts on station_contacts.id_station = station.id" +
                " inner join contact on contact.id = station_contacts.id_contact" +
                " inner join agency on agency.id = contact.id_agency" +
                " WHERE station.marker ILIKE ? AND" +
                " agency.name ILIKE ? ;");


        prepS.setString(1, station + "%");
        prepS.setString(2, agency + "%");
        ResultSet rs = prepS.executeQuery();


        boolean ret = false;
        if (rs.next())
            ret = true;

        rs.close();
        prepS.close();

        return ret;
    }*/




    /**
     * Returns the xyz coordinates
     *
     * @return            ArrayList with x, y, z, xx, yy, zz, xy, xz, yz and epoch
     * @author            Andre Rodrigues
     *
     */
    protected ArrayList<Estimated_coordinates>
    getTimeseriesXYZBasedOnParameters(String site, String analysis_centre, String sampling_period, String format, String reference_frame, String otl_model, String antenna_model, String cut_of_angle,String epoch_start, String epoch_end, String remove_outliers, String apply_offsets, String data_type, String version) throws SQLException {

        boolean first_obs;
        int id_ac=-1,id_mi=-1,mi_id_reference_frame=-1;
        String query = "";
        double  versionNumber=-1;
        boolean versionFlag=false;

        Analysis_Centers analysis_centers_obj = new Analysis_Centers();
        Method_identification method_identification_obj = new Method_identification();
        Reference_frame reference_frame_obj = new Reference_frame();
        //--- The result of the query is stored in array 'results'
        ArrayList<Estimated_coordinates> results = new ArrayList<>();

        //ADD CONDITION FOR VERSION
        if (version != null && !version.equals("")){

            //parse the version number here in case a bad number has been passed
            try{
                versionNumber = Double.parseDouble(version);
            }catch ( NumberFormatException ex ){
                Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, "version format Error "+version, ex);
                return results;
            }
            versionFlag=true;

            query += " AND method_identification.version = " + version;
        }

        //ADD CONDITION FOR EPOCH START
        if (epoch_start != null && !epoch_start.equals(""))
            query += " AND estimated_coordinates.epoch >= '" + epoch_start + "'";

        //ADD CONDITION FOR EPOCH END
        if (epoch_end != null && !epoch_end.equals(""))
            query += " AND estimated_coordinates.epoch <= '" +epoch_end + "'";

        //ADD CONDITION FOR OUTLIER
        if(remove_outliers != null && (remove_outliers.equals("true") || remove_outliers.equals("1") || remove_outliers.equals(""))) {
            //it means that outliers are removed
            query += " AND estimated_coordinates.outlier = " + "0" + "";
        }
        else
        {
            if (!remove_outliers.equals("false") && !remove_outliers.equals("0")) {

                return null;
            }
            //else, if false or 0 were given, it returns data with and without outliers, so 0 and 1 on the database
        }


        //ADD CONDITION FOR PROCESSING_PARAMETERS PARAMETERS
        if (sampling_period != null && !sampling_period.equals("")) {
                query += " AND id_processing_parameters IN (SELECT id FROM processing_parameters WHERE sampling_period = '" + sampling_period + "' ";

            if (cut_of_angle != null && !cut_of_angle.equals(""))
                    query += " AND cut_of_angle <= '" + cut_of_angle + "' ";

            if (otl_model != null && !otl_model.equals(""))
                    query += " AND otl_model = '" + otl_model + "' ";

            if(antenna_model != null && !antenna_model.equals(""))
                    query += " AND antenna_model = '" + antenna_model + "' ";

            query += " ) ";
        } else {
            return results; //Returns empty because sampling period wasn't set.
        }

        if (reference_frame != null && !reference_frame.equals(""))
            query += " AND method_identification.id_reference_frame = (SELECT id FROM reference_frame WHERE name = '" + reference_frame + "')";

        //GET STATION ID
        Connection c = null;
        try {
            int id_station = -1;

            //--- Connect to Product DB
            c = NewConnection();

            //--- Perform a query to get meta-data concerning analysis/combination centre
            PreparedStatement prepS = c.prepareStatement(   "SELECT * FROM analysis_centers WHERE name ILIKE ? OR abbreviation ILIKE ?");
            prepS.setString(1, "%"+analysis_centre+"%");
            prepS.setString(2, analysis_centre);

            ResultSet rs = prepS.executeQuery();

            if (rs.next()) {
                id_ac = rs.getInt("id");
                analysis_centers_obj.setId(id_ac);
                analysis_centers_obj.setName(rs.getString("name"));
                analysis_centers_obj.setAbbreviation(rs.getString("abbreviation"));
                analysis_centers_obj.setContact(rs.getString("contact"));
                analysis_centers_obj.setEmail(rs.getString("email"));
                analysis_centers_obj.setUrl(rs.getString("url"));
            }

            rs.close();
            prepS.close();

            //--- Perform a query to get meta-data concerning method_identification

            // If there is no version, choose the latest one
            String additional_mi_info = "";
            if(versionFlag){
                additional_mi_info = " AND mi.version =  "+version;
            }else{
                additional_mi_info = " ORDER BY mi.version DESC LIMIT 1 ";
            }


            prepS = c.prepareStatement(   "SELECT * FROM method_identification mi WHERE mi.id_analysis_centers="
                            + id_ac + " AND mi.data_type='" + data_type + "' " + additional_mi_info );

            rs = prepS.executeQuery();

            if (rs.next()) {
                id_mi = rs.getInt("id");
                method_identification_obj.setId(id_mi);
                mi_id_reference_frame = rs.getInt("id_reference_frame");
                method_identification_obj.setCreation_date(rs.getString("creation_date"));
                method_identification_obj.setVersion(Double.parseDouble(rs.getString("version")));
                method_identification_obj.setDoi(rs.getString("doi"));
                method_identification_obj.setSoftware(rs.getString("software"));
            }

            rs.close();
            prepS.close();

            // Add Analysis Centre to this Method Identification object
            method_identification_obj.setAnalysis_center(analysis_centers_obj);

            //--- Perform a query to Reference Frame info
            prepS = c.prepareStatement(   "SELECT * FROM reference_frame WHERE id="
                                                                + mi_id_reference_frame + ";");
            rs = prepS.executeQuery();

            if (rs.next()) {
                reference_frame_obj.setName(rs.getString("name"));
                reference_frame_obj.setEpoch(rs.getString("epoch"));
                reference_frame_obj.setId(mi_id_reference_frame);
                method_identification_obj.setReference_frame(reference_frame_obj);
            }

            rs.close();
            prepS.close();

            prepS = c.prepareStatement(
                    "SELECT x, y, z, var_xx, var_yy, var_zz, var_xy, var_xz, var_yz, epoch, id_station, " +
                            " method_identification.id as mi_id, method_identification.creation_date as mi_cd " +
                            " FROM estimated_coordinates " +
                            " INNER JOIN method_identification on method_identification.id = estimated_coordinates.id_method_identification " +
                            " WHERE id_station = (SELECT id FROM station WHERE marker ILIKE ?) " +
                            " AND method_identification.id = ? " +
                            query +
                            " ORDER BY epoch");


            prepS.setString(1, site);
            prepS.setInt(2, id_mi);
            rs = prepS.executeQuery();

            // Check if there are any results
            first_obs = true;
            while (rs.next()) {

                id_station = rs.getInt("id_station");
                // Create Estimated Coordinates object
                Estimated_coordinates est_coord = new Estimated_coordinates();
                    est_coord.setStation(new Station(id_station, "", "", "", "", "", "", "", "", "", 0, 0, ""));
                    est_coord.setX(rs.getDouble("x"));
                    est_coord.setY(rs.getDouble("y"));
                    est_coord.setZ(rs.getDouble("z"));
                    est_coord.setVar_xx(rs.getDouble("var_xx"));
                    est_coord.setVar_yy(rs.getDouble("var_yy"));
                    est_coord.setVar_zz(rs.getDouble("var_zz"));
                    est_coord.setVar_xy(rs.getDouble("var_xy"));
                    est_coord.setVar_xz(rs.getDouble("var_xz"));
                    est_coord.setVar_yz(rs.getDouble("var_yz"));
                    est_coord.setEpoch(rs.getString("epoch"));

                    if (first_obs) {
                        est_coord.setMethod_identification(method_identification_obj);
                        first_obs=false;
                    } else {
                        est_coord.setMethod_identification(null);
                    }
                // Add to results
                results.add(est_coord);
            }

            rs.close();
            prepS.close();

            //If there are no timeseries don't worry about anything else!
            if (results.isEmpty())
                return results;


            if(apply_offsets != null && !apply_offsets.equals("")) {

                if (apply_offsets.equals("1") || apply_offsets.equals("true")) {

                    for (Estimated_coordinates est_coord : results) {
                        prepS = c.prepareStatement(
                                "SELECT dx, dy, dz FROM jump" +
                                        " WHERE id_station = ?" +
                                        " AND id_method_identification = ? " +
                                        " AND epoch = '" + est_coord.getEpoch() + "'"
                        );

                        prepS.setInt(1, id_station);
                        prepS.setInt(2, est_coord.getMethod_identification().getId());
                        rs = prepS.executeQuery();

                        int dx = 0, dy = 0, dz = 0;
                        if (rs.next()) {
                            dx = rs.getInt("dx");
                            dy = rs.getInt("dy");
                            dz = rs.getInt("dz");
                        }

                        rs.close();
                        prepS.close();

                        est_coord.setEpoch(est_coord.getEpoch());
                        est_coord.setX(est_coord.getX() + dx);
                        est_coord.setY(est_coord.getY() + dy);
                        est_coord.setZ(est_coord.getZ() + dz);
                    }
                }
            }

        } catch (Exception ex) {

            System.out.println("Error "+ex.getMessage());
            Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }


        return results;

    }

    /**
     * Returns the enu coordinates
     *
     * @return            ArrayList with enu coordinates. Call xyz2enu method
     * @author            Andre Rodrigues
     *
     */
    ArrayList<Estimated_coordinates> getTimeseriesENUBasedOnParameters(String site, String analysis_centre, String sampling_period, String format, String reference_frame, String otl_model, String antenna_model, String cut_of_angle, String epoch_start, String epoch_end, String remove_outliers, String apply_offsets, String data_type, String version) throws SQLException {

        HashMap<Integer, Coordinates> stationsHM = new HashMap<>();

        Connection _c = null;
        PreparedStatement _ps = null;
        ResultSet _rs = null;
        try {
            _c = NewConnection();

            //for (Station station : getAllStationsWithProducts()) {

            _ps = _c.prepareStatement("SELECT reference_position_x, reference_position_y, reference_position_z, id_station FROM reference_position_velocities WHERE id_station = (SELECT id FROM station WHERE marker = ?) ORDER BY ref_epoch DESC LIMIT 1;");
            _ps.setString(1, site);
            _rs = _ps.executeQuery();
            Coordinates _coordinates;

            if (_rs.next()) {
                _coordinates = new Coordinates();
                _coordinates.setX(_rs.getInt("reference_position_x"));
                _coordinates.setY(_rs.getInt("reference_position_y"));
                _coordinates.setZ(_rs.getInt("reference_position_z"));

                // This data is needed for the xyz to enu conversion
                stationsHM.put(_rs.getInt("id_station"), _coordinates);
            }

            // If there are no Reference Position Velocities for this site,
            // use the oldest non-outlier xyz values from estimated_coordinates
            if(stationsHM.isEmpty()){
                _ps = _c.prepareStatement("SELECT x, y, z, id_station FROM estimated_coordinates WHERE id_station = (SELECT id FROM station WHERE marker = ?) AND outlier = 0 ORDER BY epoch ASC LIMIT 1;");

                _ps.setString(1, site);
                _rs = _ps.executeQuery();

                if(_rs.next()){
                    Coordinates coordinates = new Coordinates();
                    coordinates.setX(_rs.getInt("x"));
                    coordinates.setY(_rs.getInt("y"));
                    coordinates.setZ(_rs.getInt("z"));

                    // This data is needed for the xyz to enu conversion
                    stationsHM.put(_rs.getInt("id_station"), coordinates);
                }
            }

            //}

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (_rs != null) {
                try {
                    _rs.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
            if (_ps != null) {
                try {
                    _ps.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
            if (_c != null) {
                try {
                    _c.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
        }

        // If stationsHM is still empty, return empty array
        if(stationsHM.isEmpty()) {
            return new ArrayList<Estimated_coordinates>();
        }

        //Calls the conversion function passing the xyz coordinates returned and the reference_positions added to the hashmap in this function
        return CoordinatesHandler.xyz2enu(getTimeseriesXYZBasedOnParameters(site, analysis_centre, sampling_period, format, reference_frame, otl_model, antenna_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, data_type, version), stationsHM);
    }

    /**
     * Returns the XYZ/ENU timeseries residuals
     *
     * @return            ArrayList with enu timeseries residuals. Call xyz2enu method
     * @author            Rafael Couto & José Manteigueiro
     * date: 17/07/2018
     * modified: 10/03/2021 - José Manteigueiro
     *
     */
    ArrayList<Estimated_coordinates> getTimeseriesResidualsXYZorENUBasedOnParameters(String site, String analysis_centre, String sampling_period, String format, String reference_frame, String otl_model, String antenna_model, String cut_of_angle,String epoch_start, String epoch_end, String remove_outliers, String apply_offsets, String data_type, String xyzorenu, String version) throws SQLException {

        //Calls the conversion function passing the xyz coordinates returned and the reference_positions added to the hashmap in this function
        ArrayList<Estimated_coordinates> AL_estimated_coordinates = getTimeseriesXYZBasedOnParameters(site, analysis_centre, sampling_period, format, reference_frame, otl_model, antenna_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, data_type, version);

        //HashMap<Integer, Coordinates> stationsHM = new HashMap<>();
        ArrayList<Estimated_coordinates> _timeseriesResiduals = new ArrayList<>();

        Station _thisStation = new Station();

        // Check if there's no start_epoch and end_epoch selected
        // True = selected, False = not selected
        boolean _startEpochSelectedByUser = !epoch_start.equals("");
        boolean _endEpochSelectedByUser = !epoch_end.equals("");

        Connection _c = null;
        PreparedStatement _ps = null;
        ResultSet _rs = null;

        int _analysisCenterId, _methodIdentificationId, _stationId;
        double _refPosX, _refPosY, _refPosZ, _velX, _velY, _velZ;
        String _refEpoch;

        try {
            DataBaseConnectionV2 _dbcv2 = new DataBaseConnectionV2();
            _c = NewConnection();

            StringBuilder additionalQuery = new StringBuilder();
            // Analysis_centre should always be the short name of the AC, such as UGA-CNRS
            _analysisCenterId = getAnalysisCentreIdByAbbreviation(analysis_centre);

            // Select the method identification ID based on the AC + Data Type
            // In the future, Reference Frames should be handled TODO
            _methodIdentificationId = getMethodIdentificationIdByAnalysisCentreAndDataType(_analysisCenterId, "velocities");

            // Retrieve station ID by their marker
            _stationId = _dbcv2.getStationIdByMarker(site);

            // Additional string for epochs
            String _startEpochQuery = "";
            String _endEpochQuery = "";

            // Take in consideration start epoch
            if(_startEpochSelectedByUser){
                _startEpochQuery = " AND start_epoch <= ? ";
            }

            // Take in consideration end epoch
            if(_endEpochSelectedByUser){
                _endEpochQuery = " AND end_epoch >= ? ";
            }

            /*
            1 - id method identification
            2 - id station
            3 - start epoch (optional)
            4 - end epoch (optional)
             */
            _ps = _c.prepareStatement("SELECT id_station, reference_position_x, reference_position_y, reference_position_z, velx, vely, velz, ref_epoch FROM reference_position_velocities WHERE id_method_identification = ? AND id_station = ? " + _startEpochQuery + _endEpochQuery + " ORDER BY end_epoch DESC LIMIT 1;");
            _ps.setInt(1, _methodIdentificationId);
            _ps.setInt(2, _stationId);

            if(_startEpochSelectedByUser && _endEpochSelectedByUser){
                _ps.setString(3, epoch_start);
                _ps.setString(4, epoch_end);
            } else {
                // Both false, or just one true
                if(_startEpochSelectedByUser){
                    _ps.setString(3, epoch_start);
                }
                else if(_endEpochSelectedByUser){
                    _ps.setString(3, epoch_end);
                }
            }

            _rs = _ps.executeQuery();
            if(_rs.next()){
                _refPosX = _rs.getDouble("reference_position_x");
                _refPosY = _rs.getDouble("reference_position_y");
                _refPosZ = _rs.getDouble("reference_position_z");

                // From the DB, the velocities are in meters/year. For the calculation they need to be in meters/day
                _velX = _rs.getDouble("velx") / 365.25;
                _velY = _rs.getDouble("vely") / 365.25;
                _velZ = _rs.getDouble("velz") / 365.25;

                // If the Ref Epoch is null, return
                _refEpoch = _rs.getString("ref_epoch");
                if(_refEpoch == null){
                    throw new NullPointerException();
                }
            } else {
                throw new NullPointerException();
            }


            Coordinates _coordinates;
            Reference_Position_Velocities _rpv;
            Estimated_coordinates _ec_aux;
            DateFormat _dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date _ec_date, _rpv_date = _dateformat.parse(_refEpoch);
            long _mjd = PBOHandler.calculateMJD(_rpv_date);
            long _finalmjd;

            for (Estimated_coordinates _ec : AL_estimated_coordinates) {
                _coordinates = new Coordinates();
                _rpv = new Reference_Position_Velocities();
                _coordinates.setX(_refPosX);
                _coordinates.setY(_refPosY);
                _coordinates.setZ(_refPosZ);

                _rpv.setReference_position_x(_refPosX);
                _rpv.setReference_position_y(_refPosY);
                _rpv.setReference_position_z(_refPosZ);

                _rpv.setVelx(_velX);
                _rpv.setVely(_velY);
                _rpv.setVelz(_velZ);
                _rpv.setRef_epoch(_refEpoch);

                _ec_aux = new Estimated_coordinates();
                _ec_date = _dateformat.parse(_ec.getEpoch());

                _finalmjd = PBOHandler.calculateMJD(_ec_date) - _mjd;

                _ec_aux.setX(_ec.getX() - _refPosX - _velX * _finalmjd);
                _ec_aux.setY(_ec.getY() - _refPosY - _velY * _finalmjd);
                _ec_aux.setZ(_ec.getZ() - _refPosZ - _velZ * _finalmjd);
                _ec_aux.setEpoch(_ec.getEpoch());

                //This data is needed for the xyz to enu conversion
                //int station_id = rs.getInt("id_station");
                //stationsHM.put(station_id, coordinates);

                // Station object created before the for cycle
                //ec_aux.setStation(this_station);

                _timeseriesResiduals.add(_ec_aux);
            }
            //stationsHM.put(this_station.getId(), this_station.getLocation().getCoordinates());

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            if (_rs != null) {
                try {
                    _rs.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
            if (_ps != null) {
                try {
                    _ps.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
            if (_c != null) {
                try {
                    _c.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
        }

        if (xyzorenu.equals("XYZ")){
            return _timeseriesResiduals;
        } else {
            return CoordinatesHandler.XYZvector2ENUlocalframe(_timeseriesResiduals, _thisStation.getLocation().getCoordinates());
        }
        //ArrayList<Estimated_coordinates> x = CoordinatesHandler.xyz2enu(timeseriesResiduals, stationsHM);
        //return x;
    }


    /**
     * Converts xyz velocities to enu.
     * This method is based on a Machiel's file
     *
     * @return            ArrayList with enu velocities
     * @author            Andre Rodrigues
     * @author            Machiel Bos
     *
     */

    private ArrayList<Reference_Position_Velocities> ConvertVelocitiesXYZ2ENU(ArrayList<Reference_Position_Velocities> rpv, String tectonic_plate){

        ArrayList<Reference_Position_Velocities> results = new ArrayList<>();

        double rad = Math.PI /180.0;
        for (Reference_Position_Velocities aRpv : rpv) {

            Reference_Position_Velocities enurpv = new Reference_Position_Velocities();

            // --- The X, Y and Z reference_positions
            double X = aRpv.getReference_position_x();
            double Y = aRpv.getReference_position_y();
            double Z = aRpv.getReference_position_z();

            //--- velx, vely and velz
            double V_X = aRpv.getVelx();
            double V_Y = aRpv.getVely();
            double V_Z = aRpv.getVelz();

            double varXX = Math.pow(aRpv.getVelx_sigma(), 2);
            double varXY = aRpv.getVel_rho_xy() * (aRpv.getVelx_sigma() * aRpv.getVely_sigma());
            double varYY = Math.pow(aRpv.getVely_sigma(), 2);
            double varXZ = aRpv.getVel_rho_xz() * (aRpv.getVelx_sigma() * aRpv.getVelz_sigma());
            double varYZ = aRpv.getVel_rho_yz() * (aRpv.getVely_sigma() * aRpv.getVelz_sigma());
            double varZZ = Math.pow(aRpv.getVelz_sigma(), 2);

            //--- The uncertainties in the positions
            double pos_x_sigma = Math.sqrt(varXX);
            double pos_y_sigma = Math.sqrt(varYY);
            double pos_z_sigma = Math.sqrt(varZZ);

            //--- The cross-correlations in the positions
            double pos_rho_xy = aRpv.getReference_position_rho_xy();
            double pos_rho_xz = aRpv.getReference_position_rho_xz();
            double pos_rho_yz = aRpv.getReference_position_rho_yz();

            //---- The uncertainties in the velocities
            double vel_x_sigma = aRpv.getVelx_sigma();
            double vel_y_sigma = aRpv.getVely_sigma();
            double vel_z_sigma = aRpv.getVelz_sigma();

            //--- The cross-correlations in the velocities
            double vel_rho_xy = aRpv.getVel_rho_xy();
            double vel_rho_xz = aRpv.getVel_rho_xz();
            double vel_rho_yz = aRpv.getVel_rho_yz();


            //--- Convert Cartesian velocities into local East, North and Up
            double[] LBH = PBOHandler.convertXYZ2LBH(X, Y, Z);

            double lon = LBH[0] / rad;  // longitude in degrees
            double lat = LBH[1] / rad;  // latitude in degrees
            double cl = Math.cos(LBH[0]);
            double sl = Math.sin(LBH[0]);
            double ct = Math.cos(LBH[1]);
            double st = Math.sin(LBH[1]);
            double h = LBH[2]; // height above ellipsoid in metres

            //--- Transform covariance matrix
            RealMatrix R = MatrixUtils.createRealMatrix(new double[][]{
                    {-sl, cl, 0},
                    {-st * cl, -st * sl, ct},
                    {ct * cl, ct * sl, st}
            });


            //--- Construct the full XYZ covariance matrix
            RealMatrix VCV_XYZ = MatrixUtils.createRealMatrix(new double[][]{
                    {varXX, varXY, varXZ},
                    {varXY, varYY, varYZ},
                    {varXZ, varYZ, varZZ}
            });

            //--- rotate and convert metres to millimetres
            double V_E = -sl * V_X + cl * V_Y;
            double V_N = -cl * st * V_X - st * sl * V_Y + ct * V_Z;
            double V_U = cl * ct * V_X + ct * sl * V_Y + st * V_Z;

            //--- Compute the covariance matrix in East, North and Up frame
            RealMatrix VCV_ENU = R.multiply(VCV_XYZ).multiply(R.transpose());

            //--- Diagonal contains Se, Sn and Su
            double SNd = Math.sqrt(VCV_ENU.getEntry(1, 1));
            double SEd = Math.sqrt(VCV_ENU.getEntry(0, 0));
            double SUd = Math.sqrt(VCV_ENU.getEntry(2, 2));

            //--- Compute cross-correlations
            double Rne = VCV_ENU.getEntry(0, 1) / (SNd * SEd);
            double Rnu = VCV_ENU.getEntry(1, 2) / (SNd * SUd);
            double Reu = VCV_ENU.getEntry(0, 2) / (SUd * SEd);


            //X,Y,Z VALUES ARE HERE USED TO NOT CREATE ANOTHER ARRAYLIST
            enurpv.setVelx(V_N);
            enurpv.setVely(V_E);
            enurpv.setVelz(V_U);
            enurpv.setVelx_sigma(SNd);
            enurpv.setVely_sigma(SEd);
            enurpv.setVelz_sigma(SUd);
            enurpv.setVel_rho_xy(Rne);
            enurpv.setVel_rho_xz(Rnu);
            enurpv.setVel_rho_yz(Reu);
            enurpv.setReference_frame(aRpv.getReference_frame());
            enurpv.setMethod_identification(aRpv.getMethod_identification());

            results.add(enurpv);
        }

        return results;
    }


    /**
     * Returns the start and end epoch for a given station and analysis center. This method is used by pbovelocities in PBOHandlerVelocities class
     *
     * @return            Hashmap with start and end epochs
     * @author            Andre Rodrigues
     *
     */

    public HashMap<String, String> getStartEndDates(String station, String analysis_centre) throws SQLException {
        Connection c = null;
        HashMap<String, String> hm = new HashMap<String, String>();

        try {
            c = NewConnection();

            PreparedStatement prepS = c.prepareStatement(   "SELECT id FROM station WHERE marker ILIKE ?");
            try {
                prepS.setString(1, "%"+station+"%");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            ResultSet rs = prepS.executeQuery();

            int id_station = -1;
            if (rs.next())
                id_station = rs.getInt("id");

            rs.close();
            prepS.close();

            prepS = c.prepareStatement(   "SELECT id FROM analysis_centers WHERE name ILIKE ? OR abbreviation ILIKE ?");
            prepS.setString(1, "%"+analysis_centre+"%");
            prepS.setString(2, analysis_centre+"%");
            rs = prepS.executeQuery();

            int id_analysis_centre = -1;
            if (rs.next())
                id_analysis_centre = rs.getInt("id");

            rs.close();
            prepS.close();


            prepS = c.prepareStatement(
                    "SELECT start_epoch, end_epoch" +
                            " FROM reference_position_velocities" +
                            " INNER JOIN method_identification ON method_identification.id = reference_position_velocities.id_method_identification" +
                            " WHERE id_station = ?" +
                            " AND method_identification.id_analysis_centers = ? " +
                            " ORDER BY ref_epoch DESC, end_epoch DESC LIMIT 1");

            prepS.setInt(1, id_station);
            prepS.setInt(2, id_analysis_centre);
            rs = prepS.executeQuery();



            while (rs.next() == true) {

                hm.put("start", rs.getString("start_epoch"));
                hm.put("end", rs.getString("end_epoch"));
            }

            rs.close();
            prepS.close();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }


        return hm;
    }



    /**
     * Return the XYZ velocities for a given station and analysis_centre.
     *
     * Weird subroutine. It takes one 'site' as input but returns and ArrayList of velocities. In the future
     * this will facilitate to take a list of sites as input. Now 'tectonic_plate' is added as another
     * parameter. This will allow to compute velocities with respect to a given tectonic plate instead of a
     * reference frame that has zero mean velocities globally.
     *
     * INPUT
     * -----
     * site (String) : Still only 4 character marker, not 9 character
     * analysis_centre (String) : INGV, UGA-CNRS, etc.
     * tectonic_plate (String): "" -> absolute velocities, "EURA" -> subtract Eurasian Tectonic plate
     *
     * @return            ArrayList XYZ velocities in m/yr (object Reference_Position_Velocities)
     * @author            Andre Rodrigues + Machiel Bos
     *
     */
    ArrayList<Reference_Position_Velocities> getVelocitiesXYZ(String site, String analysis_centre, String tectonic_plate) throws SQLException {

        //--- Auxiliary variables;
        double        X0,Y0,Z0,Velx,Vely,Velz;
        double[]      V_plate = new double[3], LBH = new double[2];
        TectonicPlate plate = new TectonicPlate();

        //--- Allocate memory for results
        ArrayList<Reference_Position_Velocities> results = new ArrayList<>();

        Connection c = null;
        try {
            c = NewConnection();

            PreparedStatement prepS = c.prepareStatement(   "SELECT id FROM station WHERE marker ILIKE ?");
            try {
                prepS.setString(1, "%"+site+"%");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            ResultSet rs = prepS.executeQuery();

            int id_station = -1;
            if (rs.next())
                id_station = rs.getInt("id");

            rs.close();
            prepS.close();

            prepS = c.prepareStatement(   "SELECT id FROM analysis_centers WHERE name ILIKE ? OR abbreviation ILIKE ?");
            prepS.setString(1, "%"+analysis_centre+"%");
            prepS.setString(2, analysis_centre);

            rs = prepS.executeQuery();

            int id_analysis_centre = -1;
            if (rs.next())
                id_analysis_centre = rs.getInt("id");

            rs.close();
            prepS.close();

            //--- I don't like this SQL query. Why all the inner joins??
            prepS = c.prepareStatement(
                    "SELECT pf.url as pf_url, pf.epoch as pf_epoch, method_identification.creation_date as method_identification_creation_date, reference_frame.name as rf_name, reference_frame.epoch as rf_epoch, method_identification.creation_date as mi_creation_date, method_identification.version as version, method_identification.doi as doi," +
                            "velx, vely, velz, velx_sigma, vely_sigma, reference_position_x, reference_position_y, reference_position_z, " +
                            "velz_sigma, vel_rho_xy, vel_rho_xz, vel_rho_yz, reference_position_x_sigma, " +
                            "reference_position_y_sigma, reference_position_z_sigma, reference_position_rho_xy," +
                            "reference_position_rho_xz, reference_position_rho_yz " +
                            "FROM reference_position_velocities " +
                            " INNER JOIN method_identification ON method_identification.id = reference_position_velocities.id_method_identification" +
                            " INNER JOIN reference_frame ON reference_frame.id = method_identification.id_reference_frame"+
                            " INNER JOIN product_files pf ON pf.id = reference_position_velocities.id_product_files " +
                            " WHERE id_station = ?" +
                            " AND method_identification.id_analysis_centers = ? " +
                            " ORDER BY ref_epoch DESC, end_epoch DESC LIMIT 1");


            prepS.setInt(1, id_station);
            prepS.setInt(2, id_analysis_centre);
            rs = prepS.executeQuery();
            while (rs.next()) {
                Reference_Position_Velocities rpv = new Reference_Position_Velocities();

                //--- Get reference position
                X0 = rs.getDouble("reference_position_x");
                Y0 = rs.getDouble("reference_position_y");
                Z0 = rs.getDouble("reference_position_z");

                //--- Get velocities
                Velx = rs.getDouble("velx");
                Vely = rs.getDouble("vely");
                Velz = rs.getDouble("velz");

                if (tectonic_plate.equals("")==false) {
                    plate.getVelocityXYZ(tectonic_plate,X0,Y0,Z0,V_plate);
                    Velx -= V_plate[0];
                    Vely -= V_plate[1];
                    Velz -= V_plate[2];
                }
                rpv.setVelx(Velx);
                rpv.setVely(Vely);
                rpv.setVelz(Velz);
                rpv.setVelx_sigma(rs.getDouble("velx_sigma"));
                rpv.setVely_sigma(rs.getDouble("vely_sigma"));
                rpv.setVelz_sigma(rs.getDouble("velz_sigma"));
                rpv.setVel_rho_xy(rs.getDouble("vel_rho_xy"));
                rpv.setVel_rho_xz(rs.getDouble("vel_rho_xz"));
                rpv.setVel_rho_yz(rs.getDouble("vel_rho_yz"));
                rpv.setReference_position_x(X0);
                rpv.setReference_position_y(Y0);
                rpv.setReference_position_z(Z0);
                rpv.setReference_position_x_sigma(rs.getDouble("reference_position_x_sigma"));
                rpv.setReference_position_y_sigma(rs.getDouble("reference_position_y_sigma"));
                rpv.setReference_position_z_sigma(rs.getDouble("reference_position_z_sigma"));
                rpv.setReference_position_rho_xy(rs.getDouble("reference_position_rho_xy"));
                rpv.setReference_position_rho_xz(rs.getDouble("reference_position_rho_xz"));
                rpv.setReference_position_rho_yz(rs.getDouble("reference_position_rho_yz"));

                Method_identification _methodIdentification = new Method_identification();
                _methodIdentification.setCreation_date(rs.getTimestamp("mi_creation_date").toString());
                _methodIdentification.setVersion(rs.getDouble("version"));
                _methodIdentification.setDoi(rs.getString("doi"));
                rpv.setMethod_identification(_methodIdentification);

                Reference_frame _referenceFrame = new Reference_frame();
                _referenceFrame.setName(rs.getString("rf_name"));
                _referenceFrame.setEpoch(rs.getTimestamp("rf_epoch").toString());
                rpv.setReference_frame(_referenceFrame);

                Product_files pf = new Product_files();
                pf.setEpoch(rs.getString("pf_epoch"));
                pf.setUrl(rs.getString("pf_url"));
                rpv.setProduct_files(pf);

                //--- Finally, rpv object is added to results
                results.add(rpv);
            }

            rs.close();
            prepS.close();
        } catch (SocketException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }


        return results;
    }


    ArrayList<Reference_Position_Velocities> getVelocitiesENU(String site, String analysis_centre, String tectonic_plate) throws SQLException {

        ArrayList<Reference_Position_Velocities> results = getVelocitiesXYZ(site,analysis_centre,tectonic_plate);

        return ConvertVelocitiesXYZ2ENU(results,tectonic_plate);
    }


    /**
     * Return the velocityfields for one stations
     * used in the following method - VelocitiesField and associated end point and station method
     * @throws FileNotFoundException
     */

    /*private ArrayList<VelocityField> createVelocityFieldsFromStation( Connection c, int _rsStationId, String _rsStationMarker) throws SQLException{
        PreparedStatement prepS2, prepS3;
        ResultSet rs2, rs3;

        prepS2 = c.prepareStatement(
                "SELECT reference_position_x, reference_position_y, reference_position_z, " +
                        " velx, vely, velz FROM reference_position_velocities" +
                        " WHERE id_station = ?" +
                        " ORDER BY end_epoch DESC LIMIT 1");

        prepS2.setInt(1, _rsStationId);
        rs2 = prepS2.executeQuery();

        TectonicPlate plate = new TectonicPlate();
        double[] V_xyz = new double[3];

        ArrayList<VelocityField> results = new ArrayList<>();

        while (rs2.next()) {

            VelocityField vf = new VelocityField();

            double V_X = rs2.getDouble("velx");
            double V_Y = rs2.getDouble("vely");
            double V_Z = rs2.getDouble("velz");

            double x = 0, y = 0, z = 0;
            try{
                x = rs2.getDouble("reference_position_x");
                y = rs2.getDouble("reference_position_y");
                z = rs2.getDouble("reference_position_z");
            }catch(SQLException e){
                // If there are no reference_position in RPVs, use the Estimated Coordinates positions
                prepS3 = c.prepareStatement(
                        "SELECT x, y, z FROM estimated_coordinates" +
                                " WHERE id_station = ?" +
                                " ORDER BY epoch LIMIT 1");

                prepS3.setInt(1, _rsStationId);
                rs3 = prepS3.executeQuery();
                try {
                    while(rs3.next()){
                        x = rs3.getDouble("x");
                        y = rs3.getDouble("y");
                        z = rs3.getDouble("z");
                    }
                } catch(SQLException f){
                    // If there's also no position in ECs, don't show this station in velocities
                    break;
                }
            }

            //--- Subtract plate velocity (ALL)
            plate.getVelocityXYZ("EURA",x,y,z,V_xyz);
            V_X -= V_xyz[0];
            V_Y -= V_xyz[1];
            V_Z -= V_xyz[2];

            HashMap<String, Double> hm = CoordinatesHandler.getLambdaPhi(x, y, z);

            double cl = cos(hm.get("lambda"));
            double sl = sin(hm.get("lambda"));
            double ct = cos(hm.get("phi"));
            double st = sin(hm.get("phi"));
            double V_E = -sl * V_X + cl * V_Y;
            double V_N = -cl * st * V_X - st * sl * V_Y + ct * V_Z;
            //double V_U = cl * ct * V_X + ct * sl * V_Y + st * V_Z;

            double _vectorLength = sqrt(Math.pow(V_E, 2) + Math.pow(V_N, 2));
            double _vectorAngle = atan2(V_N, V_E);

            vf.setMarker( _rsStationMarker );
            vf.setLenghtvector(_vectorLength);
            vf.setAngle(_vectorAngle);

            results.add(vf);
            break;
        }
        return results;
    }*/

    private ArrayList<ArrayList<VelocityByPlateAndAc>> createVelocityFieldsFromStation(int _rsStationId, String _rsStationMarker) throws SQLException{
        /*PreparedStatement prepS2, prepS3;
        ResultSet rs2, rs3;

        prepS2 = c.prepareStatement(
                "SELECT reference_position_x, reference_position_y, reference_position_z, " +
                        " velx, vely, velz FROM reference_position_velocities" +
                        " WHERE id_station = ?" +
                        " ORDER BY end_epoch DESC LIMIT 1");

        prepS2.setInt(1, _rsStationId);
        rs2 = prepS2.executeQuery();

        TectonicPlate plate = new TectonicPlate();
        ArrayList<String> plates = plate.getPlates();
        double[] V_xyz = new double[3];

        ArrayList<ArrayList<VelocityByPlateAndAc>> results = new ArrayList<ArrayList<VelocityByPlateAndAc>>();

        while (rs2.next()) {

            double V_X = rs2.getDouble("velx");
            double V_Y = rs2.getDouble("vely");
            double V_Z = rs2.getDouble("velz");

            double x = 0, y = 0, z = 0;
            try{
                x = rs2.getDouble("reference_position_x");
                y = rs2.getDouble("reference_position_y");
                z = rs2.getDouble("reference_position_z");
            }catch(SQLException e){
                // If there are no reference_position in RPVs, use the Estimated Coordinates positions
                prepS3 = c.prepareStatement(
                        "SELECT x, y, z FROM estimated_coordinates" +
                                " WHERE id_station = ?" +
                                " ORDER BY epoch LIMIT 1");

                prepS3.setInt(1, _rsStationId);
                rs3 = prepS3.executeQuery();
                try {
                    while(rs3.next()){
                        x = rs3.getDouble("x");
                        y = rs3.getDouble("y");
                        z = rs3.getDouble("z");
                    }
                } catch(SQLException f){
                    // If there's also no position in ECs, don't show this station in velocities
                    break;
                }
            }

            //--- Subtract plate velocity (ALL)
            for (Analysis_Centers ac: analysis_center) {
                ArrayList<VelocityByPlateAndAc> aux = new ArrayList<>();
                for (String tPlate: plates) {
                    VelocityByPlateAndAc vf = new VelocityByPlateAndAc();
                    plate.getVelocityXYZ(tPlate,x,y,z,V_xyz);
                    V_X -= V_xyz[0];
                    V_Y -= V_xyz[1];
                    V_Z -= V_xyz[2];

                    HashMap<String, Double> hm = CoordinatesHandler.getLambdaPhi(x, y, z);

                    double cl = cos(hm.get("lambda"));
                    double sl = sin(hm.get("lambda"));
                    double ct = cos(hm.get("phi"));
                    double st = sin(hm.get("phi"));
                    double V_E = -sl * V_X + cl * V_Y;
                    double V_N = -cl * st * V_X - st * sl * V_Y + ct * V_Z;
                    //double V_U = cl * ct * V_X + ct * sl * V_Y + st * V_Z;

                    double _vectorLength = sqrt(Math.pow(V_E, 2) + Math.pow(V_N, 2));
                    double _vectorAngle = atan2(V_N, V_E);

                    vf.setMarker(_rsStationMarker);
                    vf.setAnalysis_center(ac.getAbbreviation());
                    vf.setPlate(tPlate);
                    vf.setMarker( _rsStationMarker );
                    vf.setVector_lenght(_vectorLength);
                    vf.setAngle(_vectorAngle);

                    aux.add(vf);
                }
                results.add(aux);
            }
            
            break;
        }*/

        ArrayList<ArrayList<VelocityByPlateAndAc>> results = new ArrayList<ArrayList<VelocityByPlateAndAc>>();
        File folder = null ;
        String folderName="/mnt/preproctoglass";
        try {
            folder = new File(folderName);
        } catch (NullPointerException e) {
            System.out.println("Cant open/create static filepath in Products " + folderName);
            e.printStackTrace();
            return null;
        }

        if (folder==null) {
            System.out.println("No files in filepath in Products " + folderName);
            return null;
        }

        File[] listOfFiles = folder.listFiles();

        if (listOfFiles==null) {
            System.out.println("No files in filepath in Products " + folderName);
            return null;
        }

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                String fileName = listOfFiles[i].toString();
                int index = fileName.lastIndexOf('.');
                if(index > 0) {
                    String extension = fileName.substring(index + 1);
                    if(extension.equals("vel")){
                        //BufferedReader reader;
                        try {
                            Scanner scanner = new Scanner(new File(listOfFiles[i].getAbsolutePath()));
                            //reader = new BufferedReader(new FileReader(listOfFiles[i].getAbsolutePath()));
                            ArrayList<VelocityByPlateAndAc> aux = new ArrayList<>();
                            while (scanner.hasNextLine()) {
                                String line = scanner.nextLine();
                                VelocityByPlateAndAc vf = new VelocityByPlateAndAc();
                                String new_line = line.trim().replaceAll("\\s{2,}", " ");
                                String[] columns = new_line.split(" ");
                                String marker = columns[0];
                                    if(marker.toUpperCase().contains(_rsStationMarker.toUpperCase())){
                                        String plate = columns[1];
                                        double V_E = Double.valueOf(columns[2]);

                                        double V_N  = Double.valueOf(columns[3]);

                                        double V_U  = Double.valueOf(columns[4]);

                                        if (V_E != 0.0 && V_N != 0.0 && V_U != 0.0){
                                            double _vectorLength_H = sqrt(Math.pow(V_E, 2) + Math.pow(V_N, 2));
                                            double _vectorAngle_H = atan2(V_N, V_E);
                                            
                                            double _vectorLength_V = sqrt(Math.pow(V_U, 2));
                                            double _vectorAngle_V = atan2(V_U, 0.0);   

                                            vf.setMarker(_rsStationMarker);
                                            vf.setAnalysis_center(listOfFiles[i].getName());
                                            vf.setPlate(plate);
                                            vf.setVector_lenght_H(_vectorLength_H);
                                            vf.setAngle_H(_vectorAngle_H); 
                                            vf.setVector_lenght_V(_vectorLength_V);
                                            vf.setAngle_V(_vectorAngle_V); 
                                            
                                            aux.add(vf);
                                        }
                                    }
                                /*if(fileName.contains("rob-euref")){
                                    //System.out.println(line);
                                    //ZYWI00POL,EURA,,,-0.00031,,,,0.00046   -0.00003    0.00001    0.00001    0.00003
                                    //ZYWI00POL,NOAM,,,,0.01755,,,-0.00540   -0.00005    0.00001    0.00001    0.00003
                                    String marker = columns[0];
                                    if(marker.toUpperCase().contains(_rsStationMarker.toUpperCase())){
                                        String plate = columns[1];
                                        double V_E = Double.valueOf(!columns[4].isEmpty() ? columns[4] : !columns[5].isEmpty() ? columns[5] : "0.0");

                                        double V_N  = Double.valueOf(!columns[7].isEmpty() ? columns[7] : !columns[8].isEmpty() ? columns[8] : !columns[9].isEmpty() ? columns[9] : "0.0");

                                        if (V_E != 0.0 && V_N != 0.0){
                                            double _vectorLength = sqrt(Math.pow(V_E, 2) + Math.pow(V_N, 2));
                                            double _vectorAngle = atan2(V_N, V_E);   

                                            vf.setMarker(_rsStationMarker);
                                            vf.setAnalysis_center(listOfFiles[i].getName());
                                            vf.setPlate(plate);
                                            vf.setVector_lenght(_vectorLength);
                                            vf.setAngle(_vectorAngle); 
                                            
                                            aux.add(vf);
                                        }
                                    }
                                }else{
                                    //System.out.println(line);
                                    String marker = columns[5];
                                    if(marker.toUpperCase().contains(_rsStationMarker.toUpperCase())){
                                        String plate = columns[6];
                                        //,,,,,YEBE,ABSO,,,,0.01885,,,,0.01632    0.00063    0.00009    0.00009    0.00033
                                        //,,,,,YEBE,EURA,,,-0.00040,,,-0.00020    0.00058    0.00009    0.00009    0.00033
                                        //,,,,,YEBE,NUBI,,,,0.00423,,,-0.00245    0.00057    0.00009    0.00009    0.00033
                                        double V_E = Double.valueOf(!columns[9].isEmpty() ? columns[9] : !columns[10].isEmpty() ? columns[10] : "0.0");
    
                                        double V_N  = Double.valueOf(!columns[12].isEmpty() ? columns[12] : !columns[13].isEmpty() ? columns[13] : !columns[14].isEmpty() ? columns[14] : "0.0");
                                        

                                        if (V_E != 0.0 && V_N != 0.0){
                                            double _vectorLength = sqrt(Math.pow(V_E, 2) + Math.pow(V_N, 2));
                                            double _vectorAngle = atan2(V_N, V_E);   

                                            vf.setMarker(_rsStationMarker);
                                            vf.setAnalysis_center(listOfFiles[i].getName());
                                            vf.setPlate(plate);
                                            vf.setVector_lenght(_vectorLength);
                                            vf.setAngle(_vectorAngle); 
                                            
                                            aux.add(vf);
                                        }
                                    }
                                }*/
                            }
                            results.add(aux);
                            scanner.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                            return results;
                        }
                    }
                }
            }
        }
        return results;
    }

        /**
         * Return the velocityfield (Angle and LengthVector) and the Station marker for a specified analysis_centre or for all, depending on the request
         *
         * @return            ArrayList VelocityField
         * @author            Andre Rodrigues, Paul
         */

        /*
          Comment Paul
          It seems that this function is called in only two places
          once to get velocities from markers from an API endpoint and once from DataBase Connection Class executeStationQuery
          markers should be casc,soph,etc
          It needed a total rewrite due to erros and code duplications
         */


        ArrayList<ArrayList<VelocityByPlateAndAc>> VelocitiesField(String markers) throws SQLException {

            markers=markers.toUpperCase();
           // ProductRequest request_holder = new ProductRequest(u);
           // request_holder.parseRequest();
            //TectonicPlate plate = new TectonicPlate();
            //double[] V_xyz = new double[3];

            ArrayList<ArrayList<VelocityByPlateAndAc>> results = new ArrayList<ArrayList<VelocityByPlateAndAc>>();

            Connection c = null;
            try {
                c = NewConnection();

                String query;

                if(!markers.equals("ALL")) {
                    query = "SELECT id, marker FROM station WHERE marker in ( ";
                    String[] marker_values = markers.split(",");
                    for (int ix = 0; ix < marker_values.length; ix++) {
                        if (0 == ix) {
                            query = query + "'" + marker_values[ix] + "'";
                        } else {
                            query = query + " , " + "'" + marker_values[ix] + "'";
                        }
                    }
                    query += " )";
                }
                else {
                    query ="SELECT DISTINCT id, marker FROM station";
                }

                PreparedStatement prepS;
                ResultSet rs;

                //prepS = c.prepareStatement("SELECT id FROM station WHERE marker = ?");
                prepS = c.prepareStatement( query ) ;
                rs = prepS.executeQuery();

                //For each station ID
                while (rs.next()) {
                    ArrayList<ArrayList<VelocityByPlateAndAc>> vFieldsFromStation = createVelocityFieldsFromStation(rs.getInt("id"), rs.getString("marker"));
                    if (null!=vFieldsFromStation)
                        results.addAll(vFieldsFromStation);
                }
                rs.close();
                prepS.close();

            } catch (SocketException | ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            } finally {
                if (c != null) {
                    try {
                        c.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

             return results;
        }


    public ArrayList<String> getStationMarkersOfStationsWithVelocitiesForAnalysisCenter(String analysis_center) throws SQLException {
        ArrayList<String> markers = new ArrayList<>();

        Connection c = null;
        try {
            c = NewConnection();

            PreparedStatement prepS = c.prepareStatement(   "SELECT id FROM analysis_centers WHERE name ILIKE ? OR abbreviation ILIKE ? LIMIT 1");
            prepS.setString(1, analysis_center);
            prepS.setString(2, analysis_center);
            ResultSet rs = prepS.executeQuery();

            int id_analysis_centre = -1;
            if (rs.next())
                id_analysis_centre = rs.getInt("id");
            else {
                rs.close();
                prepS.close();
                return markers;
            }

            rs.close();
            prepS.close();

            prepS = c.prepareStatement(
                    "SELECT marker " +
                        "FROM station " +
                        "WHERE id IN " +
                            "(SELECT distinct id_station " +
                            "FROM reference_position_velocities " +
                            "INNER JOIN method_identification " +
                            "ON method_identification.id = reference_position_velocities.id_method_identification " +
                            "WHERE method_identification.id_analysis_centers = ?)" +
                            ";"
            );
            prepS.setInt(1, id_analysis_centre);
            rs = prepS.executeQuery();

            while (rs.next()) {
                markers.add(rs.getString("marker"));
            }

            rs.close();
            prepS.close();
        } catch (SocketException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return markers;
    }

    public Station setTimeseries(Station s) {

        Connection _c = null;
        PreparedStatement _ps = null;
        ResultSet _rs = null;
        ArrayList<Method_identification> _results = new ArrayList<>();

        try {
            _c = NewConnection();
            _ps = _c.prepareStatement("SELECT DISTINCT mi.*, ac.id, rf.epoch AS rf_epoch, rf.name AS rf_name, ac.id AS ac_id, ac.name AS ac_name," +
                    " ac.abbreviation AS ac_abbreviation, ac.contact AS ac_contact, ac.email AS ac_email, ac.url AS ac_url FROM method_identification mi " +
                    "INNER JOIN reference_frame rf ON rf.id = mi.id_reference_frame " +
                    "INNER JOIN analysis_centers ac ON ac.id = mi.id_analysis_centers " +
                    "INNER JOIN estimated_coordinates ec ON ec.id_method_identification = mi.id " +
                    "WHERE ec.id_station = ? AND data_type ='timeseries';");

            Method_identification _mi;
            Analysis_Centers _ac;
            Reference_frame _rf;
            _ps.setInt(1, s.getId());
            _rs = _ps.executeQuery();

            while (_rs.next()) {
                _ac = new Analysis_Centers();
                _ac.setId(_rs.getInt("ac_id"));
                _ac.setName(_rs.getString("ac_name"));
                _ac.setAbbreviation(_rs.getString("ac_abbreviation"));
                _ac.setEmail(_rs.getString("ac_email"));
                _ac.setUrl(_rs.getString("ac_url"));

                _rf = new Reference_frame();
                _rf.setEpoch(_rs.getString("rf_epoch"));
                _rf.setName(_rs.getString("rf_name"));

                _mi = new Method_identification();
                _mi.setDoi(_rs.getString("doi"));
                _mi.setCreation_date(_rs.getString("creation_date"));
                _mi.setSoftware(_rs.getString("software"));
                _mi.setVersion(_rs.getDouble("version"));
                _mi.setAnalysis_center(_ac);
                _mi.setReference_frame(_rf);

                _results.add(_mi);
            }
        } catch (SocketException | SQLException | ClassNotFoundException _e) {
            Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _e);
        } finally {
            if(_rs != null){
                try{
                    _rs.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }

            if(_ps != null){
                try{
                    _ps.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }

            if(_c != null){
                try{
                    _c.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
        }

        s.setMI_timeseries(_results);
        return s;
    }

    public static class Tuple<X, Y> implements Serializable {
        public final X x;
        public final Y y;
        Tuple(X x, Y y) {
            this.x = x;
            this.y = y;
        }
    }

/*    HashMap<String, ArrayList<Tuple>> getDataAvailabilityFromStations() throws SQLException {

        HashMap<String, ArrayList<DataBaseT4Connection.Tuple>> result = new HashMap<>();

        ArrayList<String> station_epochs = new ArrayList<>();


        Connection c = null;
//        JsonCache mycache = new JsonCache();

        try {
            c =  NewConnection();

            for (Station station : getAllStationsWithProducts()){

//               try {
//                    //check in cache
//                    Station myStation = mycache.get(station.getId());
//                    if (myStation != null)
//                        result.put(myStation.getMarker(), myStation.getStartAndEndEpochs());
//                    else
//                        throw new Exception("Not in cache: " + station.getId() );
//                }catch (Exception e) {
//

                    try {
                        PreparedStatement prepS;
                        ResultSet rs;

                        prepS = c.prepareStatement(
                                "SELECT epoch " +
                                        "FROM estimated_coordinates" +
                                        " WHERE id_station = ?" +
                                        " ORDER BY epoch ASC");


                        prepS.setInt(1, station.getId());
                        rs = prepS.executeQuery();

                        while (rs.next()) {
                            station_epochs.add(rs.getString("epoch"));
                        }

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date startDate = formatter.parse(station_epochs.get(0));
                        Date endDate = formatter.parse(station_epochs.get(station_epochs.size() - 1));

                        LocalDate start = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                        LocalDate end = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                        ArrayList<Tuple> startAndEnd = new ArrayList<>();
                        LocalDate iterationStart = start;

                        boolean hiatus = false;
                        for (LocalDate date = start.plusDays(1); date.isBefore(end); date = date.plusDays(1)) {
                            if (!station_epochs.contains(date.toString() + " 00:00:00") && !hiatus) {
                                startAndEnd.add(new Tuple(iterationStart.toString(), date.minusDays(1).toString()));
                                hiatus = true;
                            } else if (station_epochs.contains(date.toString() + " 00:00:00") && hiatus) {
                                iterationStart = date;
                                hiatus = false;
                            }

                        }

                        result.put(station.getMarker(), startAndEnd);
                    }catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                /* } */
/*             }

       } catch (SocketException | ClassNotFoundException | ParseException e) {
            e.printStackTrace();
        } finally {
        if (c != null) {
            try {
                c.close();
            } catch (SQLException ex) {
                Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

        return result;

    }

    */


        /**
         *
         * @param
         * @return ArrayList
         * @throws SQLException
         *
         * Paul Crocker
         */
        ArrayList<ProductDataAvailability> getDataAvailabilityV2(int typeFlag) throws SQLException {
            ArrayList<ProductDataAvailability> results= new ArrayList<ProductDataAvailability>();

            Connection _c = null;
            PreparedStatement _ps = null;
            ResultSet _rs = null;

            try {
                _c = NewConnection();
                assert _c != null;
                if (GlassConstants.PRODUCTS_TIMESERIES==typeFlag)
                    _ps = _c.prepareStatement("SELECT * FROM "+DBConsts.ViewStn+" WHERE data_type='timeseries' order by id_station;");
                else
                    if (GlassConstants.PRODUCTS_COORDINATES==typeFlag)
                        _ps = _c.prepareStatement("SELECT * FROM "+DBConsts.ViewStn+" WHERE data_type='coordinates' order by id_station;");
                    else
                        //the below gives us providers for timeseries and coordinates (raw timeseries)
                    if (GlassConstants.PRODUCTS_ALLTIMESERIES==typeFlag)
                        _ps = _c.prepareStatement("SELECT * FROM "+DBConsts.ViewStn+" order by id_station");
                    else
                    if (GlassConstants.PRODUCTS_VELOCITES==typeFlag)
                        _ps = _c.prepareStatement("SELECT DISTINCT rpv.id_station,s.marker,rpv.id_method_identification AS id_mi,ac.id AS id_ac,ac.abbreviation,mi.data_type,mi.version,mi.creation_date,mi.doi,rpv.start_epoch AS min_epoch,rpv.end_epoch AS max_epoch,pf.sampling_period FROM reference_position_velocities rpv,method_identification mi,analysis_centers ac,station s,product_files pf WHERE rpv.id_method_identification = mi.id AND mi.id_analysis_centers = ac.id AND rpv.id_station = s.id AND rpv.id_product_files=pf.id GROUP BY rpv.id_station, rpv.id_method_identification, mi.id, mi.creation_date, mi.doi, mi.data_type, mi.version, ac.id, ac.abbreviation, s.id, s.marker, rpv.start_epoch, rpv.end_epoch, pf.sampling_period ORDER BY s.marker");
                    else
                        return results;

                _rs = _ps.executeQuery();

                while (_rs.next()) {
                    ProductDataAvailability p = new ProductDataAvailability(_rs);
                    results.add(p);
                }

            } catch (SocketException | ClassNotFoundException | AssertionError e) {
                Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, e);
            } finally{
                if(_rs != null){
                    try{
                        _rs.close();
                    }catch(SQLException _sqlException){
                        Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                    }
                }
                if(_ps != null){
                    try{
                        _ps.close();
                    }catch(SQLException _sqlException){
                        Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                    }
                }
                if(_c != null){
                    try{
                        _c.close();
                    }catch(SQLException _sqlException){
                        Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                    }
                }
            }

            return results;
        }

    /**
     *
     * @param - marker (string) or id (integer)
     * @return ProductDataAvailability
     * @throws SQLException
     *
     * Paul Crocker
     * The versions of this call.
     * One where the connection is opened amd closed elsewhere and another one where the connection is open and closed for each call.
     */

        ArrayList<ProductDataAvailability> getDataAvailabilityIndividualV2(int idStation, int typeFlag ) throws SQLException {
            return getDataAvailabilityIndividualV2(idStation, typeFlag, null);
        }

        ArrayList<ProductDataAvailability> getDataAvailabilityIndividualV2(int idStation, int typeFlag, Connection _c ) throws SQLException {

            //Connection _c = null;
            PreparedStatement _ps = null;
            ResultSet _rs = null;
            ArrayList<ProductDataAvailability> results= new ArrayList<ProductDataAvailability>();
            boolean persistConnection=false;

           try {
               if (null==_c)
                  _c =  NewConnection();
               else
                   persistConnection=true;

                assert _c != null;
               if (GlassConstants.PRODUCTS_TIMESERIES==typeFlag)
                   _ps = _c.prepareStatement("SELECT * FROM "+DBConsts.ViewStn+" WHERE data_type='timeseries' and id_station = ?");
               else
                   if (GlassConstants.PRODUCTS_COORDINATES==typeFlag)
                       _ps = _c.prepareStatement("SELECT * FROM "+DBConsts.ViewStn+" WHERE data_type='coordinates' and id_station = ?");
                   else
                       if (GlassConstants.PRODUCTS_ALLTIMESERIES==typeFlag)
                        _ps = _c.prepareStatement("SELECT * FROM "+DBConsts.ViewStn+" WHERE id_station = ?");
                       else
                            if (GlassConstants.PRODUCTS_VELOCITES==typeFlag)
                                _ps = _c.prepareStatement("SELECT DISTINCT rpv.id_station,s.marker,rpv.id_method_identification AS id_mi,ac.id AS id_ac,ac.abbreviation,mi.data_type,mi.version,mi.creation_date,mi.doi,rpv.start_epoch AS min_epoch,rpv.end_epoch AS max_epoch,pf.sampling_period FROM reference_position_velocities rpv,method_identification mi,analysis_centers ac,station s,product_files pf WHERE rpv.id_method_identification = mi.id AND mi.id_analysis_centers = ac.id AND rpv.id_station = s.id AND rpv.id_product_files=pf.id AND s.id = ? GROUP BY rpv.id_station, rpv.id_method_identification, mi.id, mi.creation_date, mi.doi, mi.data_type, mi.version, ac.id, ac.abbreviation, s.id, s.marker, rpv.start_epoch, rpv.end_epoch, pf.sampling_period ORDER BY s.marker");
                            else
                                return results;

                 _ps.setInt(1, idStation);
                 _rs = _ps.executeQuery();

                  while (_rs.next()) {
                      ProductDataAvailability p = new ProductDataAvailability(_rs);
                      results.add(p);
                  }

           } catch (SocketException | ClassNotFoundException | AssertionError e) {
               Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, e);
           } finally{
               if(_rs != null){
                   try{
                       _rs.close();
                   }catch(SQLException _sqlException){
                       Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                   }
               }
               if(_ps != null){
                   try{
                        _ps.close();
                    }catch(SQLException _sqlException){
                             Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                    }
                 }
                 if( !persistConnection && _c != null){
                          try{
                              _c.close();
                         }catch(SQLException _sqlException){
                               Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                         }
                 }
           }
           return results;
        }

/*
        /**
         *
         * @param site - station name/marker
         * @return
         * @throws SQLException
         *
         * Last edited on 12/03/2021 by José Manteigueiro
         * - Fixed bug where exceptions would not close DB connections
         * - General improves
         */
 /*   HashMap<String, ArrayList<Tuple>> getDataAvailabilityIndividual(String site) throws SQLException {

        HashMap<String, ArrayList<DataBaseT4Connection.Tuple>> _result = new HashMap<>();

        ArrayList<String> _station_epochs = new ArrayList<>();

        Connection _c = null;
        PreparedStatement _ps = null;
        ResultSet _rs = null;
        int _id_station = -1;

        try {
            _c = NewConnection();
            assert _c != null;
            _ps = _c.prepareStatement("SELECT id FROM station WHERE marker ILIKE ?;");
            _ps.setString(1, "%" + site + "%");
            _rs = _ps.executeQuery();

            if (_rs.next())
                _id_station = _rs.getInt("id");

        } catch (SocketException | ClassNotFoundException | AssertionError e) {
            Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, e);
        } finally{
            if(_rs != null){
                try{
                    _rs.close();
                }catch(SQLException _sqlException){
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
            if(_ps != null){
                try{
                    _ps.close();
                }catch(SQLException _sqlException){
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
            if(_c != null){
                try{
                    _c.close();
                }catch(SQLException _sqlException){
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
        }

        if(_id_station == -1){
            return _result;
        }

        JsonCache _cache = new JsonCache();

        Station _station;
        SimpleDateFormat _formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date _startDate, _endDate;
        LocalDate _start, _end;

/*
        // Confirm if the station is present in cache
        _station = _cache.get(_id_station);
        if (_station != null)
            _result.put(site, _station.getStartAndEndEpochs());
        else { */
 /*           try {
                _c = NewConnection();
                _ps = _c.prepareStatement("SELECT epoch FROM estimated_coordinates WHERE id_station = ? ORDER BY epoch ASC;");
                _ps.setInt(1, _id_station);
                _rs = _ps.executeQuery();

                while (_rs.next()) {
                    _station_epochs.add(_rs.getString("epoch"));
                }

                _startDate = _formatter.parse(_station_epochs.get(0));
                _endDate = _formatter.parse(_station_epochs.get(_station_epochs.size() - 1));

                _start = _startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                _end = _endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                ArrayList<Tuple> _startAndEnd = new ArrayList<>();
                LocalDate _iterationStart = _start, _date;

                boolean _hiatus = false;
                for (_date = _start.plusDays(1); _date.isBefore(_end); _date = _date.plusDays(1)) {
                    if (!_station_epochs.contains(_date.toString() + " 00:00:00") && !_hiatus) {
                        _startAndEnd.add(new Tuple(_iterationStart.toString(), _date.minusDays(1).toString()));
                        _hiatus = true;
                    } else if (_station_epochs.contains(_date.toString() + " 00:00:00") && _hiatus) {
                        _iterationStart = _date;
                        _hiatus = false;
                    }
                }
                _result.put(site, _startAndEnd);
            } catch (ParseException | SocketException | ClassNotFoundException _exception) {
                Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _exception);
            } finally {
                if(_rs != null){
                    try{
                        _rs.close();
                    }catch(SQLException _sqlException){
                        Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                    }
                }
                if(_ps != null){
                    try{
                        _ps.close();
                    }catch(SQLException _sqlException){
                        Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                    }
                }
                if(_c != null) {
                    try {
                        _c.close();
                    } catch (SQLException _sqlException) {
                        Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                    }
                }
            }
        /* } */

 /*       String _samplingPeriod = "";
        try {
            _c = NewConnection();
            _ps = _c.prepareStatement(" ?;");

            _ps.setInt(1, _id_station);
            _rs = _ps.executeQuery();

            if (_rs.next()) _samplingPeriod = _rs.getString("sampling_period");

        } catch (SocketException | ClassNotFoundException | SQLException _exception) {
            Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _exception);
        } finally {
            if(_rs != null){
                try{
                    _rs.close();
                }catch(SQLException _sqlException){
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
            if(_ps != null){
                try{
                    _ps.close();
                }catch(SQLException _sqlException){
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
            if(_c != null){
                try{
                    _c.close();
                }catch(SQLException _sqlException){
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
        }
/*

        HashMap<String, ArrayList<DataBaseT4Connection.Tuple>> _resultWeekly = new HashMap<>();
        if(_samplingPeriod.equalsIgnoreCase("weekly")) {
            ArrayList<DataBaseT4Connection.Tuple> _startAndEndDatesWeekly;
            ArrayList<Tuple> _startAndEndWeekly;
            SimpleDateFormat _simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date _date;
            LocalDate _first;
            for (Map.Entry<String, ArrayList<DataBaseT4Connection.Tuple>> entry : _result.entrySet()) {
                _startAndEndDatesWeekly = entry.getValue();
                _startAndEndWeekly = new ArrayList<>();

                for (DataBaseT4Connection.Tuple tuple : _startAndEndDatesWeekly) {
                    try {
                        _date = _simpleDateFormat.parse(tuple.x.toString());
                        _first = _date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                        _first = _first.minusDays(6);
                        _startAndEndWeekly.add(new Tuple(_first.toString(), tuple.y));
                        _resultWeekly.put(site, _startAndEndWeekly);
                    } catch (ParseException _parseException) {
                        Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.WARNING, null, _parseException);
                    }
                }
            }
        }

        return _resultWeekly;
    }
*/

    /**
     *
     *
     * Gets epochs for data-availability
     * This method is used for cache purposes only
     *
     * @param station_id
     * @return ArrayList<DataBaseT4Connection.Tuple> with all the epochs
     * @author André Rodrigues
     * @since 14/08/2018
     *
     * */
    ArrayList<DataBaseT4Connection.Tuple> getEpochsDataAvailability(int station_id) throws SQLException, SocketException, ClassNotFoundException{
        return getEpochsDataAvailability(station_id, null);
    }

    ArrayList<DataBaseT4Connection.Tuple> getEpochsDataAvailability(int station_id, Connection c) throws SQLException, SocketException, ClassNotFoundException{

        PreparedStatement s_epoch;        // Statement for the epochs
        ResultSet rs_epoch;       // Result Set for the epochs

        //Connection c = null;
        boolean persistConnection=false;
        ArrayList<DataBaseT4Connection.Tuple> startAndEnd = new ArrayList<>();

        try {
            if (null==c)
                c =  NewConnection();
            else
                persistConnection=true;

            ArrayList<String> station_epochs = new ArrayList<>();

            s_epoch = c.prepareStatement(
                    "SELECT epoch FROM estimated_coordinates WHERE id_station = ? ORDER BY epoch ASC");

            s_epoch.setInt(1, station_id);
            rs_epoch = s_epoch.executeQuery();

            while (rs_epoch.next()) {
                station_epochs.add(rs_epoch.getString("epoch"));
            }

            s_epoch.close();
            rs_epoch.close();

            if(!station_epochs.isEmpty()){
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date startDate = formatter.parse(station_epochs.get(0));
                Date endDate = formatter.parse(station_epochs.get(station_epochs.size() - 1));

                LocalDate start = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                LocalDate end = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                LocalDate iterationStart = start;

                boolean exist = false;
                boolean hiatus = false;
                for ( LocalDate date = start.plusDays(1);
                      date.isBefore(end.plusDays(1));
                      date = date.plusDays(1)) {
                    exist = false;
                    for (String fromArray : station_epochs) {
                        if (fromArray.substring(0,10 ).equals(date.toString())){
                            exist = true;
                            break;
                        }
                    }

                    if (exist == false && !hiatus) {
                        startAndEnd.add(new DataBaseT4Connection.Tuple(iterationStart.toString(), date.minusDays(1).toString()));
                        hiatus = true;
                    } else if (exist == true && hiatus) {
                        iterationStart = date;
                        hiatus = false;
                    }
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (c!= null && !persistConnection)
            c.close();

        return startAndEnd;
    }



    HashMap<String, String> getAttributesBasedOnKeyword(String attribute) {

        HashMap<String, String> results = new HashMap<>();

        Connection c = null;
        try {
            c =  NewConnection();

            PreparedStatement prepS;
            ResultSet rs;

            switch (attribute) {

                //the below is undocumented
                case "stations":
                case "stationsWithTS":
                    if ( GlassConstants.PRODUCT_OPTIMIZE_1)
                        prepS = c.prepareStatement("SELECT count(DISTINCT id_station) FROM view_estimated_coordinates");
                    else
                        prepS = c.prepareStatement("SELECT count(DISTINCT id_station) FROM estimated_coordinates");
                    rs = prepS.executeQuery();
                    while (rs.next()) {
                        results.put("stations_with_timeseries", rs.getInt("count")+"");
                    }
                    break;

                //the below is undocumented
                case "stationsWithV":
                    prepS = c.prepareStatement("SELECT count(DISTINCT id_station) FROM reference_position_velocities");
                    rs = prepS.executeQuery();
                    while (rs.next()) {
                        results.put("stations_with_velocities", rs.getInt("count")+"");
                    }
                    break;

                //the below is undocumented stations with products = TS or Velocity
                case "stationsWithP":
                    String q="";
                    if ( GlassConstants.PRODUCT_OPTIMIZE_1)
                        q="SELECT count(distinct view_estimated_coordinates.id_station) FROM view_estimated_coordinates FULL OUTER JOIN reference_position_velocities ON view_estimated_coordinates.id_station = reference_position_velocities.id_station ";
                    else
                        q="SELECT count(distinct estimated_coordinates.id_station) FROM estimated_coordinates FULL OUTER JOIN reference_position_velocities ON estimated_coordinates.id_station = reference_position_velocities.id_station ";
                    prepS = c.prepareStatement(q);
                    rs = prepS.executeQuery();
                    while (rs.next()) {
                        results.put("stations_with_products_ts_or_vel", rs.getInt("count")+"");
                    }
                    break;

                case "network":
                    prepS = c.prepareStatement("SELECT network.name, network.id FROM station_network "+
                                                    "INNER JOIN network ON network.id = station_network.id_network " +
                                                    "GROUP BY station_network.id_network, network.id;");
                    //    "WHERE id_station IN (SELECT DISTINCT id_station FROM estimated_coordinates) " +
                    //SELECT network.name, network.id FROM station_network INNER JOIN network ON network.id = station_network.id_network GROUP BY station_network.id_network, network.id;

                    rs = prepS.executeQuery();
                    while (rs.next()) {
                        results.put(rs.getString("name"), rs.getString("name"));
                    }
                    break;

                case "analysis_center":
                case "ac":
                    prepS = c.prepareStatement("SELECT DISTINCT name, abbreviation FROM analysis_centers " );

                    //    "INNER JOIN method_identification ON method_identification.id_analysis_centers = analysis_centers.id " +
                    //    "INNER JOIN estimated_coordinates ON estimated_coordinates.id_method_identification = method_identification.id");

                    rs = prepS.executeQuery();
                    while (rs.next()) {
                        results.put(rs.getString("name"), rs.getString("abbreviation"));
                    }
                    break;

                // Only analysis centers that have velocities
                case "analysis_center-velocities":
                case "acv":
                    prepS = c.prepareStatement("select distinct ac.name as name, ac.abbreviation as abbreviation " +
                            "from analysis_centers ac inner join method_identification mi on mi.id_analysis_centers = ac.id " +
                            "inner join reference_position_velocities rpv on rpv.id_method_identification  = mi.id;");
                    rs = prepS.executeQuery();
                    while (rs.next()) {
                        results.put(rs.getString("name"), rs.getString("abbreviation"));
                    }
                    break;

                // Only analysis centers that have timeseries
                case "analysis_center-timeseries":
                case "acts":
                    prepS = c.prepareStatement("select distinct ac.name, ac.abbreviation from analysis_centers ac " +
                            "inner join method_identification mi on mi.id_analysis_centers = ac.id " +
                            "where mi.data_type ='timeseries';");
                    rs = prepS.executeQuery();
                    while (rs.next()) {
                        results.put(rs.getString("name"), rs.getString("abbreviation"));
                    }
                    break;


                case "reference_frame":
                case "rf":
                    prepS = c.prepareStatement("SELECT DISTINCT name FROM reference_frame " );
                                                      //  +"INNER JOIN method_identification ON method_identification.id_reference_frame = reference_frame.id " +
                                                      //  "INNER JOIN estimated_coordinates ON estimated_coordinates.id_method_identification = method_identification.id");
                            rs = prepS.executeQuery();
                    while (rs.next()) {
                        results.put(rs.getString("name"), rs.getString("name"));
                    }
                    break;

                case "otl_model":
                case "om":
                    prepS = c.prepareStatement("SELECT DISTINCT otl_model FROM processing_parameters " );
                                                       //+ "INNER JOIN estimated_coordinates ON estimated_coordinates.id_processing_parameters = processing_parameters.id");
                            rs = prepS.executeQuery();
                    while (rs.next()) {
                        results.put(rs.getString("otl_model"), rs.getString("otl_model"));
                    }
                    break;

                case "antenna_model":
                case "am":
                    prepS = c.prepareStatement("SELECT DISTINCT antenna_model FROM processing_parameters " );
                            //+"INNER JOIN estimated_coordinates ON estimated_coordinates.id_processing_parameters = processing_parameters.id");
                    rs = prepS.executeQuery();
                    while (rs.next()) {
                        results.put(rs.getString("antenna_model"), rs.getString("antenna_model"));
                    }
                    break;

                case "sampling_period":
                case "sp":
                    prepS = c.prepareStatement("SELECT DISTINCT sampling_period FROM processing_parameters " );
                            //+"INNER JOIN estimated_coordinates ON estimated_coordinates.id_processing_parameters = processing_parameters.id");
                    rs = prepS.executeQuery();
                    while (rs.next()) {
                        results.put(rs.getString("sampling_period"), rs.getString("sampling_period"));
                    }
                    break;

                case "coordinates_system":
                case "cs":
                    results.put("ENU", "enu");
                    results.put("XYZ", "xyz");
                    break;

                case "format":
                    results.put("XML", "xml");
                    results.put("JSON", "json");
                    results.put("PBO", "pbo");
                    results.put("Midas", "midas");
                    results.put("Hector", "hector");
                    break;
                default:
                    break;

            }


        } catch (SocketException | ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;

    }


    public int getAnalysisCentreIdByAbbreviation(String abbreviation){
        Connection _c = null;
        PreparedStatement _ps = null;
        ResultSet _rs = null;
        int id = -1;

        try{
            _c = NewConnection();
            _ps = _c.prepareStatement("SELECT id FROM analysis_centers WHERE abbreviation = ?;");
            _ps.setString(1, abbreviation);
            _rs = _ps.executeQuery();

            if(_rs.next()){
                id = _rs.getInt("id");
            }
        } catch (SocketException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if(_rs != null){
                try{
                    _rs.close();
                } catch (SQLException _sqlException){
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }

            if(_ps != null){
                try{
                    _ps.close();
                } catch (SQLException _sqlException){
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }

            if(_c != null){
                try{
                    _c.close();
                } catch (SQLException _sqlException){
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
        }

        return id;
    }

    public int getMethodIdentificationIdByAnalysisCentreAndDataType(int _analysisCenterId, String _dataType){
        Connection _c = null;
        PreparedStatement _ps = null;
        ResultSet _rs = null;
        int id = -1;

        try{
            _c = NewConnection();
            _ps = _c.prepareStatement("SELECT id FROM method_identification WHERE id_analysis_centers = ? AND data_type = ?;");
            _ps.setInt(1, _analysisCenterId);
            _ps.setString(2, _dataType);
            _rs = _ps.executeQuery();

            if(_rs.next()){
                id = _rs.getInt("id");
            }
        } catch (SocketException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if(_rs != null){
                try{
                    _rs.close();
                } catch (SQLException _sqlException){
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }

            if(_ps != null){
                try{
                    _ps.close();
                } catch (SQLException _sqlException){
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }

            if(_c != null){
                try{
                    _c.close();
                } catch (SQLException _sqlException){
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
        }

        return id;
    }



    public ArrayList<String> filterStationMarkersByGeobox(ArrayList<String> markers, Geobox gb) {

        Connection c = null;
        ArrayList<String> acceptableMarkers = new ArrayList<>();
        try {
            c = NewConnection();

            PreparedStatement prepS = c.prepareStatement(
            "SELECT s.marker AS marker FROM station s " +
                    "INNER JOIN location l ON l.id = s.id_location " +
                    "INNER JOIN coordinates c ON c.id = l.id_coordinates " +
                    "WHERE c.lat >= ? AND c.lat <= ? AND c.lon >= ? and c.lon <= ?;"
            );
            prepS.setDouble(1, gb.getMinLat());
            prepS.setDouble(2, gb.getMaxLat());
            prepS.setDouble(3, gb.getMinLon());
            prepS.setDouble(4, gb.getMaxLon());
            ResultSet rs = prepS.executeQuery();

            while (rs.next())
                acceptableMarkers.add(rs.getString("marker"));

            rs.close();
            prepS.close();
        } catch (SocketException | ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT4Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        acceptableMarkers.retainAll(markers);
        return acceptableMarkers;

    }
}
