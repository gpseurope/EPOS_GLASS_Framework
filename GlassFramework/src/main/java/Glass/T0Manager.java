/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Glass;

import Configuration.SiteConfig;
import EposTables.*;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.EJB;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import java.io.*;
import java.io.File;
import java.net.SocketException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;





/*
 * T0-MANAGER
 * This class is responsible for defining the URIs to query the T0 EPOS database and
 * returns the queries results.
 */
@Path("t0-manager")
public class T0Manager {

    @EJB
    SiteConfig siteConfig;

    // =========================================================================
    //  FIELDS
    // =========================================================================
    @Context
    private UriInfo context;
    private String node_name;
    private int connection_id;
    private String myIP = "";                   // This String will store the ip address of the machine
    private String DBConnectionString = null;   // This String is used for the connection with the database

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
     * T0 MANAGER
     * This is the default constructor for the T0 MANAGER class.
     */

    //private String JWT_SECRET = "g3o6K5uh0fVIoTFP7Md9hA5UMEcOsRQ5bONruCMsGit7lJMAZwAEm7eTsI9XElW";
    public T0Manager() {
        this.siteConfig = lookupSiteConfigBean();
        myIP= //"10.0.7.65";
                siteConfig.getLocalIpValue();
        DBConnectionString = //DBConnectionString = "jdbc:postgresql://" + myIP + ":" + "5432" + "/" + "EPOS";
                siteConfig.getDBConnectionString();
    }

    private SiteConfig lookupSiteConfigBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    // =========================================================================
    // T0 QUERY METHODS
    // =========================================================================


    /*
     * GET NODE JSON
     * Returns a JSON with the nodes
     */
    @Path("getNodes")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray getNodesJson(){
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        ArrayList<Node> nodesData  = dbc.getNodes();
        return new Parsers().getNodesJson(nodesData);
    }

    /*
     * UPDATE NODE JSON
     *
     */
    @Path("updateNode")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateNode(JsonObject jo){
//        int id = Integer.parseInt(jo.getString("_NodeId"));
        int id = jo.getInt("_NodeId");
        String name =  jo.getString("_NodeName");
        String host =  jo.getString("_NodeHost");
        String port =  String.valueOf(jo.getInt("_NodePort"));
        String dbname =  jo.getString("_NodeDbname");
        String username =  jo.getString("_NodeUsername");
        String password =  jo.getString("_NodePassword");
        String contact_name = jo.getString("_contact_name");
        String url = jo.getString("_url");
        String email = jo.getString("_email");
        Boolean encrypt = jo.getBoolean("_encrypt");



        Node newNode = new Node(id, name, host, port, dbname, username, password);
        newNode.setUrl(url);
        newNode.setEmail(email);
        newNode.setContact_name(contact_name);
        DataBaseT0Connection dbc = new DataBaseT0Connection();

        if (hasSameName(newNode, dbc)){
            return Response.status(Response.Status.CONFLICT).entity("Same name").build();
        }

        int check = validate_host_port_dbname(newNode, dbc);
        if ( check == 0 ) {
            return Response.status(Response.Status.CONFLICT).entity("already exists host, port, dbname").build();
        }
        else if ( check == 2 )
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        else { // if check == 1
            if ( encrypt )
                return updateNodeEncryption(newNode);

            boolean resp = dbc.updateNode(newNode, " ");
            if(resp){
                return Response.status(Response.Status.NO_CONTENT).build();
            }
            else{
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }



    }
    /*
     * UPDATE NODE JSON with encryption
     *
     */
    public Response updateNodeEncryption(Node newNode){
        String password2;
        try {
            if(newNode.getPassword().equals("")) {
                password2 = "";
            }
            else {
                password2 = getKeyandEncrypt(newNode.getPassword());
                if (password2.equals(" ")) { //error encrypt
                    System.out.println("Error on getKeyandEncrypt function");
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, "Error on getKeyandEncrypt function");
                    return Response.status(Response.Status.BAD_REQUEST).entity("No File").build();
                }
            }
            newNode.setPassword(password2);
            DataBaseT0Connection dbc = new DataBaseT0Connection();
            boolean resp = dbc.updateNode(newNode, " ");
            if (resp) {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
            else {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
        catch (Exception e){
            System.out.println("Error on getKeyandEncrypt function");
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(Response.Status.BAD_REQUEST).entity("No File").build();
        }

//        Node newNode = new Node(id, name, host, port, dbname, username, password);
//        newNode.setUrl(url);
//        newNode.setEmail(email);
//        newNode.setContact_name(contact_name);
//        DataBaseT0Connection dbc = new DataBaseT0Connection();
//        boolean resp = dbc.updateNode(newNode, " ");

    }

    /*
     * ADD NODE JSON
     *
     */
    @Path("addNode")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addNode(JsonObject jo){
        //int id = Integer.parseInt(jo.getString("_NodeId"));
        String name =  jo.getString("_NodeName");
        String host =  jo.getString("_NodeHost");
        String port =  String.valueOf(jo.getInt("_NodePort"));
        String dbname =  jo.getString("_NodeDbname");
        String username =  jo.getString("_NodeUsername");
        String password =  jo.getString("_NodePassword");

        // chech if not null
        String url ;
        String contact;
        String email;

        if ( jo.isNull("_url"))
            url = null;
        else
            url = jo.getString("_url");

        if ( jo.isNull("_contact_name"))
            contact = null;
        else
            contact = jo.getString("_contact_name");


        if ( jo.isNull("_email"))
            email = null;
        else
            email = jo.getString("_email");


        Boolean encrypt = jo.getBoolean("_encrypt");

        DataBaseT0Connection dbc = new DataBaseT0Connection();
        Node newNode = new Node(0, name, host, port, dbname, username, password);
        newNode.setEmail(email);
        newNode.setUrl(url);
        newNode.setContact_name(contact);
        //validate name

        if (hasSameName(newNode, dbc)){
            return Response.status(Response.Status.CONFLICT).entity("Same name").build();
        }
        // validate port ,host, dbname
        int hasPort_Host_dbname = validate_host_port_dbname(newNode, dbc);
        if (hasPort_Host_dbname == 0){
            return Response.status(Response.Status.CONFLICT).entity("already exists host, port, dbname").build();
        }
        else if (hasPort_Host_dbname == 2)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        else { // if ( hasPort_Host_dbname == 1)
            if ( encrypt )
                return addNodeEncryption(newNode);

            boolean resp = dbc.insertNode(newNode);
            if(resp){
                return Response.status(Response.Status.NO_CONTENT).build();
            }
            else{
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }

    }
    /*
     * validate host port dbname
     * returns:
     * 0 - exist
     * 1 - not exist
     * 2 - other
     */
    private int validate_host_port_dbname(Node node, DataBaseT0Connection dbc) {
        //validate host port dbname
        boolean hasPort_Host_dbname;
        Connection c = null;
        PreparedStatement preparedStatement = null;
        try {
            hasPort_Host_dbname = dbc.checkHost_port_dbname(node);
        }
        catch (SocketException | SQLException | ClassNotFoundException e ) {
            return 2;
        }
        finally {
            if( c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if( preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
        if (hasPort_Host_dbname)
            return 0;

        return 1;
    }
    /**
     * returns true is  has same, false otherwise
     **/
    private boolean hasSameName(Node node, DataBaseT0Connection dbc) {
        //validate if node has same name
        Boolean sameName = false;
        Connection c = null;
        PreparedStatement preparedStatement = null;
        try {
            sameName = dbc.checkSameName(c, preparedStatement, node);
            return sameName;
        }
        catch (SocketException | SQLException | ClassNotFoundException e ) {
            return true;
        }
        finally {
            if( c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if( preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    /*
     * ADD NODE JSON with encryption
     *
     */
    public Response addNodeEncryption(Node newNode){
        try {
            String passwordEnc = getKeyandEncrypt(newNode.getPassword());
            if ( passwordEnc.equals(" ") ) {
                System.out.println("Error on getKeyandEncrypt function");
                Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, "Error on getKeyandEncrypt function");
                return Response.status(Response.Status.BAD_REQUEST).entity("No File").build();
            }
            else {
                DataBaseT0Connection dbc = new DataBaseT0Connection();
                newNode.setPassword(passwordEnc);
                boolean resp = dbc.insertNode(newNode);
                if ( resp ) {
                    return Response.status(Response.Status.NO_CONTENT).build();
                }
                else{
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
                }
            }
        }
        catch (Exception e){
            System.out.println("Error on getKeyandEncrypt function");
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(Response.Status.BAD_REQUEST).entity("No File").build();
        }
    }
    /*
     * DELETE NODE JSON
     * Returns a JSON  with all connections.
     */
    @Path("deleteNode")
    @POST
    public Response deleteNode(@QueryParam("Delete_ID") int id){
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        boolean resp = dbc.deleteNode(id);

        if(resp){
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        else{
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    /*
     * GET CONNECTIONS JSON
     * Returns a JSON  with all connections.
     */
    @Path("connectionsJSON")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray getConnectionsJSON(){
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        ArrayList<Connections> connectionsArrayList = dbc.getConnections();
        return new Parsers().getConnectionsJSON(connectionsArrayList);

    }

    /*
     * ADD CONNECTIONS JSON
     * Returns a JSON  with all connections.
     */
    @Path("addConnection")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addConnectionJSON(JsonObject data){
        Node nS = new Node();
        int idS = data.getInt("source");
        nS.setId(idS);
        Node nD = new Node();
        int idD = data.getInt("destiny");
        nD.setId(idD);
        ArrayList<Station> stArray = new ArrayList<>();
        JsonArray stationsJSON = data.getJsonArray("station");
        for (int i = 0; i < stationsJSON.size() ; i++) {
            Station iStation = new Station();
            iStation.setId(stationsJSON.getInt(i));
            stArray.add(iStation);
        }
        String mtdata = data.getString("metaData");
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        Connections connection = new Connections();
        connection.setSource(nS);
        connection.setDestiny(nD);
        connection.setMetadata(mtdata);
        boolean resp = dbc.insertConnection(connection, stArray);

        if(resp){
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        else{
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    /*
     * UPDATE CONNECTIONS JSON
     * Returns a JSON  with all connections.
     */
    @Path("updateConnection")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateConnectionJSON(JsonObject jo){
        JsonObject data = jo.getJsonObject("DATA");
        int idCon = Integer.parseInt(data.getString("ID"));
        System.out.println(idCon);
        Node nS = new Node();
        int idS = Integer.parseInt(data.getString("ID_SOURCE"));
        nS.setId(idS);
        Node nD = new Node();
        int idD = Integer.parseInt(data.getString("ID_DEST"));
        nD.setId(idD);
        Station st = new Station();
        int idSta = data.getInt("ID_STATION");
        st.setId(idSta);
        String mtdata = data.getString("ID_METADATA");

        Connections connection = new Connections(idCon, nS, nD, st, mtdata);
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        boolean resp = dbc.updateConnection(connection, idCon);

        if(resp){
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        else{
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

    }
    /*
     * DELETE CONNECTIONS JSON
     * Returns a JSON  with all connections.
     */
    @Path("delConnection")
    @POST
    public Response deleteConnectionJSON(@QueryParam("Delete_ID") int id){
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        boolean resp = dbc.deleteConnection(id);
        if(resp){
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        else{
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    /*
     *   GET Failed Queries JSON
     *   Returns a JSON with failed queries.
     *   */
    @Path("failed-queriesJSON")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray getFailedQueries(){
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        // Get failed queries list
        ArrayList<Failed_queries> failed_queries = dbc.getFailedQueries();
        Parsers pa = new Parsers();
        return pa.getFailedQueriesJSON(failed_queries);

    }

    @Path("queue-queriesJSON")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray getQueueQueries() {
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        // Get failed queries list
        ArrayList<Queries> queries = dbc.getQueueQueries();
        return new Parsers().getQueueQueries(queries);
    }

    @Path("deleteQuery")
    @POST
    public Response deleteQuery(@QueryParam("Delete_ID") int id){
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        boolean resp = dbc.deleteQuery(id);

        if(resp){
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        else{
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Path("getStationsJSON")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray getStationJson(){
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        ArrayList<Station> resultS  = dbc.getStations();


        return new Parsers().getStationsJSON(resultS);
    }


    @Path("getStationsMarkerJSON")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getStationMarkerJSON(@QueryParam("marker") String marker){
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        ArrayList<Station> resultS  = dbc.getStationsMarker();
        boolean resp = false;
        for(Station s: resultS) {
            if ( s.getMarkerLongName().equals(marker) ) {
                resp = true;
                break;
            }
        }
        if(resp) {
            Response.ResponseBuilder rb = Response.ok("EXISTS");
            return rb.build();
        }
        else {
            Response.ResponseBuilder rb = Response.ok("NOT EXISTS");
            return rb.build();
        }

    }

    @Path("deleteDataCenter")
    @POST
    public Response deleteDataCenter(@QueryParam("DCid") int id){
        DataBaseT0Connection dataBaseT0Connection = new DataBaseT0Connection();
        if (dataBaseT0Connection.deleteDataCenter(id)){
            return Response.status(Response.Status.OK).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }


    }

    @Path("addDataCenter")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addDataCenter(JsonObject jo){
        Data_center dc = new Data_center();
//        Node node = new Node();
        Agency agency = new Agency();
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        dc.setProtocol(jo.getString("_protocol"));
        dc.setHostname(jo.getString("_hostname"));
        dc.setAcronym(jo.getString("_acronym"));
        dc.setName(jo.getString("_name"));
        dc.setRoot_path(jo.getString("_rootPath"));
        agency.setId(jo.getInt("_agencyID"));
        dc.setAgency(agency);
//        node.setId(jo.getInt("_nodeId"));
//        dc.setNode(node);
        JsonArray nodes = jo.getJsonArray("_nodeList");
        JsonArray dirStruct = jo.getJsonArray("_dcStructure");
        boolean fail = dbc.newDataCenter(dc, dirStruct, nodes);
        if(fail) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        else {
            return Response.status(Response.Status.NO_CONTENT).build();
        }



    }

    /*
     * Get Stations markerLongName by data center id
     * Returns a JSON  with all connections.
     */
    @Path("getStationByDataCenterID")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray getStationDataCenterID(@QueryParam("station_id") int id){

        DataBaseT0Connection dbc = new DataBaseT0Connection();
        ArrayList<Station> arraStations  = dbc.getStationsDataCenter(id);

        return new Parsers().getStationByDataCenterID(arraStations);
    }


    @Path("getDataCenter")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray getDataCenter(@DefaultValue("") @QueryParam("Acronym") String Acronym) {

        DataBaseT0Connection dbc = new DataBaseT0Connection();
        ArrayList<Data_center> arraDc  = dbc.getDataCenter();

        return new Parsers().getDataCenter(arraDc);

    }
    @Path("getDataCenterbyAcronym/{Acronym}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray getDataCenterbyAcronym(@PathParam("Acronym") String Acronym) {

        DataBaseT0Connection dbc = new DataBaseT0Connection();
        ArrayList<Data_center> arraDc  = dbc.getDataCenterbyAcronym(Acronym);

        return new Parsers().getDataCenter(arraDc);

    }

    @Path("addDataCenterStation")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addDataCenterStation(JsonObject jo ) {
        DataBaseT0Connection dbc = new DataBaseT0Connection();

        Datacenter_station dcS = new Datacenter_station();

        int id = dbc.getGapDataCenterStation();
        int idS = jo.getInt("_id_station");
        int idDC = jo.getInt("_id_dataCenter");
        String dataCenterType = jo.getString("dataCenterType");
        String markerLongName = jo.getString("markerLongName");

        Station s = new Station();
        s.setId(idS);

        Data_center dc = new Data_center();
        dc.setId(idDC);

        dcS.setId(id);
        dcS.setStation(s);
        dcS.setDatacenter(dc);
        dcS.setDatacenter_type(dataCenterType);
        dcS.setMarkerLongName(markerLongName);

        boolean resp = dbc.insertDataCenterStation(dcS);
        if(resp){
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        else{
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

    }


    @Path("getNodeDC")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray getNodesDC(){
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        ArrayList<String[]> arraDc  = dbc.getNodeDC();
        return new Parsers().getNodeDc(arraDc);
    }

    @Path("getDataCenterID")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject getDataCenterByID(@QueryParam("dcID") int id){
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        Data_center_structure dataCenter = dbc.getDatacenterByID(id);
        return new Parsers().getDCbyID(dataCenter);

    }


    @Path("updateDataCenter")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateDataCenter(JsonObject jo){
        Data_center dc = new Data_center();
//        Node node = new Node();
        Agency agency = new Agency();
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        dc.setId(jo.getInt("_id"));
        dc.setProtocol(jo.getString("_protocol"));
        dc.setHostname(jo.getString("_hostname"));
        dc.setAcronym(jo.getString("_acronym"));
        dc.setName(jo.getString("_name"));
        dc.setRoot_path(jo.getString("_rootPath"));
        agency.setId(jo.getInt("_agencyID"));
        dc.setAgency(agency);
//        node.setId(jo.getInt("_nodeId"));
//        dc.setNode(node);
        JsonArray nodes = jo.getJsonArray("_nodeList");
        JsonArray dirStruct = jo.getJsonArray("_dcStructure");
        boolean fail = dbc.updateDataCenter(dc, dirStruct, nodes);
        if(fail) {
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

    }


    @Path("getAgencies")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray getAgencies() {
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        ArrayList<Agency> agencyArrayList = dbc.getAllAgencies();
        return  new Parsers().getAgencyT0JSON(agencyArrayList);
    }

    @Path("getFileType")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray getFileType(){
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        ArrayList<File_type> fileTypeArrayList = dbc.getFileTypes();
        return new Parsers().getFileTypeT0JSON(fileTypeArrayList);
    }

    @Path("getStationsForDataCenter")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray getStationDC() {
        DataBaseT0Connection dbc = new DataBaseT0Connection();
        LinkedHashMap<Data_center, ArrayList<Station>> data = dbc.getStationsForDataCenter();
        return new Parsers().getStationsForDataCenter(data);
    }


//    @Path("getJWT")
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response getToken(@QueryParam("token") String token) {
//        String urlLink = "https://orcid.org/oauth/token?";
//
//        CloseableHttpClient httpClient = HttpClients.createDefault();
//        HttpPost post = new HttpPost(urlLink);
////        post.setHeader("Accept", "application/json");
//          post.setHeader("Content-Type", "application/x-www-form-urlencoded");
////        post.setHeader("User-Agent", "Java client");
//
//        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
//        nameValuePairs.add(new BasicNameValuePair("client_id", "APP-95NQDRE1UR5OHZ84"));
//        nameValuePairs.add(new BasicNameValuePair("client_secret", "7c817148-62ed-49ca-81e8-6c181f2dba76"));
//        nameValuePairs.add(new BasicNameValuePair("grant_type", "authorization_code"));
//        nameValuePairs.add(new BasicNameValuePair("code", token));
////        nameValuePairs.add(new BasicNameValuePair("redirect_uri", "http://localhost:4200/login"));
//        StringBuffer result = new StringBuffer();
//
//
//        try {
//            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//            CloseableHttpResponse response = httpClient.execute(post);
//            HttpEntity entity2 = response.getEntity();
//
//            int respCode = response.getStatusLine().getStatusCode();
//            // response code is 200 everything is ok
//            if (response.getStatusLine().getStatusCode() == 200){
//
//                BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
//
//                StringBuffer result1 = new StringBuffer();
//                String line = "";
//                while ((line = br.readLine()) != null) {
//                    result1.append(line);
//                }
////                line = result1.toString();
////                JSONParser parser = new JSONParser();
////                JSONObject json = (JSONObject) parser.parse(line);
//
////                String accessTokens = json.get("access_token").toString();
////                String refreshToken = json.get("refresh_token").toString();
////                String expiresIn = json.get("expires_in").toString();
////                String name = json.get("name").toString();
////                String orcid = json.get("orcid").toString();
//
////                Map<String, String> claims = new HashMap<>();
////                claims.put("access_token", accessTokens);
////                claims.put("refresh_token", accessTokens);
////                claims.put("expiresIn", accessTokens);
////                claims.put("name", accessTokens);
////                claims.put("orcid", accessTokens);
//
//                EntityUtils.consume(entity2);
//                JwtBuilder jwts =  Jwts.builder().
//                        setPayload(result1.toString()).
//                        signWith(SignatureAlgorithm.HS512, JWT_SECRET.getBytes());
//
//                JsonObject jsonObject = Json.createObjectBuilder().add("JWT", jwts.compact()).build();
//                return Response.status(Response.Status.CREATED).entity(jsonObject).build();
//                return Response.status(Response.Status.CREATED).entity(jsonObject).build();
//
//
//            }
//            else {
//                // return error
//                EntityUtils.consume(entity2);
//                return Response.status(Response.Status.NO_CONTENT).entity("error getting token").build();
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
//        }
//    }


//    @Path("validateJWT")
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response validateJWT(@QueryParam("jwt") String jwt){
//
//
//        try {
//            Jwts.parser().setSigningKey(JWT_SECRET.getBytes()).parseClaimsJws(jwt);
//            return Response.status(Response.Status.OK).entity("true").build();
//        }
//        catch (io.jsonwebtoken.SignatureException e) {
//            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("false").build();
//
//        }
//    }



    private String getKeyandEncrypt( String password) throws  Exception {


        File f = new File("./key.dat");
        byte[] bytes;
        String enc = " ";

        if(f.exists() && !f.isDirectory()) {
            FileInputStream fs = new FileInputStream("./key.dat");
            bytes = new byte[32]; // 256 bits are converted to 32 bytes;
            int check = fs.read(bytes);
            if(check != 32){
                System.out.println("Key.dat is invalid.");
                // System.exit(2);
                return enc;

            }
        }
        else {
            System.out.println("Key.dat not found in this folder.");
            // System.exit(3);
            return enc;

        }


        byte[] encrypted = encrypt(password, bytes);
        byte[] encoded = Base64.getEncoder().encode(encrypted);

        BufferedWriter writer = new BufferedWriter(new FileWriter("enc.blob"));
        writer.write(new String(encoded));
        writer.close();
        enc = new String(encoded);
        return enc;
        // System.exit(0);

    }

    private static byte[] encrypt(String plainText, byte[] key) throws Exception {
        byte[] clean = plainText.getBytes();

        // IV Generated
        int ivSize = 16;
        byte[] iv = new byte[ivSize];
        SecureRandom random = new SecureRandom();
        random.nextBytes(iv);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

        /*
        // Key hashed
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(key);
        System.arraycopy(digest.digest(), 0, keyBytes, 0, keyBytes.length);
        */
        byte[] keyBytes = new byte[32];
        System.arraycopy(key, 0, keyBytes, 0, keyBytes.length);
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");

        // Encrypt

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] encrypted = cipher.doFinal(clean);

        // Combine IV with encrypted
        byte[] encryptedIVAndText = new byte[ivSize + encrypted.length];
        System.arraycopy(iv, 0, encryptedIVAndText, 0, ivSize);
        System.arraycopy(encrypted, 0, encryptedIVAndText, ivSize, encrypted.length);

        return encryptedIVAndText;

    }

    private String getKeyandDecrypt( String passEncrypt ) throws Exception {

        String masterKeyPath = "./key.dat";
        String dec = " ";

        byte[] key;
        File masterkey = new File(masterKeyPath);


        if(masterkey.exists() && !masterkey.isDirectory()) {
            FileInputStream fs = new FileInputStream(masterKeyPath);
            key = new byte[32]; // 256 bits are converted to 32 bytes;
            int check = fs.read(key);
        }
        else {
            System.out.println("No master key file found. You need to have the master key in this folder.");
            return dec;
        }

        byte[] decoded = Base64.getDecoder().decode(passEncrypt);
        String decrypted = decrypt(decoded, key);

        // BufferedWriter writer = new BufferedWriter(new FileWriter("decrypted.password"));
        // writer.write(decrypted);
        // writer.close();
        //System.out.println(encrypted.length);
        //System.out.println(new String(encoded));
        dec = decrypted;
        return dec;

    }

    private static String decrypt(byte[] encryptedIvTextBytes, byte[] key) throws Exception {
        int ivSize = 16;
        int keySize = 32;

        // Extract IV.
        byte[] iv = new byte[ivSize];
        System.arraycopy(encryptedIvTextBytes, 0, iv, 0, iv.length);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

        // Extract encrypted part.
        int encryptedSize = encryptedIvTextBytes.length - ivSize;
        byte[] encryptedBytes = new byte[encryptedSize];
        System.arraycopy(encryptedIvTextBytes, ivSize, encryptedBytes, 0, encryptedSize);

        byte[] keyBytes = new byte[keySize];
        /*
        // Hash key.
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(key);
        System.arraycopy(md.digest(), 0, keyBytes, 0, keyBytes.length);
        */
        System.arraycopy(key, 0, keyBytes, 0, keyBytes.length);
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");

        // Decrypt.
        Cipher cipherDecrypt = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipherDecrypt.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] decrypted = cipherDecrypt.doFinal(encryptedBytes);

        return new String(decrypted);
    }

    @Path("generateKeyFile")
    @GET
    public Response generateKeyFile() {
        File f = new File("./key.dat");
        byte[] bytes;
        try {
            if (f.exists() && !f.isDirectory()) {
                System.out.println("There's already a key.dat in this folder.");
                System.out.println("No additional key file was created.\nThis program will now exit.");
                return Response.status(Response.Status.OK).build();
            } else {
                FileOutputStream out = new FileOutputStream("./key.dat");
                SecureRandom random = new SecureRandom();
                bytes = new byte[32]; // 256 bits are converted to 32 bytes;
                random.nextBytes(bytes);
                out.write(bytes);
                out.close();
                System.out.println("Key file successfully created with the name key.dat\nThis program will now exit.");
                return Response.status(Response.Status.CREATED).build();
            }
        }
        catch (IOException e ){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Error creating key file", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
