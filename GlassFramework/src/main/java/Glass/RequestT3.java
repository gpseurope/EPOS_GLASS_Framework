/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Glass;

import EposTables.Coordinates;
import java.util.ArrayList;

/**
 *
 * @author kngo
 * REQUEST T2
 * This class is responsible for storing the data for a URI request.
 */
public class RequestT3 {
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private String request_string;                  // Stores the value fo the request string
    public ArrayList<String> date_range_list;       // Stores the requests for date range
    public ArrayList<String> publish_date_list;     // Stores the requests for publish date
    public ArrayList<String> file_type_list;        // Stores the requests for file type
    public ArrayList<String> charCode_list;         // Stores the requests for the 4 char code
    public ArrayList<String> siteName_list;         // Stores the requests for the site name
    public ArrayList<String> datesInRange_list;     // Stores the requests for sites with dates in range
    public ArrayList<String> network_list;          // Stores the requests for network
    public ArrayList<String> siteType_list;         // Stores the requests for site type
    public ArrayList<String> antennaType_list;      // Stores the requests for the antenna type
    public ArrayList<String> receiverType_list;     // Stores the resquets for the receiver type
    public ArrayList<String> radomeType_list;       // Stores the requests for the radome type
    public ArrayList<String> country_list;           // Stores the requests for the nation
    public ArrayList<String> state_list;            // Stores the requests for the state
    public ArrayList<String> city_list;             // Stores the requests for the city
    public ArrayList<String> agency_list;           // Stores the requests for the agency
    public ArrayList<Coordinates> coordinates_list; // Stores the requests for coordinates
    public float minLat, maxLat;                    // Stores the request for latitude rectangle
    public float minLon, maxLon;                    // Stores the request for longitude rectangle
    public float lat, lon, radius;                  // Stores the request for circle coordinates
    public String coordinates_type;                 // Stores the type of coordinate request
    public float minAlt, maxAlt;                    // Stores the request for altitude
    public float minLatitude, maxLatitude;          // Stores the request for laltitude
    public float minLongitude, maxLongitude;        // Stores the request for longitude
    public ArrayList<String> height_list;           // Stores the requests for height
    public ArrayList<String> altitude_list;         // Stores the requests for altitude
    public ArrayList<String> latitude_list;         // Stores the requests for latitude
    public ArrayList<String> longitude_list;        // Stores the requests for longitude
    public ArrayList<String> installed_list;        // Stores the requests for installed date
    public ArrayList<String> removed_list;          // Stores the requests for removed date
    public ArrayList<String> satellite_list;        // Stores the requests for satellite system
    public ArrayList<String> inverse_networks_list; // Stores the requests for inverse networks
    public ArrayList<String> station_type_list;     // Stores the requests for station type
    public Double data_availability_value;
    public ArrayList<String> sampling_frequency_list;
    public ArrayList<String> sampling_window_list;
    public String minInstall, maxInstall;                    // Stores the request for installed dates
    public String minRemoved, maxRemoved;                    // Stores the request for removed dates
    public float minimumObservation;                          // Stores the requests for Minimun observation of the station
    public Boolean removedate;
    public Boolean installdate;
    public ArrayList<String> constellation_list;        // Stores the requests for satellite system in T3
    public ArrayList<String> observationtype_list;
    public ArrayList<String> frequency_list;
    public ArrayList<String> channel_list;
    public float spprms;
    public float ratioepoch;
    public int nbcycleslips;
    public int nbclockjumps;
    public float multipathvalue;
    public float elevangle;
    public ArrayList<String> statusfile_list;
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * REQUEST T3
    * This is the default constructor for the Request T3 class.
    */
    public RequestT3()
    {
        this.request_string = "";
        this.date_range_list = new ArrayList();
        this.publish_date_list = new ArrayList();
        this.file_type_list = new ArrayList();
        this.charCode_list = new ArrayList();
        this.siteName_list = new ArrayList();
        this.datesInRange_list = new ArrayList();
        this.network_list = new ArrayList();
        this.siteType_list = new ArrayList();
        this.antennaType_list = new ArrayList();
        this.receiverType_list = new ArrayList();
        this.radomeType_list = new ArrayList();
        this.country_list = new ArrayList();
        this.state_list = new ArrayList();
        this.city_list = new ArrayList();
        this.agency_list = new ArrayList();
        this.coordinates_list = new ArrayList();
        this.coordinates_type = "";
        this.height_list = new ArrayList();
        this.latitude_list = new ArrayList();
        this.longitude_list = new ArrayList();
        this.altitude_list = new ArrayList();
        this.installed_list = new ArrayList();
        this.removed_list = new ArrayList();
        this.satellite_list = new ArrayList();
        this.inverse_networks_list = new ArrayList();
        this.station_type_list = new ArrayList();
        this.data_availability_value = 0.0;
        this.sampling_frequency_list = new ArrayList();
        this.sampling_window_list = new ArrayList();
        this.removedate = false;
        this.installdate = false;
        this.minLat = 0.0f;
        this.maxLat = 0.0f;
        this.minLon = 0.0f;
        this.maxLon = 0.0f;
        this.minAlt = 0.0f;
        this.maxAlt = 0.0f;
        this.minInstall = "";
        this.maxInstall = "";
        this.minRemoved = "";
        this.maxRemoved = "";
        this.constellation_list = new ArrayList();
        this.observationtype_list = new ArrayList();
        this.frequency_list = new ArrayList();
        this.channel_list = new ArrayList();
        this.spprms = 0.0f;
        this.ratioepoch = 0.0f;
        this.nbcycleslips = 0;
        this.nbclockjumps = 0;
        this.multipathvalue = 0.0f;
        this.elevangle = 0.0f;
        this.statusfile_list = new ArrayList();
    }
    
    /*
    * REQUEST T3
    * This constructor for the Request class sets the value for the request string.
    */
    public RequestT3(String request_string)
    {
        this.request_string = request_string;
        this.date_range_list = new ArrayList();
        this.publish_date_list = new ArrayList();
        this.file_type_list = new ArrayList();
        this.charCode_list = new ArrayList();
        this.siteName_list = new ArrayList();
        this.datesInRange_list = new ArrayList();
        this.network_list = new ArrayList();
        this.siteType_list = new ArrayList();
        this.antennaType_list = new ArrayList();
        this.receiverType_list = new ArrayList();
        this.radomeType_list = new ArrayList();
        this.country_list = new ArrayList();
        this.state_list = new ArrayList();
        this.city_list = new ArrayList();
        this.agency_list = new ArrayList();
        this.coordinates_list = new ArrayList();
        this.coordinates_type = "";
        this.height_list = new ArrayList();
        this.latitude_list = new ArrayList();
        this.longitude_list = new ArrayList();
        this.altitude_list = new ArrayList();
        this.installed_list = new ArrayList();
        this.removed_list = new ArrayList();
        this.satellite_list = new ArrayList();
        this.inverse_networks_list = new ArrayList();
        this.station_type_list = new ArrayList();
        this.data_availability_value = 0.0;
        this.sampling_frequency_list = new ArrayList();
        this.sampling_window_list = new ArrayList();
        this.removedate = false;
        this.installdate = false;
        this.minLat = 0.0f;
        this.maxLat = 0.0f;
        this.minLon = 0.0f;
        this.maxLon = 0.0f;
        this.minAlt = 0.0f;
        this.maxAlt = 0.0f;
        this.minInstall = "";
        this.maxInstall = "";
        this.minRemoved = "";
        this.maxRemoved = "";
        this.constellation_list = new ArrayList();
        this.observationtype_list = new ArrayList();
        this.frequency_list = new ArrayList();
        this.channel_list = new ArrayList();
        this.spprms = 0.0f;
        this.ratioepoch = 0.0f;
        this.nbcycleslips = 0;
        this.nbclockjumps = 0;
        this.multipathvalue = 0.0f;
        this.elevangle = 0.0f;
        this.statusfile_list = new ArrayList();
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET REQUEST STRING
    * Returns the request_string value.
    */
    public String getRequest_string() {
        return request_string;
    }

    /*
    * SET REQUEST STRING
    * Sets the request_string value.
    */
    public void setRequest_string(String request_string) {
        this.request_string = request_string;
    }
    
    // =========================================================================
    //  PARSING REQUEST
    // =========================================================================
    /*
    * PARSE REQUEST
    * This method parses the rrequest string and stores the values on the respective
    * ArrayLists for the usage in the queries.
    */
    public void parseRequest()
    {
        if (request_string.equalsIgnoreCase("") == false)
        {
            // *****************************************************************
            // STEP 1
            // Divide the request string by the & character
            // *****************************************************************
            String[] parameters = request_string.split("&");
            
            // *****************************************************************
            // STEP 2
            // Handle each parameter
            // *****************************************************************
            for (int i=0; i<parameters.length; i++)
            {
                // *************************************************************
                // STEP 3
                // Separate keyword from values
                // *************************************************************
                String[] individual = parameters[i].split("=");
             
                // *************************************************************
                // STEP 4
                // Store request data
                // *************************************************************
                switch (individual[0]) {
                    
                    case "dateRange":
                        date_range_list.add(individual[1].replace("%20", " "));
                        break;
                    case "publish_date_list":
                        publish_date_list.add(individual[1].replace("%20", " "));
                        break;
                    case "fileType":
                        String[] file_type_values = individual[1].split(",");
                        for (int j=0; j<file_type_values.length; j++)
                            file_type_list.add(file_type_values[j]);
                        break;
                        
                    case "dataAvailability":
                        try{
                            data_availability_value = Double.parseDouble(individual[1]);
                        }catch(NumberFormatException nfe){
                            data_availability_value = 0.0;
                        }
                        break;

                    case "samplingFrequency":
                        String[] sampling_frequency_values = individual[1].split(",");
                        for (int j=0; j<sampling_frequency_values.length; j++)
                            sampling_frequency_list.add(sampling_frequency_values[j]);
                        break;
                        
                    case "samplingWindow":
                        String[] sampling_window_values = individual[1].split(",");
                        for (int j=0; j<sampling_window_values.length; j++)
                            sampling_window_list.add(sampling_window_values[j]);
                        break;    
                        
                    case "marker":
                        String[] char_code_values = individual[1].split(",");
                        for (int j=0; j<char_code_values.length; j++)
                            charCode_list.add(char_code_values[j]);
                        break;
                        
                    case "site":
                        String[] site_name_values = individual[1].split(",");
                        for (int j=0; j<site_name_values.length; j++)
                            siteName_list.add(site_name_values[j]);
                        break;
                                              
                    case "network":
                        String[] network_values = individual[1].split(",");
                        for (int j=0; j<network_values.length; j++)
                            network_list.add(network_values[j]);
                        break;
                                               
                    case "antenna":
                        String[] antenna_type_values = individual[1].split(",");
                        for (int j=0; j<antenna_type_values.length; j++)
                            antennaType_list.add(antenna_type_values[j]);
                        break;
                        
                    case "receiver":
                        String[] receiver_type_values = individual[1].split(",");
                        for (int j=0; j<receiver_type_values.length; j++)
                            receiverType_list.add(receiver_type_values[j]);
                        break;
                        
                    case "radome":
                        String[] radome_type_values = individual[1].split(",");
                        for (int j=0; j<radome_type_values.length; j++)
                            radomeType_list.add(radome_type_values[j]);
                        break;
                        
                    case "country":
                        String[] nation_values = individual[1].split(",");
                        for (int j=0; j<nation_values.length; j++)
                            country_list.add(nation_values[j]);
                        break;
                        
                    case "state":
                        String[] state_values = individual[1].split(",");
                        for (int j=0; j<state_values.length; j++)
                            state_list.add(state_values[j]);
                        break;
                        
                    case "city":
                        String[] city_values = individual[1].split(",");
                        for (int j=0; j<city_values.length; j++)
                            city_list.add(city_values[j]);
                        break;
                        
                    case "agency":
                        String[] agency_values = individual[1].split(",");
                        for (int j=0; j<agency_values.length; j++)
                            agency_list.add(agency_values[j]);
                        break;
                        
                    case "coordinates":                         
                        // Handle the type of coordinate method
                        switch (individual[1]) {
                            case "rectangle":
                                // Set coordinates type
                                coordinates_type = "rectangle";
                                break;

                            case "circle":
                                // Set coordinates type
                                coordinates_type = "circle";
                                break;

                            case "polygon":
                                // Set coordinates type
                                coordinates_type = "polygon";
                                break;                  
                        }
                        break;    
                        
                    case "minLat": 
                        minLat = Float.parseFloat(individual[1]);
                        break;
                    case "maxLat":
                        maxLat = Float.parseFloat(individual[1]);
                        break;
                    case "minLon":
                        minLon = Float.parseFloat(individual[1]);
                        break;
                    case "maxLon":
                        maxLon = Float.parseFloat(individual[1]);
                        break;
                    
                    case "centerLat":    
                        lat = Float.parseFloat(individual[1]);
                        break;
                    case "centerLon":    
                        lon = Float.parseFloat(individual[1]);
                        break;
                    case "radius":    
                        radius = Float.parseFloat(individual[1]);
                        break;    
                    
                    case "polygon":    
                        // Get values
                        String[] values_polygon = individual[1].split(",");
                        for (int j = 0; j < values_polygon.length; j++) {
                            String[] coordinates_value = values_polygon[j].split("!");
                            Float lat_value = Float.parseFloat(coordinates_value[0]);
                            Float lon_value = Float.parseFloat(coordinates_value[1]);
                            Coordinates coord = new Coordinates();
                            coord.setLat(lat_value);
                            coord.setLon(lon_value);
                            coordinates_list.add(coord);
                        }
                        break;
                        
                    case "height":
                        String[] height_values = individual[1].split(",");
                        for (int j=0; j<height_values.length; j++)
                            height_list.add(height_values[j]);
                        break;
                        
                    case "latitude":
                        minLatitude = 0.0f;
                        maxLatitude = 0.0f;
                        String[] latitude_values = individual[1].split(",");
                        for (int j=0; j<latitude_values.length; j++)
                        {
                            if(latitude_values[j].equalsIgnoreCase("0")){
                                latitude_values[j]="0.00000000000001";
                            }
                            latitude_list.add(latitude_values[j]);
                        } 
                        if(latitude_values.length==2){
                            if(latitude_values[0].length()!=0 && latitude_values[1].length()!=0){
                                minLatitude = Float.parseFloat(latitude_values[0]);
                                maxLatitude = Float.parseFloat(latitude_values[1]);
                            }else if(latitude_values[0].length()==0 && latitude_values[1].length()!=0){
                                minLatitude = 0.0f;
                                maxLatitude = Float.parseFloat(latitude_values[1]);
                            }    
                        }else if(latitude_values[0].length()!=0){
                            minLatitude = Float.parseFloat(latitude_values[0]);
                            maxLatitude = 0.0f;
                        }   
                        break;
                        
                    case "longitude":
                        minLongitude = 0.0f;
                        maxLongitude = 0.0f;
                        String[] longitude_values = individual[1].split(",");
                        for (int j=0; j<longitude_values.length; j++)
                        {
                            if(longitude_values[j].equalsIgnoreCase("0")){
                                longitude_values[j]="0.00000000000001";
                            }
                            longitude_list.add(longitude_values[j]);
                        }    
                        if(longitude_values.length==2){
                            if(longitude_values[0].length()!=0 && longitude_values[1].length()!=0){
                                minLongitude = Float.parseFloat(longitude_values[0]);
                                maxLongitude = Float.parseFloat(longitude_values[1]);
                            }else if(longitude_values[0].length()==0 && longitude_values[1].length()!=0){    
                                minLongitude = 0.0f;
                                maxLongitude = Float.parseFloat(longitude_values[1]);
                            }    
                        }else if(longitude_values[0].length()!=0){
                            minLongitude = Float.parseFloat(longitude_values[0]);
                            maxLongitude = 0.0f;
                        }  
                        break;    
                        
                    case "altitude":
                        minAlt = 0.0f;
                        maxAlt = 0.0f;
                        String[] altitude_values = individual[1].split(",");
                        for (int j=0; j<altitude_values.length; j++)
                        {
                            if(altitude_values[j].equalsIgnoreCase("0")){
                                altitude_values[j]="0.00000000000001";
                            }
                            altitude_list.add(altitude_values[j]);
                        } 
                        if(altitude_values.length==2){
                            if(altitude_values[0].length()!=0 && altitude_values[1].length()!=0){
                                minAlt = Float.parseFloat(altitude_values[0]);
                                maxAlt = Float.parseFloat(altitude_values[1]);
                            }else if(altitude_values[0].length()==0 && altitude_values[1].length()!=0){
                                minAlt = 0.0f;
                                maxAlt = Float.parseFloat(altitude_values[1]);
                            }     
                        }else if(altitude_values[0].length()!=0){
                            minAlt = Float.parseFloat(altitude_values[0]);
                            maxAlt = 0.0f;
                        }    
                        break;
                                        
                    case "installedDateMin":
                        installdate = true;
                        minInstall = individual[1];
                        break;
                        
                    case "installedDateMax":
                        installdate = true;
                        maxInstall = individual[1];
                        break;    
                        
                    case "removedDateMin":
                        removedate = true;
                        minRemoved = individual[1];
                        break;
                        
                    case "removedDateMax":
                        removedate = true;
                        maxRemoved = individual[1];
                        break;
                        
                    case "satelliteSystem":
                        String[] satellite_values = individual[1].split(",");
                        for (int j=0; j<satellite_values.length; j++)
                            satellite_list.add(satellite_values[j]);
                        break;
                        

                    case "invertedNetworks":
                        String[] inverse_networks = individual[1].split(",");
                        for (int j=0; j<inverse_networks.length; j++)
                            inverse_networks_list.add(inverse_networks[j]);
                        break;
                        
                    case "type":
                        String[] station_types_values = individual[1].split(",");
                        for (int j=0; j<station_types_values.length; j++)
                            station_type_list.add(station_types_values[j]);
                        break;
                        
                    case "minimumObservationYears":
                        minimumObservation = Float.parseFloat(individual[1]);
                        break;
                        
                    case "statusfile":
                        String[] statusfile_values = individual[1].split(",");
                        for (int j=0; j<statusfile_values.length; j++)
                            statusfile_list.add(statusfile_values[j]);
                        break;    
                    //T3    
                    case "constellation":
                        String[] constellation_values = individual[1].split(",");
                        for (int j=0; j<constellation_values.length; j++)
                            constellation_list.add(constellation_values[j]);
                        break;
                        
                    case "observationtype":
                        String[] observationtype_values = individual[1].split(",");
                        for (int j=0; j<observationtype_values.length; j++)
                            observationtype_list.add(observationtype_values[j]);
                        break;
                        
                    case "frequency":
                        String[] frequency_values = individual[1].split(",");
                        for (int j=0; j<frequency_values.length; j++)
                            frequency_list.add(frequency_values[j]);
                        break;    
                    
                    case "channel":
                        String[] channel_values = individual[1].split(",");
                        for (int j=0; j<channel_values.length; j++)
                            channel_list.add(channel_values[j]);
                        break;
                        
                    case "spprms":
                        spprms = Float.parseFloat(individual[1]);
                        break;
                        
                    case "ratioepoch":
                        ratioepoch = Float.parseFloat(individual[1]);
                        break;
                        
                    case "nbcycleslips":
                        nbcycleslips = Integer.parseInt(individual[1]);
                        break;
                        
                    case "nbclockjumps":
                        nbclockjumps = Integer.parseInt(individual[1]);
                        break;    
                        
                    case "multipathvalue":
                        multipathvalue = Float.parseFloat(individual[1]);
                        break;
                        
                    case "elevangle":
                        elevangle = Float.parseFloat(individual[1]);
                        break;     
                }
            }
        }
        else
        {
            System.out.println("Error: empty resquest string!");
        }
    }
    
    /*
    * REQUEST TO STRING
    * This method prints the values of the ArrayLists.
    */
    public void requestToString()
    {
        String date_range_values = "Date Range: ";
        for (int i=0; i<date_range_list.size(); i++)
            date_range_values = date_range_values + date_range_list.get(i) + " ";
        System.out.println(date_range_values);
        
        String publish_date_values = "Publish Date: ";
        for (int i=0; i<publish_date_list.size(); i++)
            publish_date_values = publish_date_values + publish_date_list.get(i) + " ";
        System.out.println(publish_date_values);
        
        String file_type_values = "File Type: ";
        for (int i=0; i<file_type_list.size(); i++)
            file_type_values = file_type_values + file_type_list.get(i) + " ";
        System.out.println(file_type_values);
        
        String char_code_values = "Char Code: ";
        for (int i=0; i<charCode_list.size(); i++)
            char_code_values = char_code_values + charCode_list.get(i) + " ";
        System.out.println(char_code_values);
        
        String site_name_values = "Site Name: ";
        for (int i=0; i<siteName_list.size(); i++)
            site_name_values = site_name_values + siteName_list.get(i) + " ";
        System.out.println(site_name_values);
        
        String dates_in_range_values = "Date In Range: ";
        for (int i=0; i<datesInRange_list.size(); i++)
            dates_in_range_values = dates_in_range_values + datesInRange_list.get(i) + " ";
        System.out.println(dates_in_range_values);
        
        String network_values = "Network: ";
        for (int i=0; i<network_list.size(); i++)
            network_values = network_values + network_list.get(i) + " ";
        System.out.println(network_values);
        
        String site_type_values = "Site Type: ";
        for (int i=0; i<siteType_list.size(); i++)
            site_type_values = site_type_values + siteType_list.get(i) + " ";
        System.out.println(site_type_values);
        
        String antenna_type_values = "Antenna Type: ";
        for (int i=0; i<antennaType_list.size(); i++)
            antenna_type_values = antenna_type_values + antennaType_list.get(i) + " ";
        System.out.println(antenna_type_values);
        
        String receiver_type_values = "Receiver Type: ";
        for (int i=0; i<receiverType_list.size(); i++)
            receiver_type_values = receiver_type_values + receiverType_list.get(i) + " ";
        System.out.println(receiver_type_values);
        
        String radome_type_values = "Radome Type: ";
        for (int i=0; i<radomeType_list.size(); i++)
            radome_type_values = radome_type_values + radomeType_list.get(i) + " ";
        System.out.println(radome_type_values);
        
        String nation_values = "Nation: ";
        for (int i=0; i<country_list.size(); i++)
            nation_values = nation_values + country_list.get(i) + " ";
        System.out.println(nation_values);
        
        String state_values = "State: ";
        for (int i=0; i<state_list.size(); i++)
            state_values = state_values + state_list.get(i) + " ";
        System.out.println(state_values);
        
        String city_values = "City: ";
        for (int i=0; i<city_list.size(); i++)
            city_values = city_values + city_list.get(i) + " ";
        System.out.println(city_values);
        
        String agency_values = "Agency: ";
        for (int i=0; i<agency_list.size(); i++)
            agency_values = agency_values + agency_list.get(i) + " ";
        System.out.println(agency_values);
        
        System.out.println("tipo:" + coordinates_type);
        switch (coordinates_type) {
            case "rectangle":
                System.out.println("minLat: " + minLat);
                System.out.println("maxLat: " + maxLat);
                System.out.println("minLon: " + minLon);
                System.out.println("maxLon: " + maxLon);
                break;
                
            case "circle":
                System.out.println("Lat: " + lat);
                System.out.println("Lon: " + lon);
                System.out.println("Radius: " + radius);
                break;
                
            case "polygon":
                String polygon_values = "Polygon: ";
                for (int i=0; i<coordinates_list.size(); i++)
                {
                    polygon_values = polygon_values + coordinates_list.get(i).getLat() +
                            " " + coordinates_list.get(i).getLon() + ", ";
                }
                System.out.println(polygon_values);
                break;
        }
    }
}
