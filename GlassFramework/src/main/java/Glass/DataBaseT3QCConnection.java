package Glass;


import EposTables.QC_Report_Summary;
import EposTables.Rinex_file;

import java.net.SocketException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
public class DataBaseT3QCConnection extends DataBaseT3Connection {


    /*
    A page corresponds to 50 files
     */
    public ArrayList getFilesT3QCcodmpth(int id, String dates, int pageNumber, int perPageNumber) throws SQLException {

        String query = "select rf.id, rf.name, rf.reference_date, rf.status   from rinex_file as rf " +
                "INNER JOIN qc_report_summary as qcrs  on rf.id =  qcrs.id_rinexfile " +
                "where rf.id_station = ? " + dates +
                "and qcrs.id in " +
                " ( select qccon.id_qc_report_summary as id" +
                " from qc_constellation_summary as qccon " +
                " inner join  qc_observation_summary_c  as qcc " +
                " on qccon.id = qcc.id_qc_constellation_summary" +
                " where qcc.cod_mpth is NULL order by qccon.id_qc_report_summary )  order by rf.id ";

        if (pageNumber>0)
            query += " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY";

        Connection c = null;
        ArrayList<Rinex_file> results = new ArrayList<Rinex_file>();

        try {

            c = NewConnection();

            // *********************************************************************
            // Handle Files
            // *********************************************************************
            // Initialized statement for select
            PreparedStatement s = c.prepareStatement(query);
            s.setInt(1, id);

            ResultSet rs = null;

            rs = s.executeQuery(); // .executeQuery(query);
            //int cntx=0;
            while (rs.next() == true) {
                // Create Rinex File object

                //if (cntx++>10) break;

                Rinex_file rinex_file = new Rinex_file();

                // *****************************************************************
                // Handle Rinex File
                // *****************************************************************
                rinex_file.setName(rs.getString("name"));
                rinex_file.setReference_date(rs.getString("reference_date"));
                rinex_file.setStatus(rs.getInt("status"));

                // *************************************************************
                // Handle QC Summary Report
                // *************************************************************
                QC_Report_Summary qc_summary_report = getQC_Report_Summary_for_Rinex_Id(c, rs.getInt("id"));
                if (qc_summary_report != null)
                    rinex_file.setQCReportSummary(qc_summary_report);

                results.add(rinex_file);
            }

            rs.close();
            s.close();

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT3Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return results;
    }

    }
