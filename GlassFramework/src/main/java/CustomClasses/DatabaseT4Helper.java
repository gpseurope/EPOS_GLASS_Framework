package CustomClasses;

import Configuration.DBConsts;
import Configuration.SiteConfig;
import EposTables.Analysis_Centers;
import EposTables.Estimated_coordinates;
import EposTables.Method_identification;
import EposTables.Reference_frame;
import Glass.GlassConstants;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.lang.reflect.Method;
import java.net.SocketException;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/* Small class that gets fills some table values for given analysis center abbreviation and
 * type of product. This action is required quite a few times in DataBaseT4Connection.java which
 * justifies putting it into a separate class.
 *
 *  \author Machiel Bos
 *  \date 31/12/2020  Santa Clara
 */
public class DatabaseT4Helper {

    @EJB
    SiteConfig siteConfig;

    private String myIP = null;     // This String will store the ip address of the machine
    private String DBConnectionString = null;

    //--- These are the variables I need to query from the DB.
    private int        id_ac,id_mi,mi_id_reference_frame,id_frame;
    private double      mi_version;
    private String     ac_name,ac_abbr,ac_contact,ac_email,ac_url,mi_creation_date,mi_doi;
    private String     rf_name,rf_epoch;



    //--- Paul's subroutine to make a new connection. Note that this is an exact copy of the one
    //    in DataBaseT4Connection.
    private Connection NewConnection() throws SocketException, ClassNotFoundException, SQLException {

        // Create connection with the local database
        Class.forName("org.postgresql.Driver");
        Connection c = DriverManager.getConnection(DBConnectionString, DBConsts.DB_USERNAME, DBConsts.DB_PASSWORD);
        c.setAutoCommit(false);
        return c;
    }

    //--- Paul's subroutine to know how to make a new connection. Note that this is an exact copy of
    //    the one in DataBaseT4Connection.
    protected SiteConfig lookupSiteConfigBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }


    //--- The constructor. It must be called with 'analysis_centre' and 'data_type' in order to
    //    have sufficient information to perform the queries.
    public DatabaseT4Helper(ArrayList<Estimated_coordinates> data, String version, String data_type) {

        //--- Paul's mandatory initial lines to set the 'DBConnectionString'
        this.siteConfig = lookupSiteConfigBean();
        myIP = siteConfig.getLocalIpValue();
        DBConnectionString = siteConfig.getDBConnectionString(); // used by NewConnection

        //--- Connect to Product DB
        Connection c;

        try {
            c = NewConnection();

            String conditions = " WHERE mi.id = ? ";
            if (version != null && !version.equals("")){
                conditions += " AND mi.version = ? ";
            }

            //--- Perform a query to get meta-data concerning analysis/combination centre
            PreparedStatement prepS = c.prepareStatement(   "SELECT ac.*, mi.id as mi_id, mi.id_reference_frame as mi_id_reference_frame, mi.creation_date as mi_creation_date," +
                    "mi.doi as mi_doi, mi.version as mi_version FROM method_identification mi " +
                    "INNER JOIN analysis_centers ac on mi.id_analysis_centers = ac.id " + conditions + ";");

            // TODO: this 'for' doesn't look correct
            int _miID = 0;
            for(Estimated_coordinates ec : data){
                try{
                    _miID = ec.getMethod_identification().getId();
                    break;
                }catch(NullPointerException _npe){
                }
            }

            prepS.setInt(1, _miID);
            if (version != null && !version.equals("")){
                prepS.setString('2', version);
            }


            ResultSet rs = prepS.executeQuery();
            if (rs.next()) {
                id_ac      = rs.getInt("id");
                ac_name    = rs.getString("name");
                ac_abbr    = rs.getString("abbreviation");
                ac_contact = rs.getString("contact");
                ac_email   = rs.getString("email");
                ac_url     = rs.getString("url");
                id_mi                 = rs.getInt("mi_id");
                mi_id_reference_frame = rs.getInt("mi_id_reference_frame");
                mi_creation_date      = rs.getString("mi_creation_date");
                mi_doi                = rs.getString("mi_doi");
                mi_version            = rs.getFloat("mi_version");
            }
            rs.close();
            prepS.close();

            //--- Perform a query to Reference Frame info
            prepS = c.prepareStatement(   "SELECT * FROM reference_frame WHERE id = ?;");
            prepS.setInt(1, mi_id_reference_frame);
            rs = prepS.executeQuery();
            if (rs.next()) {
                id_frame  = rs.getInt("id"); // of course equal to mi_id_reference_frame
                rf_name   = rs.getString("name");
                rf_epoch  = rs.getString("epoch");
            }
            rs.close();
            prepS.close();

        } catch (SocketException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    //--- Simple getter for name of Reference Frame
    public String getReference_frame() {
        return rf_name;
    }

    //--- Simple getter for DOI
    public String getDOI() {
        return mi_doi;
    }

    //--- Simple getter for version of product
    public Double getVersion() {
        return mi_version;
    }
}
