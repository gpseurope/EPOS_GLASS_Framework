package CustomClasses;

public class StationNotFound extends Exception
{
    // Parameterless Constructor
    public StationNotFound() {}

    // Constructor that accepts a message
    public StationNotFound(String message)
    {
        super(message);
    }
}