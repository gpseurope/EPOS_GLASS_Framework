package CustomClasses;

public class markerinfo {
    public String longmarker;
    public int id;

    public markerinfo() {
    }

    public String getLongmarker() {
        return longmarker;
    }

    public void setLongmarker(String longmarker) {
        this.longmarker = longmarker;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "markerinfo{" +
                "longmarker='" + longmarker + '\'' +
                ", id=" + id +
                '}';
    }

    public markerinfo(String longmarker, int id) {
        this.longmarker = longmarker;
        this.id = id;
    }
}
