package CustomClasses;

import javax.ws.rs.core.Response;

public class ValidatorException   extends Exception  {

    public Response.Status responseStatus;

    public ValidatorException(Response.Status responseStatus, String message ) {
        super(message);
        this.responseStatus=responseStatus;
    }

    public ValidatorException(String message) {

        super(message);
    }
}
