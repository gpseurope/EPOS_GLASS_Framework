package CustomClasses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TectonicPlate {

    private double mas2rad = 4.8481368e-9;
    private Map<String, double[]> model = new HashMap<String, double[]>();
    private ArrayList<String> plates = new ArrayList<String>() {
        {
            add("EURA");
            add("NUBI");
            add("NOAM");
            add("NNR");
        }
    };

    public TectonicPlate() {

        //--- longitude (degrees), latitude (degrees) and omega (rad/year) of ITRF2014
        double euler_poles[][] = {
                {   -0.248,   -0.324,    0.675 }, // ANTA
                {    1.154,   -0.136,    1.444 }, // ARAB
                {    1.510,    1.182,    1.215 }, // AUST
                {   -0.085,   -0.531,    0.770 }, // EURA
                {    1.154,   -0.005,    1.454 }, // INDI
                {   -0.333,   -1.544,    1.623 }, // NAZC
                {    0.024,   -0.694,   -0.063 }, // NOAM
                {    0.099,   -0.614,    0.733 }, // NUBI
                {   -0.409,    1.047,   -2.169 }, // PCFC
                {   -0.270,   -0.301,   -0.140 }, // SOAM
                {   -0.121,   -0.794,    0.884 }, // SOMA
                {    0.000,    0.000,    0.000 }, // ABSO
        };

        //--- Fill HashMap model with ITRF2014 Euler Poles
        model.put("ANTA",euler_poles[0]);
        model.put("ARAB",euler_poles[1]);
        model.put("AUST",euler_poles[2]);
        model.put("EURA",euler_poles[3]);
        model.put("INDI",euler_poles[4]);
        model.put("NAZC",euler_poles[5]);
        model.put("NOAM",euler_poles[6]);
        model.put("NUBI",euler_poles[7]);
        model.put("PCFC",euler_poles[8]);
        model.put("SOAM",euler_poles[9]);
        model.put("SOMA",euler_poles[10]);
        model.put("NNR",euler_poles[11]);
    }

    /* For given tectonic plate name, compute the velocity of that plate for given location (lon,lat)
     * Velocity is given in Cartesian coordinates.
     *
     * INPUT
     * -----
     * plate_name (String): 4 letter abbreviation of tectonic plate (ex. 'EURA')
     * X (double) : X of location for which velocity is required (radians)
     * Y (double) : Y of location for which velocity is required (radians)
     * Z (double) : Z of location for which velocity is required (radians)
     *
     * OUTPUT
     * ------
     * V_xyz (double[3]) : plate motion in X, Y and Z-direction (m/yr)
     */
    public void getVelocityXYZ(String plate_name, double X, double Y, double Z, double V_xyz[]) {

        double[]  omega = new double[3];

        //--- Get Euler Pole
        omega = model.get(plate_name);
        if (omega == null) {
            System.out.printf("Requested plate model %s, does not exist",plate_name);
            V_xyz[0] = V_xyz[1] = V_xyz[2] = 9.9e99;
        } else {
            V_xyz[0]= mas2rad*(Z*omega[1] - Y*omega[2]);
            V_xyz[1]= mas2rad*(X*omega[2] - Z*omega[0]);
            V_xyz[2]= mas2rad*(Y*omega[0] - X*omega[1]);
        }
    }

    public ArrayList<String> getPlates() {
        return plates;
    }

    public void setPlates(ArrayList<String> plate) {
        this.plates = plate;
    }

}