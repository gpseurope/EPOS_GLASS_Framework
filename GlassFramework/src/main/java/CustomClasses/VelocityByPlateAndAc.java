package CustomClasses;

import java.io.Serializable;

public class VelocityByPlateAndAc implements Serializable {
    public String marker;
    public String marker_long_name;
    public String plate;
    public String analysis_center;
    public double angle_H;
    public double vector_lenght_H;
    public double angle_V;
    public double vector_lenght_V;

    public VelocityByPlateAndAc(String marker, String marker_long_name, String plate, String analysis_center, double angle_H, double vector_lenght_H, double angle_V, double vector_lenght_V) {
        this.marker = marker;
        this.marker_long_name = marker_long_name;
        this.plate = plate;
        this.analysis_center = analysis_center;
        this.angle_H = angle_H;
        this.vector_lenght_H = vector_lenght_H;
        this.angle_V = angle_V;
        this.vector_lenght_V = vector_lenght_V;
    }

    public VelocityByPlateAndAc() {
        this.marker = "";
        this.marker_long_name = "";
        this.plate = "";
        this.analysis_center = "";
        this.angle_H = 0.0;
        this.vector_lenght_H = 0.0;
        this.angle_V = 0.0;
        this.vector_lenght_V = 0.0;
    }

    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public String getMarker_long_name() {
        return marker_long_name;
    }

    public void setMarker_long_name(String marker_long_name) {
        this.marker_long_name = marker_long_name;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getAnalysis_center() {
        return analysis_center;
    }

    public void setAnalysis_center(String analysis_center) {
        this.analysis_center = analysis_center;
    }

    public double getAngle_H() {
        return angle_H;
    }

    public void setAngle_H(double angle_H) {
        this.angle_H = angle_H;
    }

    public double getVector_lenght_H() {
        return vector_lenght_H;
    }

    public void setVector_lenght_H(double vector_lenght_H) {
        this.vector_lenght_H = vector_lenght_H;
    }

    public double getAngle_V() {
        return angle_V;
    }

    public void setAngle_V(double angle_V) {
        this.angle_V = angle_V;
    }

    public double getVector_lenght_V() {
        return vector_lenght_V;
    }

    public void setVector_lenght_V(double vector_lenght_V) {
        this.vector_lenght_V = vector_lenght_V;
    }

    

    
}
