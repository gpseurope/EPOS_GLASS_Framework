package CustomClasses;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class ProductDataAvailability {

    private int id_station ;
    private String marker  ;
    private int id_ac ;
    private String abbreviation;
    private int id_mi;
    private String data_type;
    private float version;
    private String doi ;
    private Date creation_date ;
    private Date min_epoch ;
    private Date max_epoch ;
    private String sampling_period ;

    public int getId_station() {
        return id_station;
    }

    public void setId_station(int id_station) {
        this.id_station = id_station;
    }

    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public int getId_ac() {
        return id_ac;
    }

    public void setId_ac(int id_ac) {
        this.id_ac = id_ac;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public int getId_mi() {
        return id_mi;
    }

    public void setId_mi(int id_mi) {
        this.id_mi = id_mi;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public float getVersion() {
        return version;
    }

    public void setVersion(float version) {
        this.version = version;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getMin_epoch() {
        return min_epoch;
    }

    public void setMin_epoch(Date min_epoch) {
        this.min_epoch = min_epoch;
    }

    public Date getMax_epoch() {
        return max_epoch;
    }

    public void setMax_epoch(Date max_epoch) {
        this.max_epoch = max_epoch;
    }

    public String getSampling_period() {
        return sampling_period;
    }

    public void setSampling_period(String sampling_period) {
        this.sampling_period = sampling_period;
    }

    @Override
    public String toString() {
        return "ProductDataAvailability{" +
                "id_station=" + id_station +
                ", marker='" + marker + '\'' +
                ", id_ac=" + id_ac +
                ", abbreviation='" + abbreviation + '\'' +
                ", id_mi=" + id_mi +
                ", data_type=" + data_type +
                ", version=" + version +
                ", doi='" + doi + '\'' +
                ", creation_date=" + creation_date +
                ", min_epoch=" + min_epoch +
                ", max_epoch=" + max_epoch +
                ", sampling_period=" + sampling_period +
                '}';
    }
    public String toJson() {
        return "{" +
                "  \"id_station\":" + id_station +
                ", \"marker\":\"" + marker + "\""+
                ", \"id_ac\":" + id_ac +
                ", \"abbreviation\":\"" + abbreviation +"\""+
                ", \"id_mi\":" + id_mi +
                ", \"data_type\":\"" + data_type +"\""+
                ", \"version\":" + version +
                ", \"doi\":\"" + doi +"\""+
                ", \"creation_date\":\"" + creation_date + "\""+
                ", \"min_epoch\":\"" + min_epoch + "\""+
                ", \"max_epoch\":\"" + max_epoch + "\""+
                ", \"sampling_period\":\"" + sampling_period + "\""+
                '}';
    }

    public static String ArrayListToJson(ArrayList<ProductDataAvailability> list){

        if (list==null || list.size()==0)
            return "[]";
        StringBuilder s=new StringBuilder("[");
        boolean first=true;
        for (ProductDataAvailability p : list){
            if (!first)  s.append(",");
            s.append(p.toJson());
            first=false;
        }
        s.append("]");
        return s.toString();
    }

    public ProductDataAvailability(ResultSet _rs) throws SQLException {
        this.setId_station(_rs.getInt("id_station"));
        this.setMarker(_rs.getString("marker"));
        this.setId_mi(_rs.getInt("id_mi"));
        this.setId_ac(_rs.getInt("id_ac"));
        this.setAbbreviation(_rs.getString("abbreviation"));
        this.setData_type(_rs.getString("data_type"));
        this.setVersion(_rs.getFloat("version"));
        this.setCreation_date(_rs.getDate("creation_date"));
        //p.setDoi(_rs.getString(_rs.getString("doi")));
        String doiString=null;
        try{
            doiString=_rs.getString("doi");
        } catch ( Exception _e){
            ;
        }
        this.setDoi(doiString);
        this.setMin_epoch(_rs.getDate("min_epoch"));
        this.setMax_epoch(_rs.getDate("max_epoch"));
        this.setSampling_period(_rs.getString("sampling_period"));
    }
}
