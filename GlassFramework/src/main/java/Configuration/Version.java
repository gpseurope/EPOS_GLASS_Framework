package Configuration;

import Glass.GlassConstants;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;


/**
 * Created by rgcouto on 14/12/17.
 * Modified 20/01/2018 by P. Crocker
 * Converted to POJO - Java Bean in order to inject into Web Pages
 * Added getBuildDate to try and get build date info
 */
public class Version implements java.io.Serializable{

    private String version;
    private String artifactId;
    private String buildNumber;
    private String buildDate;
    private String databaseName;
    private String databaseIP;


    public Version(){
        Properties properties = new Properties();
        try {
            properties.load(this.getClass().getResourceAsStream("/project.properties"));
            artifactId = properties.getProperty("artifactId");
            version = properties.getProperty("version");
            buildNumber = properties.getProperty("buildNumber");
            buildDate = properties.getProperty("buildDate");

            try (InputStream is = new FileInputStream("./GLASS.conf") ) {
                Properties prop = new Properties();
                prop.load(is);
                is.close();
                databaseName = prop.getProperty("database");
                databaseIP = prop.getProperty("dbip");
            } catch (IOException e) {
                databaseName = "Unknown";
                databaseIP = "Unknown";
            }

        } catch (IOException e) {
            artifactId = "GlassFramework";
            version = "Unknown";
            buildNumber = "Unknown";
            buildDate = "Unknown";
            e.printStackTrace();
        }


    }

    public String getVersion() {

        return version;
    }

    public String getArtifactId() {

        return artifactId;
    }

    public String getBuildNumber() {

        return buildNumber;
    }

    public String getBuildDate() {

        return buildDate;

    }

    public String getDatabaseName() {
        return databaseName;
    }

    public String getDatabaseIP() {
        return databaseIP;
    }


    public String getNodeType() {
        if (GlassConstants.PRODUCTSPORTAL) return "Glass Products Node";
        return "Glass Data Node";
    }
}
