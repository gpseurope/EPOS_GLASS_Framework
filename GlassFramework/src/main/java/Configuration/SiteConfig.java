/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.security.PermitAll;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import static Configuration.DBConsts.DB_NAME;
import static Configuration.DBConsts.DB_PORT;

/**
 *
 * @author crocker
 */
@Singleton
@Startup // initialize at deployment time instead of first invocation
@PermitAll
public class SiteConfig {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    static private String myIP = null;
    static private String DBConnectionString = null;

    @PostConstruct
    void loadConfiguration() {

        // do the startup initialization here
        InetAddress localHostLANAddress;
        try {
            localHostLANAddress = getLocalHostLANAddress();
            myIP = localHostLANAddress.getHostAddress();
        } catch (UnknownHostException ex) {
            myIP = "error";
            Logger.getLogger(SiteConfig.class.getName()).log(Level.SEVERE, null, ex);
            //System.exit(2); cant call  System.exit without crashing Entire GlassFish Server
        }
        myIP = "127.0.0.1";
        DBConnectionString = "jdbc:postgresql://" + myIP + ":" + DB_PORT + "/" + DB_NAME;

        //Tries to load data from

        //System.out.println(System.getProperty("user.dir"));
        try (InputStream is = new FileInputStream("./GLASS.conf") ) {

            Properties prop = new Properties();
            prop.load(is);
            is.close();

            System.out.println("> Loading DB configuration");
            if(     prop.getProperty("dbip")        != null &&
                    prop.getProperty("dbport")      != null &&
                    prop.getProperty("database")    != null &&
                    prop.getProperty("dbuser")      != null &&
                    prop.getProperty("dbpassword")  != null
            ) {

                myIP = prop.getProperty("dbip");
                DBConsts.DB_USERNAME = prop.getProperty("dbuser");
                DBConsts.DB_PASSWORD = prop.getProperty("dbpassword");

                DBConnectionString = "jdbc:postgresql://" + myIP + ":" + prop.getProperty("dbport") + "/" + prop.getProperty("database");
                System.out.println("> DB configuration loaded");
            }

            System.out.println("> Loading email configuration");
            if(
                    prop.getProperty("username") != null &&
                    prop.getProperty("password") != null &&
                    prop.getProperty("smtpserver") != null &&
                    prop.getProperty("port") != null &&
                    prop.getProperty("debug") != null
            ) {
                DBConsts.GLASS_EMAIL_ADDRESS = prop.getProperty("username");
                DBConsts.GLASS_EMAIL_PASSWORD = prop.getProperty("password");
                DBConsts.GLASS_EMAIL_PORT = prop.getProperty("port");
                DBConsts.GLASS_EMAIL_SMTP_SERVER = prop.getProperty("smtpserver");
                DBConsts.GLASS_EMAIL_DEBUG = prop.getProperty("debug");
                System.out.println("> Email configuration loaded");
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("DBConnectionString: " + DBConnectionString);
    }

    @Lock(LockType.READ) // To allow multiple threads to invoke this method
    // simultaneously
    public String getLocalIpValue() {
        return myIP;
    }

    @Lock(LockType.READ) // To allow multiple threads to invoke this method
    // simultaneously
    public String getDBConnectionString() {
        return DBConnectionString;
    }

    /**
     * http://stackoverflow.com/questions/9481865/getting-the-ip-address-of-the-current-machine-using-java
     * Returns an <code>InetAddress</code> object encapsulating what is most
     * likely the machine's LAN IP address.
     * <p/>
     * This method is intended for use as a replacement of JDK method
     * <code>InetAddress.getLocalHost</code>, because that method is ambiguous
     * on Linux systems. Linux systems enumerate the loopback network interface
     * the same way as regular LAN network interfaces, but the JDK
     * <code>InetAddress.getLocalHost</code> method does not specify the
     * algorithm used to select the address returned under such circumstances,
     * and will often return the loopback address, which is not valid for
     * network communication. Details
     * <a href="http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4665037">here</a>.
     * <p/>
     * This method will scan all IP addresses on all network interfaces on the
     * host machine to determine the IP address most likely to be the machine's
     * LAN address. If the machine has multiple IP addresses, this method will
     * prefer a site-local IP address (e.g. 192.168.x.x or 10.10.x.x, usually
     * IPv4) if the machine has one (and will return the first site-local
     * address if the machine has more than one), but if the machine does not
     * hold a site-local address, this method will return simply the first
     * non-loopback address found (IPv4 or IPv6).
     * <p/>
     * If this method cannot find a non-loopback address using this selection
     * algorithm, it will fall back to calling and returning the result of JDK
     * method <code>InetAddress.getLocalHost</code>.
     * <p/>
     *
     * @throws UnknownHostException If the LAN address of the machine cannot be
     * found.
     */
    private static InetAddress getLocalHostLANAddress() throws UnknownHostException {
        System.out.println("Main IP Search Called");
        try {
            InetAddress candidateAddress = null;
            // Iterate all NICs (network interface cards)...
            for (Enumeration ifaces = NetworkInterface.getNetworkInterfaces(); ifaces.hasMoreElements();) {
                NetworkInterface iface = (NetworkInterface) ifaces.nextElement();
                // Iterate all IP addresses assigned to each card...
                for (Enumeration inetAddrs = iface.getInetAddresses(); inetAddrs.hasMoreElements();) {
                    InetAddress inetAddr = (InetAddress) inetAddrs.nextElement();
                    if (!inetAddr.isLoopbackAddress()) {

                        if (inetAddr.isSiteLocalAddress()) {
                            // Found non-loopback site-local address. Return it immediately...
                            return inetAddr;
                        } else if (candidateAddress == null) {
                            // Found non-loopback address, but not necessarily site-local.
                            // Store it as a candidate to be returned if site-local address is not subsequently found...
                            candidateAddress = inetAddr;
                            // Note that we don't repeatedly assign non-loopback non-site-local addresses as candidates,
                            // only the first. For subsequent iterations, candidate will be non-null.
                        }
                    }
                }
            }
            if (candidateAddress != null) {
                // We did not find a site-local address, but we found some other non-loopback address.
                // Server might have a non-site-local address assigned to its NIC (or it might be running
                // IPv6 which deprecates the "site-local" concept).
                // Return this non-loopback candidate address...
                return candidateAddress;
            }
            // At this point, we did not find a non-loopback address.
            // Fall back to returning whatever InetAddress.getLocalHost() returns...
            InetAddress jdkSuppliedAddress = InetAddress.getLocalHost();
            if (jdkSuppliedAddress == null) {
                throw new UnknownHostException("The JDK InetAddress.getLocalHost() method unexpectedly returned null.");
            }
            return jdkSuppliedAddress;
        } catch (Exception e) {
            UnknownHostException unknownHostException = new UnknownHostException("Failed to determine LAN address: " + e);
            unknownHostException.initCause(e);
            throw unknownHostException;
        }
    }
}
