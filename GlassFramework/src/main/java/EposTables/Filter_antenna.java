package EposTables;

/*
* FILTER_ANTENNA
* This class represents the filter_antenna table of the EPOS database.
*/
public class Filter_antenna extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the filter_antenna table
    private int id_item;                // Id_item field of the filter_antenna table
    private Attribute attribute;        // Attribute field of the filter_antenna table
    private Antenna_type antenna_type;  // Antenna_type field of the filter_antenna table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * FILTER_ANTENNA
    * This is the default constructor for the Filter_antenna class.
    */
    public Filter_antenna() {
        id = 0;
        id_item = 0;
        attribute = new Attribute();
        antenna_type = new Antenna_type();
    }

     /*
    * FILTER_ANTENNA
    * This constructor for the Filter_antenna class accepts values as parameters.
    */
    public Filter_antenna(int id, int id_item) {
        this.id = id;
        this.id_item = id_item;
        this.attribute = new Attribute();
        this.antenna_type = new Antenna_type();
    }
    
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET ID_ITEM
    * Returns the id_item value.
    */
    public int getId_item() {
        return id_item;
    }

    /*
    * SET ID_ITEM
    * Sets the value for the id_item.
    */
    public void setId_item(int id_item) {
        this.id_item = id_item;
    }

    /*
    * GET ATTRIBUTE
    * Returns the attribute value.
    */
    public Attribute getAttribute() {
        return attribute;
    }

    /*
    * SET ATTRIBUTE
    * Sets the value for the attribute.
    */
    public void setAttribute(int id_attribute, String name) {
        this.attribute.setId(id_attribute);
        this.attribute.setName(name);
    }

    /*
    * GET ANTENNA_TYPE
    * Returns the antenna_type value.
    */
    public Antenna_type getAntenna_type() {
        return antenna_type;
    }

    /*
    * SET ANTENNA_TYPE
    * Sets the value for the antenna_type.
    */
    public void setAntenna_type(int id_antenna_type, String name, 
            String igs_defined, String model) {
        this.antenna_type.setId(id_antenna_type);
        this.antenna_type.setName(name);
        this.antenna_type.setIgs_defined(igs_defined);
        this.antenna_type.setModel(model);
    }
}
