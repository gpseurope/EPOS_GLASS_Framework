package EposTables;

/*
* JUMP TYPE
* This class represents the Jump Type table of the EPOS product database.
*/
public class Jump_type extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                 // Id field of the jump type table
    private String type;            // Type field of the jump type table
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * JUMP TYPE
    * This is the default constructor for the Jump Type class.
    */
    public Jump_type() {
    }
    
    /*
    * JUMP TYPE
    * This constructor for the Jump Type class accepts values as parameters.
    */
    public Jump_type(int id, String type) {
        this.id = id;
        this.type = type;
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET TYPE
    * Returns the type value.
    */
    public String getType() {
        return type;
    }

    /*
    * SET TYPE
    * Sets the value for the type.
    */
    public void setType(String type) {
        this.type = type;
    }   
}
