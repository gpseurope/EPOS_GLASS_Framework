package EposTables;

/*
* ESTIMATED COORDINATES
* This class represents the Estimated Coordinates table of the EPOS product database.
*/
public class Estimated_coordinates extends EposTablesMasterClass  {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                                         // Id field of the estimated coordinates table
    private Station station;                                // Station field of the estimated coordinates table
    private String epoch;                                   // Epoch field of the estimated coordinates table
    private Method_identification method_identification;    // Method Identification field of the estimated coordinates table
    private double x;                                       // X field of the estimated coordinates table
    private double y;                                       // Y field of the estimated coordinates table
    private double z;                                       // Z field of the estimated coordinates table
    private double var_xx;                                      // var_xx field of the estimated coordinates table
    private double var_yy;                                      // var_yy field of the estimated coordinates table
    private double var_zz;                                      // var_zz field of the estimated coordinates table
    private double var_xy;                                      // var_xy field of the estimated coordinates table
    private double var_xz;                                      // var_xz field of the estimated coordinates table
    private double var_yz;                                      // var_yz field of the estimated coordinates table
    private Outliers outlier;                               // Outlier field of the estimated coordinates table
    private double MJD;
    private Processing_Parameters processing_parameters;    //id_processing_parameters of processing parameters table
    private Sinex_Files sinex_files;                         //id_sinex_files of sinex_files table
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * ESTIMATED COORDINATES
    * This is the default constructor for the Estimated Coordinates class.
    */

    public Estimated_coordinates(int id, Station station, String epoch, Method_identification method_identification, double x, double y, double z, double var_xx, double var_yy, double var_zz, double var_xy, double var_xz, double var_yz, Outliers outlier, double MJD, Processing_Parameters processing_parameters, Sinex_Files sinex_files) {
        this.id = id;
        this.station = station;
        this.epoch = epoch;
        this.method_identification = method_identification;
        this.x = x;
        this.y = y;
        this.z = z;
        this.var_xx = var_xx;
        this.var_yy = var_yy;
        this.var_zz = var_zz;
        this.var_xy = var_xy;
        this.var_xz = var_xz;
        this.var_yz = var_yz;
        this.outlier = outlier;
        this.MJD = MJD;
        this.processing_parameters = processing_parameters;
        this.sinex_files = sinex_files;
    }

    public Estimated_coordinates() {
        this.id = id;
        this.station = station;
        this.epoch = epoch;
        this.method_identification = method_identification;
        this.x = x;
        this.y = y;
        this.z = z;
        this.var_xx = var_xx;
        this.var_yy = var_yy;
        this.var_zz = var_zz;
        this.var_xy = var_xy;
        this.var_xz = var_xz;
        this.var_yz = var_yz;
        this.outlier = outlier;
        this.MJD = MJD;
        this.processing_parameters = processing_parameters;
        this.sinex_files = sinex_files;
    }

    /*
        * ESTIMATED COORDINATES
        * This constructor for the Estimated Coordinates class accepts values as parameters.
        */
    public Estimated_coordinates(int id, String epoch, double x, double y, double z,
                                 double var_xx, double var_yy, double var_zz, double var_xy, double var_xz, double var_yz, double MJD) {
        this.id = id;
        this.station = new Station();
        this.epoch = epoch;
        this.method_identification = new Method_identification();
        this.x = x;
        this.y = y;
        this.z = z;
        this.var_xx = var_xx;
        this.var_yy = var_yy;
        this.var_zz = var_zz;
        this.var_xy = var_xy;
        this.var_xz = var_xz;
        this.var_yz = var_yz;
        this.outlier = new Outliers();
        this.MJD=MJD;
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET STATION
    * Returns the station value.
    */
    public Station getStation() {
        return station;
    }

    /*
    * SET STATION
    * Sets the value for the station.
    */
    public void setStation(Station station) {
        this.station = station;
    }

    /*
    * GET EPOCH
    * Returns the epoch value.
    */
    public String getEpoch() {
        return epoch;
    }

    /*
    * SET EPOCH
    * Sets the value for the epoch.
    */
    public void setEpoch(String epoch) {
        this.epoch = epoch;
    }

    /*
    * GET METHOD IDENTIFICATION
    * Returns the method identification value.
    */
    public Method_identification getMethod_identification() {
        return method_identification;
    }

    /*
    * SET METHOD IDENTIFICATION
    * Sets the value for the method identification.
    */
    public void setMethod_identification(Method_identification method_identification) {
        this.method_identification = method_identification;
    }

    /*
    * GET X
    * Returns the x value.
    */
    public double getX() {
        return x;
    }

    /*
    * SET X
    * Sets the value for the x.
    */
    public void setX(double x) {
        this.x = x;
    }

    /*
    * GET Y
    * Returns the y value.
    */
    public double getY() {
        return y;
    }

    /*
    * SET Y
    * Sets the value for the y.
    */
    public void setY(double y) {
        this.y = y;
    }

    /*
    * GET Z
    * Returns the z value.
    */
    public double getZ() {
        return z;
    }

    /*
    * SET Z
    * Sets the value for the z.
    */
    public void setZ(double z) {
        this.z = z;
    }

    /*
    * GET SXX
    * Returns the var_xx value.
    */
    public double getVar_xx() {
        return var_xx;
    }

    /*
    * SET SXX
    * Sets the value for the var_xx.
    */
    public void setVar_xx(double var_xx) {
        this.var_xx = var_xx;
    }

    /*
    * GET SYY
    * Returns the var_yy value.
    */
    public double getVar_yy() {
        return var_yy;
    }

    /*
    * SET SYY
    * Sets the value for the var_yy.
    */
    public void setVar_yy(double var_yy) {
        this.var_yy = var_yy;
    }

    /*
    * GET SZZ
    * Returns the var_zz value.
    */
    public double getVar_zz() {
        return var_zz;
    }

    /*
    * SET SZZ
    * Sets the value for the var_zz.
    */
    public void setVar_zz(double var_zz) {
        this.var_zz = var_zz;
    }

    /*
    * GET SXY
    * Returns the var_xy value.
    */
    public double getVar_xy() {
        return var_xy;
    }

    /*
    * SET SXY
    * Sets the value for the var_xy.
    */
    public void setVar_xy(double var_xy) {
        this.var_xy = var_xy;
    }

    /*
    * GET SXZ
    * Returns the var_xz value.
    */
    public double getVar_xz() {
        return var_xz;
    }

    /*
    * SET SXZ
    * Sets the value for the var_xz.
    */
    public void setVar_xz(double var_xz) {
        this.var_xz = var_xz;
    }

    /*
    * GET SYZ
    * Returns the var_yz value.
    */
    public double getVar_yz() {
        return var_yz;
    }

    /*
    * SET SYZ
    * Sets the value for the var_yz.
    */
    public void setVar_yz(double var_yz) {
        this.var_yz = var_yz;
    }

    /*
    * GET SOLUTION NETWORK
    * Returns the solution network value.
    */

    public double getMJD() {
        return MJD;
    }

    public void setMJD(double MJD) {
        this.MJD = MJD;
    }

    /*
    * GET OUTLIER
    * Returns the outlier value.
    */
    public Outliers getOutlier() {
        return outlier;
    }

    /*
    * SET OUTLIER
    * Sets the value for the outlier.
    */
    public void setOutlier(Outliers outlier) {
        this.outlier.setId(outlier.getId());
        this.outlier.setDescription(outlier.getDescription());
        this.outlier.setType(outlier.getType());
    }

    public Processing_Parameters getProcessing_parameters() {
        return processing_parameters;
    }

    public void setProcessing_parameters(Processing_Parameters processing_parameters) {
        this.processing_parameters = processing_parameters;
    }

    public Sinex_Files getSinex_files() {
        return sinex_files;
    }

    public void setSinex_files(Sinex_Files sinex_files) {
        this.sinex_files = sinex_files;
    }
}
