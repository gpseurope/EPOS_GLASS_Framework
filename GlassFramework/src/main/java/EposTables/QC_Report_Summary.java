/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EposTables;

import java.util.ArrayList;

/**
 *
 * @author kngo
 */
public class QC_Report_Summary extends EposTablesMasterClass {
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     
    private int id_rinexfile;                
    private QC_Parameters qc_parameters;             
    private String date_beg;      
    private String date_end;
    private float data_smp;              
    private float obs_elev;        
    private int obs_have;       
    private int obs_expt;      
    private int user_have;       
    private int user_expt;       
    private int cyc_slps;       
    private int clk_jmps;         
    private int xbeg;     
    private int xend;
    private int xint;
    private int xsys;
    private ArrayList qc_constellation_summary; 
    private ArrayList qc_navigation_msg; 
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * QC_Report_Summary
    * This is the default constructor for the QC_Report_Summary class.
    */
    public QC_Report_Summary() {
        id = 0;
        id_rinexfile = 0;
        qc_parameters = new QC_Parameters();
        date_beg = "";
        date_end = "";
        data_smp = 0;
        obs_elev = 0;
        obs_have = 0;
        obs_expt = 0;
        user_have = 0;
        user_expt = 0;
        cyc_slps = 0;
        clk_jmps = 0;
        xbeg = 0;
        xend = 0;
        xint = 0;
        xsys = 0;
        qc_constellation_summary = new ArrayList();
        qc_navigation_msg = new ArrayList();
    }

    /*
    * QC_Report_Summary
    * This constructor for the QC_Report_Summary class accepts values as parameters.
    */
    public QC_Report_Summary(int id, int id_rinexfile, QC_Parameters qc_parameters, String date_beg, 
            String date_end, float data_smp, float obs_elev, 
            int obs_have, int obs_expt, int user_have,
            int user_expt, int cyc_slps,int clk_jmps, int xbeg, int xend, int xint, int xsys) {
        this.id = id;
        this.id_rinexfile = id_rinexfile;
        this.qc_parameters = qc_parameters;
        this.date_beg = date_beg;
        this.date_end = date_end;
        this.data_smp = data_smp;
        this.obs_elev = obs_elev;
        this.obs_have = obs_have;
        this.obs_expt = obs_expt;
        this.user_have = user_have;
        this.user_expt = user_expt;
        this.cyc_slps = cyc_slps;
        this.clk_jmps = clk_jmps;
        this.xbeg = xbeg;
        this.xend = xend;
        this.xint = xint;
        this.xsys = xsys;
        this.qc_constellation_summary = new ArrayList();
        this.qc_navigation_msg = new ArrayList();
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET RINEX ID
    * Returns the id rinex value.
    */
    public int getRinexId() {
        return id_rinexfile;
    }

    /*
    * SET RINEX ID
    * Sets the value for the id_rinexfile.
    */
    public void setRinexId(int id_rinexfile) {
        this.id_rinexfile = id_rinexfile;
    }

    /*
    * GET QC PARAMETERS
    * Returns the qc parameters value.
    */
    public QC_Parameters getQCParameters() {
        return qc_parameters;
    }

    /*
    * SET QC PARAMETERS
    * Sets the value for the qc parameters.
    */
    public void setQCParameters(QC_Parameters qc_parameters) {
        this.qc_parameters = qc_parameters;
    }
    
    /*
    * GET DATE BEGIN
    * Returns the date begin value.
    */
    public String getDateBeg() {
        return date_beg;
    }

    /*
    * SET DATE BEGIN
    * Sets the value for the date begin.
    */
    public void setDateBeg(String date_beg) {
        this.date_beg = date_beg;
    }
    
    /*
    * GET DATE END
    * Returns the date end value.
    */
    public String getDateEnd() {
        return date_end;
    }

    /*
    * SET DATE END
    * Sets the value for the date end.
    */
    public void setDateEnd(String date_end) {
        this.date_end = date_end;
    }
    
    /*
    * GET DATA SMP
    * Returns the data smp value.
    */
    public float getDataSmp() {
        return data_smp;
    }

    /*
    * SET DATA SMP
    * Sets the value for the data smp.
    */
    public void setDataSmp(float data_smp) {
        this.data_smp = data_smp;
    }
    
    /*
    * GET OBS ELEV
    * Returns the obs elev value.
    */
    public float getObsElev() {
        return obs_elev;
    }

    /*
    * SET OBS ELEV
    * Sets the value for the obs elev.
    */
    public void setObsElev(float obs_elev) {
        this.obs_elev = obs_elev;
    }
    
    /*
    * GET OBS HAVE
    * Returns the obs have value.
    */
    public int getObsHave() {
        return obs_have;
    }

    /*
    * SET OBS HAVE
    * Sets the value for the obs have.
    */
    public void setObsHave(int obs_have) {
        this.obs_have = obs_have;
    }

    /*
    * GET OBS EXPT
    * Returns the obs expt value.
    */
    public int getObsExpt() {
        return obs_expt;
    }

    /*
    * SET OBS EXPT
    * Sets the value for the obs expt.
    */
    public void setObsExpt(int obs_expt) {
        this.obs_expt = obs_expt;
    }
    
    /*
    * GET USER HAVE
    * Returns the user have value.
    */
    public int getUserHave() {
        return user_have;
    }

    /*
    * SET USER HAVE
    * Sets the value for the user have.
    */
    public void setUserHave(int user_have) {
        this.user_have = user_have;
    }

    /*
    * GET USER EXPT
    * Returns the user expt value.
    */
    public int getUserExpt() {
        return user_expt;
    }

    /*
    * SET USER EXPT
    * Sets the value for the obs expt.
    */
    public void setUserExpt(int user_expt) {
        this.user_expt = user_expt;
    }
    
    /*
    * GET CYC SLPS
    * Returns the cyc slps value.
    */
    public int getCycSlps() {
        return cyc_slps;
    }

    /*
    * SET CYC SLPS
    * Sets the value for the cyc slps.
    */
    public void setCycSlps(int cyc_slps) {
        this.cyc_slps = cyc_slps;
    }
    
    /*
    * GET CLK JMPS
    * Returns the clk jmps value.
    */
    public int getClkJmps() {
        return clk_jmps;
    }

    /*
    * SET CLK JMPS
    * Sets the value for the clk jmps.
    */
    public void setClkJmps(int clk_jmps) {
        this.clk_jmps = clk_jmps;
    }
    
    /*
    * GET X BEGIN
    * Returns the xbeg value.
    */
    public int getXBeg() {
        return xbeg;
    }

    /*
    * SET X BEGIN
    * Sets the value for the xbeg.
    */
    public void setXBeg(int xbeg) {
        this.xbeg = xbeg;
    }
    
    /*
    * GET X END
    * Returns the xend value.
    */
    public int getXEnd() {
        return xend;
    }

    /*
    * SET X END
    * Sets the value for the xend.
    */
    public void setXEnd(int xend) {
        this.xend = xend;
    }
    
    /*
    * GET X INT
    * Returns the xint value.
    */
    public int getXInt() {
        return xint;
    }

    /*
    * SET X INT
    * Sets the value for the xint.
    */
    public void setXInt(int xint) {
        this.xint = xint;
    }
    
    /*
    * GET X SYS
    * Returns the xsys value.
    */
    public int getXSys() {
        return xsys;
    }

    /*
    * SET X SYS
    * Sets the value for the xsys.
    */
    public void setXSys(int xsys) {
        this.xsys = xsys;
    }
    
    /**
    * ADD QC CONSTELLATION SUMMARY
    * Adds a new qc_constellation_summary to the list.
    */
    public void AddQCConstellationSummary(QC_Constellation_Summary qc_constellation_summary)
    {  
        this.qc_constellation_summary.add(qc_constellation_summary);
    }
    
    /**
    * GET QC CONSTELLATION SUMMARY
    * Returns the list of qc constellation summary.
    */
    public ArrayList<QC_Constellation_Summary> getQCConstellationSummary()
    {
        return qc_constellation_summary;
    }
    
    /**
    * ADD QC NAVIGATION MSG
    * Adds a new qc_navigation_msg to the list.
    */
    public void AddQCNavigationMsg(QC_Navigation_Msg qc_navigation_msg)
    {  
        this.qc_navigation_msg.add(qc_navigation_msg);
    }
    
    /**
    * GET QC NAVIGATION SUMMARY
    * Returns the list of qc navigation summary.
    */
    public ArrayList<QC_Navigation_Msg> getQCNavigationMsg()
    {
        return qc_navigation_msg;
    }
}
