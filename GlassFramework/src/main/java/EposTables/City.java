package EposTables;

/**
* USER_GROUP
* This class represents the user_group table of the EPOS database.
*/
public class City extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the city table
    private State state;                // State field of the city table
    private String name;                // Name field of the city table

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * CITY
    * This is the default constructor for the City class.
    */
    public City() {
        id = 0;
        state = new State();
        name = "";
    }

    /**
    * CITY
    * This constructor for the City class accepts values as parameters.
    */
    public City(int id, State state, String name) {
        this.id = id;
        this.state = state;
        this.name = name;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET ID_STATE
    * Returns the state object.
    */
    public State getState() {
        return state;
    }

    /**
    * SET STATE
    * Sets the values for the State.
    */
    public void setState(int id_state, Country country, String name) {
        this.state.setId(id_state);
        this.state.setCountry(country.getId(), country.getName(), country.getIso_code());
        this.state.setName(name);
    }

    /**
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /**
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }
}
