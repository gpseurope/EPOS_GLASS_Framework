package EposTables;

/**
* RECEIVER_TYPE
* This class represents the receiver_type table of the EPOS database.
*/
public class Receiver_type extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the receiver_type table
    private String name;                // Name field of the receiver_type table
    private String igs_defined;         // Igs_defined field of the receiver_type table
    private String model;               // Model field of the receiver_type table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * RECEIVER_TYPE
    * This is the default constructor for the Receiver_type class.
    */
    public Receiver_type() {
        id = 0;
        name = "";
        igs_defined = "";
        model = "";
    }

    /**
    * RECEIVER_TYPE
    * This constructor for the Receiver_type class accepts values as parameters.
    */
    public Receiver_type(int id, String name, String igs_defined, String model) {
        this.id = id;
        this.name = name;
        this.igs_defined = igs_defined;
        this.model = model;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /**
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }

    /**
    * GET IGS_DEFINED
    * Returns the igs_defined value.
    */
    public String getIgs_defined() {
        return igs_defined;
    }

    /**
    * SET IGS_DEFINED
    * Sets the value for the igs_defined.
    */
    public void setIgs_defined(String igs_defined) {
        this.igs_defined = igs_defined;
    }

    /**
    * GET MODEL
    * Returns the model value.
    */
    public String getModel() {
        return model;
    }

    /**
    * SET MODEL
    * Sets the value for the model.
    */
    public void setModel(String model) {
        this.model = model;
    }
}
