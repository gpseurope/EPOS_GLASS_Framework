package EposTables;

/**
* Product_files
* This class represents the Product_files table of the EPOS database.
*/
public class Product_files extends EposTablesMasterClass {

    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                 // Id field of the Product_files table
    private String url;             // Url field of the Product_files table
    private String epoch;           // Epoch field of the Product_files table


    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================


   /**
   * Product_files
   * This is the default constructor for the Product_files class.
   */

    public Product_files() {
        this.id = 0;
        this.url = "";
        this.epoch = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEpoch() {
        return epoch;
    }

    public void setEpoch(String epoch) {
        this.epoch = epoch;
    }

    public Product_files(int id, String url, String epoch) {
        this.id = id;
        this.url = url;
        this.epoch = epoch;
    }
}
