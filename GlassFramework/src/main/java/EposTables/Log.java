package EposTables;

/**
* LOG
* This class represents the log table of the EPOS database.
*/
public class Log extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                 // Id field of the log table
    private String title;           // Title field of the log table
    private String date;            // Date field of the log table
    private Log_type log_type;      // Log_type field of the log table
    private int id_station;         // Id_station field of the log table
    private String modified;        // Modified field of the log table
    private String previous;        // Previous field of the log table
    private Contact contact;        // Contact field of the log table
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * LOG
    * This is the default constructor for the Log class.
    */
    public Log() {
        id = 0;
        title = "";
        date = "";
        log_type = new Log_type();
        id_station = 0;
        modified = "";
        previous = "";
        contact = new Contact();
    }
    
    /**
    * LOG
    * This constructor for the Log class accepts values as parameters.
    */
    public Log(int id, String title, String date, Log_type log_type, int id_station, String modified, String previous, Contact contact) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.log_type = log_type;
        this.id_station = id_station;
        this.modified = modified;
        this.previous = previous;
        this.contact = contact;
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET TITLE
    * Returns the title value.
    */
    public String getTitle() {
        return title;
    }

    /**
    * SET TITLE
    * Sets the value for the title.
    */
    public void setTitle(String title) {
        this.title = title;
    }
    
    /**
    * GET DATE
    * Returns the date value.
    */
    public String getDate() {
        return date;
    }

    /**
    * SET DATE
    * Sets the value for the date.
    */
    public void setDate(String date) {
        this.date = date;
    }

    /**
    * GET LOG_TYPE
    * Returns the log_type object.
    */
    public Log_type getLog_type() {
        return log_type;
    }

    /**
    * SET LOG_TYPE
    * Sets the values for the log_type.
    */
    public void setLog_type(int id_log_type, String name) {
        this.log_type.setId(id_log_type);
        this.log_type.setName(name);
    }

    /**
    * GET ID_STATION
    * Returns the id_station value.
    */
    public int getId_station() {
        return id_station;
    }

    /**
    * SET ID_STATION
    * Sets the value for the id_station.
    */
    public void setId_station(int id_station) {
        this.id_station = id_station;
    }

    /**
    * GET MOFIFIED
    * Returns the modified value.
    */
    public String getModified() {
        return modified;
    }

    /**
    * SET MODIFIED
    * Sets the value for the modified.
    */
    public void setModified(String modified) {
        this.modified = modified;
    }

    /**
    * GET ICONTACT
    * Returns the contact object.
    */
    public Contact getContact() {
        return contact;
    }

    /**
     * GET PREVIOUS
     * Returns the previous object.
     */
    public String getPrevious() {
        return previous;
    }

    /**
     * SET PREVIOUS
     * Sets the previous object.
     */
    public void setPrevious(String previous) {
        this.previous = previous;
    }


    public void setLog_type(Log_type log_type) {
        this.log_type = log_type;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
}
