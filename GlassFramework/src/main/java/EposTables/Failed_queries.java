package EposTables;

import java.util.Date;

/*
* FAILED QUERIES
* This class represents the failed queries table of the EPOS database.
*/
public class Failed_queries extends EposTablesMasterClass {

    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the connections table
    private String query;               // Query to be executed
    private Node destiny;               // Destiny node of the query
    private Date timeStamp;
    private String reason;

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * FAILED QUERIES
    * This is the default constructor for the Failed Queries class.
    */
    public Failed_queries() {
        this.destiny = new Node();
    }

    /*
    * FAILED QUERIES
    * This constructor for the Failed Queries class accepts values as parameters.
    */
    public Failed_queries(int id, String query, Node destiny, Date timeStamp, String reason) {
        this.id = id;
        this.query = query;
        this.destiny = destiny;
        this.timeStamp = timeStamp;
        this.reason = reason;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET Query
    * Returns the query.
    */
    public String getQuery() {
        return query;
    }

    /*
    * SET Query
    * Sets the value for the query.
    */
    public void setQuery(String query) { this.query = query; }

    /*
    * GET DESTINY
    * Returns the destiny node.
    */
    public Node getDestiny() {
        return destiny;
    }

    /*
    * SET DESTINY
    * Sets the value for the destiny node.
    */
    @SuppressWarnings("Duplicates")
    public void setDestiny(Node destiny) {
        this.destiny.setId(destiny.getId());
        this.destiny.setName(destiny.getName());
        this.destiny.setHost(destiny.getHost());
        this.destiny.setPort(destiny.getPort());
        this.destiny.setDbname(destiny.getDbname());
        this.destiny.setUsername(destiny.getUsername());
        this.destiny.setPassword(destiny.getPassword());
        this.destiny.setContact_name(destiny.getContact_name());
        this.destiny.setUrl(destiny.getUrl());
        this.destiny.setEmail(destiny.getEmail());
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
