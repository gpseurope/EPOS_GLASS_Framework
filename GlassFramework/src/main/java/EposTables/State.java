package EposTables;

/**
* STATE
* This class represents the state table of the EPOS database.
*/
public class State extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the state table
    private Country country;            // Country field of the state table
    private String name;                // Name field of the state table
    
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * STATE
    * This is the default constructor for the State class.
    */
    public State() {
        id = 0;
        country = new Country();
        name = "";
    }

    /**
    * STATE
    * This constructor for the State class accepts values as parameters.
    */
    public State(int id, Country country, String name) {
        this.id = id;
        this.country = country;
        this.name = name;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET COUNTRY
    * Returns the country object.
    */
    public Country getCountry() {
        return country;
    }

    /**
    * SET ID_COUNTRY
    * Sets the values for the Country.
    */
    public void setCountry(int id_country, String name, String iso_code) {
        this.country.setId(id_country);
        this.country.setName(name);
        this.country.setIso_code(iso_code);
    }

    /**
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /**
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }
}
