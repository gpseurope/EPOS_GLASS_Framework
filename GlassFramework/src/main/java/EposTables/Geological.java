package EposTables;

/**
* GEOLOGICAL
* This class represents the geological table of the EPOS database.
*/
public class Geological extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                         // Id field of the geological table
    private Bedrock bedrock;                // Bedrock field of the geological table
    private String characteristic;          // Characteristic field of the geological table
    private String fracture_spacing;        // Fracture_spacing field of the geological table
    private String fault_zone;              // Fault_zone field of the geological table
    private String distance_to_fault;       // Distance_to_fault field of the geological table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * GEOLOGICAL
    * This is the default constructor for the Geological class.
    */
    public Geological() {
        id = 0;
        bedrock = new Bedrock();
        characteristic = "";
        fracture_spacing = "";
        fault_zone = "";
        distance_to_fault = "";
    }

    /**
    * GEOLOGICAL
    * This constructor for the Geological class accepts values as parameters.
    */
    public Geological(int id, Bedrock bedrock,  String characteristic, String fracture_spacing, String fault_zone, String distance_to_fault) {
        this.id = id;
        this.bedrock = bedrock;
        this.characteristic = characteristic;
        this.fracture_spacing = fracture_spacing;
        this.fault_zone = fault_zone;
        this.distance_to_fault = distance_to_fault;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET BEDROCK
    * Returns the bedrock object.
    */
    public Bedrock getBedrock() {
        return bedrock;
    }

    /**
    * SET BEDROCK
    * Sets the values for the bedrock.
    */
    public void setBedrock(int id_bedrock, String condition, String type) {
        this.bedrock.setId(id_bedrock);
        this.bedrock.setCondition(condition);
        this.bedrock.setType(type);
    }

    /**
     * SET BEDROCK
     * Sets the values for the bedrock.
     */
    public void setBedrock(Bedrock bedrock) {
        this.bedrock = bedrock;
    }

    /**
    * GET CHARACTERISTIC
    * Returns the characteristic value.
    */
    public String getCharacteristic() {
        return characteristic;
    }

    /**
    * SET CHARACTERISTIC
    * Sets the value for the characteristic.
    */
    public void setCharacteristic(String characteristic) {
        this.characteristic = characteristic;
    }

    /**
    * GET FRACTURE_SPACING
    * Returns the fracture_spacing value.
    */
    public String getFracture_spacing() {
        return fracture_spacing;
    }

    /**
    * SET FRACTURE_SPACING
    * Sets the value for the fracture_spacing.
    */
    public void setFracture_spacing(String fracture_spacing) {
        this.fracture_spacing = fracture_spacing;
    }

    /**
    * GET FAULT_ZONE
    * Returns the fault_zone value.
    */
    public String getFault_zone() {
        return fault_zone;
    }

    /**
    * SET FAULT_ZONE
    * Sets the value for the fault_zone.
    */
    public void setFault_zone(String fault_zone) {
        this.fault_zone = fault_zone;
    }

    /**
    * GET DISTANCE
    * Returns the distance value.
    */
    public String getDistance_to_fault() {
        return distance_to_fault;
    }

    /**
    * SET DISTANCE_TO_FAULT
    * Sets the value for the distance_to_fault.
    */
    public void setDistance_to_fault(String distance_to_fault) {
        this.distance_to_fault = distance_to_fault;
    }
}
