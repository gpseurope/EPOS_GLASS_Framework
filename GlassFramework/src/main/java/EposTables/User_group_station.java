package EposTables;

/**
* USER_GROUP_STATION
* This class represents the user_group_station table of the EPOS database.
*/
public class User_group_station extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;
    private User_group user_groups;
    private int id_stations;

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * USER_GROUP_STATION
    * This is the default constructor for the User_group_station class.
    */
    public User_group_station() {
        id = 0;
        user_groups = new User_group();
        id_stations = 0;
    }

    /**
    * USER_GROUP_STATION
    * This constructor for the User_group_station class accepts values as parameters.
    */
    public User_group_station(int id, User_group user_group, int id_stations) {
        this.id = id;
        this.user_groups = user_group;
        this.id_stations = id_stations;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET ID_USER_GROUPS
    * Returns the user_groups object.
    */
    public User_group getUser_groups() {
        return user_groups;
    }

    /**
    * SET USER_GROUPS
    * Sets the value for the user_groups.
    */
    public void setUser_groups(int id_user_groups, String name) {
        this.user_groups.setId(id_user_groups);
        this.user_groups.setName(name);
    }

    /**
    * GET ID_STATIONS
    * Returns the id_stations value.
    */
    public int getId_stations() {
        return id_stations;
    }

    /**
    * SET ID_STATIONS
    * Sets the value for the id_stations.
    */
    public void setId_stations(int id_stations) {
        this.id_stations = id_stations;
    }
}
