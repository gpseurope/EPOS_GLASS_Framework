package EposTables;

/**
* STATION_NETWORK
* This class represents the station_network table of the EPOS database.
*/
public class Station_network extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                 // Id field of the station_network table
    private int id_station;         // Id_station field of the station_network table
    private Network network;        // Network field of the station_network table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * STATION_NETWORK
    * This is the default constructor for the Station_network class.
    */
    public Station_network() {
        id = 0;
        id_station = 0;
        network = new Network();
    }

    /**
    * STATION_NETWORK
    * This constructor for the Station_network class accepts values as parameters.
    */
    public Station_network(int id, int id_station, Network network) {
        this.id = id;
        this.id_station = id_station;
        this.network = network;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET ID_STATION
    * Returns the id_station value.
    */
    public int getId_station() {
        return id_station;
    }

    /**
    * SET ID_STATION
    * Sets the value for the id_station.
    */
    public void setId_station(int id_station) {
        this.id_station = id_station;
    }

    /**
    * GET NETWORK
    * Returns the network object.
    */
    public Network getNetwork() {
        return network;
    }

    /**
    * SET NETWORK
    * Sets the values for the network.
    */
    public void setNetwork(Network network) {
        this.network = network;
    }
}
