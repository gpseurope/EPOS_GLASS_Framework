package EposTables;

/**
* Instrument_collocation
* This class represents the Instrument_collocation table of the EPOS database.
*/
public class Instrument_collocation extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;             // Id field of the Instrument_collocation table
    private int id_station;     // Id_station field of the Instrument_collocation table
    private String type;        // Type field of the Instrument_collocation table
    private String status;      // Status field of the Instrument_collocation table
    private String date_from;   // Date_from field of the Instrument_collocation table
    private String date_to;     // Date_to field of the Instrument_collocation table
    private String comment;     // Comment field of the Instrument_collocation table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * Instrument_collocation
    * This is the default constructor for the Instrument_collocation class.
    */
    public Instrument_collocation() {
        id = 0;
        id_station = 0;
        type = "";
        status = "";
        date_from = "";
        date_to = "";
        comment = "";
    }

    /**
    * Instrument_collocation
    * This constructor for the Instrument_collocation class accepts values as parameters.
    */
    public Instrument_collocation(int id, int id_station, String type, String status, String date_from, String date_to, String comment) {
        this.id = id;
        this.id_station = id_station;
        this.type = type;
        this.status = status;
        this.date_from = date_from;
        this.date_to = date_to;
        this.comment = comment;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET ID_STATION
    * Returns the id_station value.
    */
    public int getId_station() {
        return id_station;
    }

    /**
    * SET ID_STATION
    * Sets the value for the id_station.
    */
    public void setId_station(int id_station) {
        this.id_station = id_station;
    }

    /**
    * GET TYPE
    * Returns the type value.
    */
    public String getType() {
        return type;
    }

    /**
    * SET TYPE
    * Sets the value for the type.
    */
    public void setType(String type) {
        this.type = type;
    }

    /**
    * GET STATUS
    * Returns the status value.
    */
    public String getStatus() {
        return status;
    }

    /**
    * SET STATUS
    * Sets the value for the status.
    */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
    * GET DATE_FROM
    * Returns the date_from value.
    */
    public String getDate_from() {
        return date_from;
    }

    /**
    * SET DATE_FROM
    * Sets the value for the date_from.
    */
    public void setDate_from(String date_from) {
        this.date_from = date_from;
    }

    /**
    * GET DATE_TO
    * Returns the date_to value.
    */
    public String getDate_to() {
        return date_to;
    }

    /**
    * SET DATE_TO
    * Sets the value for the date_to.
    */
    public void setDate_to(String date_to) {
        this.date_to = date_to;
    }

    /**
    * GET COMMENT
    * Returns the comment value.
    */
    public String getComment() {
        return comment;
    }

    /**
    * SET COMMENT
    * Sets the value for the comment.
    */
    public void setComment(String comment) {
        this.comment = comment;
    }
}
