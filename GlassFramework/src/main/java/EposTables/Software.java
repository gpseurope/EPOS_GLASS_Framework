package EposTables;

/*
* SOFTWARE
* This class represents the Software table of the EPOS product database.
*/
public class Software extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                 // Id field of the Software table
    private String name;            // Name field of the Software table
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * SOFTWARE
    * This is the default constructor for the Software class.
    */
    public Software() {
    }

    /*
    * SOFTWARE
    * This constructor for the Software class accepts values as parameters.
    */
    public Software(int id, String name) {
        this.id = id;
        this.name = name;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /*
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }
}
