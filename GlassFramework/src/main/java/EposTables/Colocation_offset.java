package EposTables;

/**
* COLOCATION_OFFSET
* This class represents the colocation_offset table of the EPOS database.
*/
public class Colocation_offset extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                                 // Id field of the colocation_offset table
    private Station_colocation station_colocation;  // Station_colocation field of the colocation_offset table
    private float offset_x;                         // Offset_x field of the colocation_offset table
    private float offset_y;                         // Offset_y field of the colocation_offset table
    private float offset_z;                         // Offset_z field of the colocation_offset table
    private String date_measured;                   // Date_measured field of the colocation_offset table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * COLOCATION_OFFSET
    * This is the default constructor for the Colocation_offset class.
    */
    public Colocation_offset() {
        id = 0;
        station_colocation = new Station_colocation();
        offset_x = 0;
        offset_y = 0;
        offset_z = 0;
        date_measured = "";
    }

    /**
    * COLOCATION_OFFSET
    * This constructor for the Colocation_offset class accepts values as parameters.
    */
    public Colocation_offset(int id, float offset_x, float offset_y, float offset_z, String date_measured) {
        this.id = id;
        this.station_colocation = station_colocation;
        this.offset_x = offset_x;
        this.offset_y = offset_y;
        this.offset_z = offset_z;
        this.date_measured = date_measured;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET STATION_CLOCATION
    * Returns the station_colocation value.
    */
    public Station_colocation getStation_colocation() {
        return station_colocation;
    }

    /**
    * SET STATION_COLOCATION
    * Sets the values for the station_colocation.
    */
    public void setStation_colocation(Station_colocation station_colocation) {
        this.station_colocation = station_colocation;
    }

    /**
    * GET OFFSET_X
    * Returns the offset_x value.
    */
    public float getOffset_x() {
        return offset_x;
    }

    /**
    * SET OFFSET_X
    * Sets the value for the offset_x.
    */
    public void setOffset_x(float offset_x) {
        this.offset_x = offset_x;
    }

    /**
    * GET OFFSET_Y
    * Returns the offset_y value.
    */
    public float getOffset_y() {
        return offset_y;
    }

    /**
    * SET OFFSET_Y
    * Sets the value for the offset_y.
    */
    public void setOffset_y(float offset_y) {
        this.offset_y = offset_y;
    }

    /**
    * GET OFFSET_Z
    * Returns the offset_z value.
    */
    public float getOffset_z() {
        return offset_z;
    }

    /**
    * SET OFFSET_Z
    * Sets the value for the offset_z.
    */
    public void setOffset_z(float offset_z) {
        this.offset_z = offset_z;
    }

    /**
    * GET DATE_MEASURED
    * Returns the date_measured value.
    */
    public String getDate_measured() {
        return date_measured;
    }

    /**
    * SET DATE_MEASURED
    * Sets the value for the date_measured.
    */
    public void setDate_measured(String date_measured) {
        this.date_measured = date_measured;
    }  
}
