/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EposTables;

/**
 *
 * @author kngo
 */
public class QC_Navigation_Msg extends EposTablesMasterClass {
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     
    private int id_qc_report_summary;                
    private Constellation constellation;             
    private int nsat;      
    private int have;



    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * QC_Navigation_Msg
    * This is the default constructor for the QC_Navigation_Msg class.
    */
    public QC_Navigation_Msg() {
        id = 0;
        id_qc_report_summary = 0;                
        constellation = new Constellation();             
        nsat = 0;      
        have = 0;
    }

    /*
    * QC_Navigation_Msg
    * This constructor for the QC_Navigation_Msg class accepts values as parameters.
    */
    public QC_Navigation_Msg(int id, int id_qc_report_summary, Constellation constellation, int nsat, int have) 
    {
        this.id = id;
        this.id_qc_report_summary = id_qc_report_summary;                
        this.constellation = constellation;             
        this.nsat = nsat;      
        this.have = have;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET ID QC REPORT SUMMARY
    * Returns the id_qc_report_summary value.
    */
    public int getIdQcReportSummary() {
        return id_qc_report_summary;
    }

    /*
    * SET ID QC REPORT SUMMARY
    * Sets the value for the id_qc_report_summary.
    */
    public void setIdQcReportSummary(int id_qc_report_summary) {
        this.id_qc_report_summary = id_qc_report_summary;
    }

    /*
    * GET CONSTELLATION
    * Returns the constellation value.
    */
    public Constellation getConstellation() {
        return constellation;
    }

    /*
    * SET CONSTELLATION
    * Sets the value for the constellation.
    */
    public void setConstellation(Constellation constellation) {
        this.constellation = constellation;
    }
    
    /*
    * GET NSAT
    * Returns the nsat value.
    */
    public int getNsat() {
        return nsat;
    }

    /*
    * SET NSAT
    * Sets the value for the nsat.
    */
    public void setNsat(int nsat) {
        this.nsat = nsat;
    }
    
    /*
    * GET HAVE
    * Returns the have value.
    */
    public int getHave() {
        return have;
    }

    /*
    * SET HAVE
    * Sets the value for the have.
    */
    public void setHave(int have) {
        this.have = have;
    }
    
    
}
