package EposTables;

/**
* EFFECTS
* This class represents the effects table of the EPOS database.
*/
public class Effects extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the effects table
    private String type;                // Type field of the effects table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * EFFECTS
    * This is the default constructor for the Effects class.
    */
    public Effects() {
        id = 0;
        type = "";
    }

    /**
    * EFFECTS
    * This constructor for the Effects class accepts values as parameters.
    */
    public Effects(int id, String type) {
        this.id = id;
        this.type = type;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
    * GET TYPE
    * Returns the type value.
    */
    public String getType() {
        return type;
    }

    /**
    * SET TYPE
    * Sets the value for the type.
    */
    public void setType(String type) {
        this.type = type;
    }
}
