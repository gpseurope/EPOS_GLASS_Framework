package EposTables;

/*
* SOLUTION NETWORK
* This class represents the Solution Network table of the EPOS product database.
*/
public class Solution_network extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                 // Id field of the solution network table
    private String url;             // Url field of the solution network table
    private Helmert helmert;        // Helmert field of the solution network table
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * SOLUTION NETWORK
    * This is the default constructor for the Solution Network class.
    */
    public Solution_network() {
        this.id = 0;
        this.url = "";
        this.helmert = new Helmert();
    }

    /*
    * SOLUTION NETWORK
    * This constructor for the Solution Network class accepts values as parameters.
    */
    public Solution_network(int id, String url) {
        this.id = id;
        this.url = url;
        this.helmert = new Helmert();
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET URL
    * Returns the url value.
    */
    public String getUrl() {
        return url;
    }

    /*
    * SET URL
    * Sets the value for the url.
    */
    public void setUrl(String url) {
        this.url = url;
    }

    /*
    * GET HELMERT
    * Returns the helmert value.
    */
    public Helmert getHelmert() {
        return helmert;
    }

    /*
    * SET HELMERT
    * Sets the value for the helmert.
    */
    public void setHelmert(int id, double cx, double cy, double cz, double s, double rx, 
            double ry, double rz, Reference_frame old_frame, Reference_frame new_frame,
            Agency agency) {
        this.helmert.setId(id);
        this.helmert.setCx(cx);
        this.helmert.setCy(cy);
        this.helmert.setCz(cz);
        this.helmert.setS(s);
        this.helmert.setRx(rx);
        this.helmert.setRy(ry);
        this.helmert.setRz(rz);
        this.helmert.setOld_frame(old_frame.getId(), old_frame.getName(), old_frame.getEpoch());
        this.helmert.setNew_frame(new_frame.getId(), new_frame.getName(), new_frame.getEpoch());
        this.helmert.setAgency(agency.getId(), agency.getName(), agency.getAbbreviation(), 
                agency.getAddress(), agency.getWww(), agency.getInfos());
    }
}
