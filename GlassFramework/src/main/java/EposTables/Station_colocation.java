package EposTables;

/**
* STATION_COLOCATION
* This class represents the station_colocation table of the EPOS database.
*/
public class Station_colocation extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                                             // Id field of the station_colocation table
    private Station station;                                     // Id_station field of the station_colocation table
    private Station station_colocated;                          // Stattion_colocated field of the station_colocation table

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * STATION_COLOCATION
    * This is the default constructor for the Station_colocation class.
    */
    public Station_colocation() {
        id = 0;
        station = new Station();
        station_colocated = new Station();
    }

    /**
    * STATION_COLOCATION
    * This constructor for the Station_colocation class accepts values as parameters.
    */
    public Station_colocation(int id, Station station, Station station_colocated) {
        this.id = id;
        this.station = station;
        this.station_colocated = station_colocated;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET STATION
    * Returns the station value.
    */
    public int getIdStation() {
        return station.getId();
    }

    /**
    * SET STATION
    * Sets the value for the station.
    */
    public void setStation(Station station) {
        this.station = station;
    }

    /**
    * GET STATION_COLOCATED
    * Returns the id_station_colocated value.
    */
    public Station getStation_colocated() {
        return station_colocated;
    }

}
