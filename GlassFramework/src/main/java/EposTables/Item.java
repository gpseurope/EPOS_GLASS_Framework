package EposTables;

import java.util.ArrayList;

/**
* ITEM
* This class represents the item table of the EPOS database.
*/
public class Item extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                                     // Id field of the item table
    private Item_type item_type;                        // Item_type field of the item table
    private Contact contact_as_producer;                // Contact_as_producer field of the item table
    private Contact contact_as_owner;                   // Contact_as_owner field of the item table
    private String comment;                             // String field of the item table
    private ArrayList item_attributes;                  // List of item attributes

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * ITEM
    * This is the default constructor for the Item class.
    */
    public Item() {
        id = 0;
        item_type = new Item_type();
        contact_as_producer = new Contact();
        contact_as_owner = new Contact();
        comment = "";
        item_attributes = new ArrayList();
    }

    /**
    * ITEM
    * This constructor for the Item class accepts values as parameters.
    */
    public Item(int id, int id_item_type, int id_contact_as_producer, 
            int id_contact_as_owner, String comment) {
        this.id = id;
        this.item_type = new Item_type();
        this.contact_as_producer = new Contact();
        this.contact_as_owner = new Contact();
        this.comment = comment;
        this.item_attributes = new ArrayList();
    }


    /**
     * ITEM
     * This constructor for the Item class accepts values as parameters.
     */
    public Item(int id, Item_type item_type, String comment) {
        this.id = id;
        this.item_type = item_type;
        this.contact_as_producer = new Contact();
        this.contact_as_owner = new Contact();
        this.comment = comment;
        this.item_attributes = new ArrayList();
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET ITEM_TYPE
    * Returns the item_type value.
    */
    public Item_type getItem_type() {
        return item_type;
    }

    /**
    * SET ITEM_TYPE
    * Sets the value for the item_type.
    */
    public void setItem_type(int id_item_type, String name) {
        this.item_type.setId(id_item_type);
        this.item_type.setName(name);
    }

    /**
    * GET CONTACT_AS_PRODUCER
    * Returns the contact_as_producer value.
    */
    public Contact getContact_as_producer() {
        return contact_as_producer;
    }

    /**
    * SET CONTACT_AS_PRODUCER
    * Sets the value for the contact_as_producer.
    */
    public void setContact_as_producer(int id_contact, String name, String title, 
            String email, String phone, String gms, String comment,
            Agency agency, String role) {
        this.contact_as_producer.setId(id_contact);
        this.contact_as_producer.setName(name);
        this.contact_as_producer.setTitle(title);
        this.contact_as_producer.setEmail(email);
        this.contact_as_producer.setPhone(phone);
        this.contact_as_producer.setGsm(gms);
        this.contact_as_producer.setComment(comment);
        this.contact_as_producer.setAgency(agency.getId(), agency.getName(), 
                agency.getAbbreviation(), agency.getAddress(), agency.getWww(), 
                agency.getInfos());
        this.contact_as_producer.setRole(role);
    }

    /**
    * GET CONTACT_AS_OWNER
    * Returns the contact_as_owner value.
    */
    public Contact getContact_as_owner() {
        return contact_as_owner;
    }

    /**
    * SET CONTACT_AS_OWNER
    * Sets the value for the contact_as_owner.
    */
    public void setContact_as_owner(int id_contact, String name, String title, 
            String email, String phone, String gms, String comment,
            Agency agency, String role) {
        this.contact_as_owner.setId(id_contact);
        this.contact_as_owner.setName(name);
        this.contact_as_owner.setTitle(title);
        this.contact_as_owner.setEmail(email);
        this.contact_as_owner.setPhone(phone);
        this.contact_as_owner.setGsm(gms);
        this.contact_as_owner.setComment(comment);
        this.contact_as_owner.setAgency(agency.getId(), agency.getName(), 
                agency.getAbbreviation(), agency.getAddress(), agency.getWww(), 
                agency.getInfos());
        this.contact_as_owner.setRole(role);
    }

    /**
    * GET COMMENT
    * Returns the comment value.
    */
    public String getComment() {
        return comment;
    }

    /**
    * SET COMMENT
    * Sets the value for the comment.
    */
    public void setComment(String comment) {
        this.comment = comment;
    }
    
    /**
    * GET ITEM_ATTRIBUTES
    * Returns the list of attributes fo the item.
    */
    public ArrayList<Item_attribute> GetItemAttribues()
    {
        return item_attributes;
    }
    
    /**
    * ADD ITEM_ATTRIBUTE
    * Adds a new item attribute to the list.
    */
    public void AddItemAttribute(Item_attribute item_attribute)
    {  
        this.item_attributes.add(item_attribute);
    }
}
