package EposTables;

/**
* ITEM_TYPE
* This class represents the item_type table of the EPOS database.
*/
public class Item_type extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the item_type table
    private String name;                // Name field of the item_type table

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * ITEM_TYPE
    * This is the default constructor for the Item_type class.
    */
    public Item_type() {
        id = 0;
        name = "";
    }

    /**
    * ITEM_TYPE
    * This constructor for the Item_type class accepts values as parameters.
    */
    public Item_type(int id, String name) {
        this.id = id;
        this.name = name;
    }
    
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /**
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }
}
