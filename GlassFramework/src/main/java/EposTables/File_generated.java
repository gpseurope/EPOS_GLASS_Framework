package EposTables;

/*
* FILE_GENERATED
* This class represents the file_generated table of the EPOS database.
*/
public class File_generated extends EposTablesMasterClass  {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the file_generated table
    private int id_station;             // Id_station field of the file_generated table
    private File_type file_type;        // File_type field of the file_generated table
    private String station_marker;      // Marker field of the station

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * FILE_GENERATED
    * This is the default constructor for the File_generated class.
    */
    public File_generated() {
        id = 0;
        id_station = 0;
        file_type = new File_type();
        station_marker = "";
    }

    /*
    * FILE_GENERATED
    * This constructor for the File_generated class accepts values as parameters.
    */
    public File_generated(int id, int id_station) {
        this.id = id;
        this.id_station = id_station;
        this.file_type = new File_type();
        station_marker = "";
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET ID_STATION
    * Returns the id_station value.
    */
    public int getId_station() {
        return id_station;
    }

    /*
    * SET ID_STATION
    * Sets the value for the id_station.
    */
    public void setId_station(int id_station) {
        this.id_station = id_station;
    }

    /*
    * GET FILE_TYPE
    * Returns the file_type value.
    */
    public File_type getFile_type() {
        return file_type;
    }

    /*
    * SET FILE_TYPE
    * Sets the values for the file_type.
    */
    public void setFile_type(int id_file_type, String format) {
        this.file_type.setId(id_file_type);
        this.file_type.setFormat(format);
    }

    /*
    * GET STATION_MARKER
    * Returns the station_marker value.
    */
    public String getStation_marker() {
        return station_marker;
    }

    /*
    * SET STATION_MARKER
    * Sets the values for the station_marker.
    */
    public void setStation_marker(String station_marker) {
        this.station_marker = station_marker;
    }
    
    
}
