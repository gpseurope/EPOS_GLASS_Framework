package EposTables;

/*
* FILTER_RADOME
* This class represents the filter_radome table of the EPOS database.
*/
public class Filter_radome extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the filter_radome table
    private int id_item;                // Id_item field of the filter_radome table
    private Attribute attribute;        // Attribute field of the filter_radome table
    private Radome_type radome_type;    // Radome_type field of the filter_radome table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * FILTER_RADOME
    * This is the default constructor for the Filter_radome class.
    */
    public Filter_radome() {
        id = 0;
        id_item = 0;
        attribute = new Attribute();
        radome_type = new Radome_type();
    }

    /*
    * FILTER_RADOME
    * This constructor for the Filter_radome class accepts values as parameters.
    */
    public Filter_radome(int id, int id_item) {
        this.id = id;
        this.id_item = id_item;
        this.attribute = new Attribute();
        this.radome_type = new Radome_type();
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET ID_ITEM
    * Returns the id_item value.
    */
    public int getId_item() {
        return id_item;
    }

    /*
    * SET ID_ITEM
    * Sets the value for the id_item.
    */
    public void setId_item(int id_item) {
        this.id_item = id_item;
    }

    /*
    * GET ATTRIBUTE
    * Returns the attribute value.
    */
    public Attribute getAttribute() {
        return attribute;
    }

    /*
    * SET ATTRIBUTE
    * Sets the values for the attribute.
    */
    public void setAttribute(int id_attribute, String name) {
        this.attribute.setId(id_attribute);
        this.attribute.setName(name);
    }

    /*
    * GET RADOME_TYPE
    * Returns the radome_type value.
    */
    public Radome_type getRadome_type() {
        return radome_type;
    }

    /*
    * SET RADOME_TYPE
    * Sets the values for the radome_type.
    */
    public void setRadome_type(int id_radome_type, String name, 
            String igs_defined, String description) {
        this.radome_type.setId(id_radome_type);
        this.radome_type.setName(name);
        this.radome_type.setIgs_defined(igs_defined);
        this.radome_type.setDescription(description);
    }
}
