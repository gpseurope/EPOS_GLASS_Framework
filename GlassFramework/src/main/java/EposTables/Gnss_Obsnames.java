/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EposTables;

/**
 *
 * @author kngo
 */
public class Gnss_Obsnames extends EposTablesMasterClass {
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     
    private String name;                
    private String obstype;             
    private String channel;      
    private int frequency_band;
    private boolean official; 


    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * Gnss_Obsnames
    * This is the default constructor for the Gnss_Obsnames class.
    */
    public Gnss_Obsnames() {
        id = 0;
        name = "";                
        obstype = "";             
        channel = "";
        frequency_band = 0;
        official = false;
    }

    /*
    * Gnss_Obsnames
    * This constructor for the Gnss_Obsnames class accepts values as parameters.
    */
    public Gnss_Obsnames(int id, String name, String obstype, String channel, int frequency_band, boolean official) 
    {
        this.id = id;
        this.name = name;                
        this.obstype = obstype;             
        this.channel = channel;
        this.frequency_band = frequency_band;
        this.official = official;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /*
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }
    
    /*
    * GET OBSTYPE
    * Returns the obstype value.
    */
    public String getObsType() {
        return obstype;
    }

    /*
    * SET OBSTYPE
    * Sets the value for the obstype.
    */
    public void setObsType(String obstype) {
        this.obstype = obstype;
    }

    /*
    * GET CHANNEL
    * Returns the channel value.
    */
    public String getChannel() {
        return channel;
    }

    /*
    * SET CHANNEL
    * Sets the value for the channel.
    */
    public void setChannel(String channel) {
        this.channel = channel;
    }
    
    /*
    * GET FREQUENCY BAND
    * Returns the frequency_band value.
    */
    public int getFrequencyBand() {
        return frequency_band;
    }

    /*
    * SET FREQUENCY BAND
    * Sets the value for the official.
    */
    public void setFrequencyBand(int frequency_band) {
        this.frequency_band = frequency_band;
    }
    
    /*
    * GET OFFICIAL
    * Returns the official value.
    */
    public boolean getOfficial() {
        return official;
    }

    /*
    * SET OFFICIAL
    * Sets the value for the official.
    */
    public void setOfficial(boolean official) {
        this.official = official;
    }
}
