/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EposTables;

import Glass.DataBaseT0Connection;

import java.util.ArrayList;

/**
 *
 * @author kngo
 */
public class Data_center_structure extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                         // Id field of the data center structure table
    private Data_center data_center;        
    private int id_file_type;               // id of the file type table
    private String directory_naming;       // directory naming field of the data center structure table
    private String comments;                 // comments field of the data center structure table
    private ArrayList<DataBaseT0Connection.DirStruct> dirStructs;
    private ArrayList<Node> nodeArrayList;
    

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * DATA_CENTER
    * This is the default constructor for the Data_center class.
    */
    public Data_center_structure() {
        id = 0;
        data_center = new Data_center();
        id_file_type = 0;
        directory_naming = "";
        comments = "";
    }

    /*
    * DATA_CENTER
    * This constructor for the Data_center class accepts values as parameters.
    */
    public Data_center_structure(int id, int id_file_type, String directory_naming, String comments) {
        this.id = id;
        this.data_center = new Data_center();
        this.id_file_type= id_file_type;
        this.directory_naming = directory_naming;
        this.comments = comments;
    }

    public Data_center_structure(Data_center dc, ArrayList<DataBaseT0Connection.DirStruct> dirStructs,
                                 ArrayList<Node> nodes){
        this.setDataCenter(dc);
        this.dirStructs = dirStructs;
        this.nodeArrayList = nodes;
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    
    /*
    * GET Data_center
    * Returns the list of Data_center.
    */
    public Data_center getDataCenter() {
        return data_center;
    }
    
    /*
    * Set Data_center
    * Set a new Data_center.
    */
    public void setDataCenter(Data_center data_center) {
        this.data_center = data_center;
    }
    
    
    /*
    * GET id_file_type
    * Returns the id_file_type value.
            */
    public int getId_file_type() {
        return id_file_type;
    }

    /*
        * SET id_file_type
        * Sets the value for the id_file_type.
        */
    public void setId_file_type(int id_file_type) {
        this.id_file_type = id_file_type;
    }


    /*
    * GET directory naming
    * Returns the directory naming value.
    */
    public String getDirectory_naming() {
        return directory_naming;
    }

    /*
    * SET directory naming
    * Sets the value for the directory naming.
    */
    public void setDirectory_naming(String directory_naming) {
        this.directory_naming = directory_naming;
    }

    /*
    * GET comments
    * Returns the comments value.
    */
    public String getComments() {
        return comments;
    }

    /*
    * SET comments
    * Sets the value for the comments.
    */
    public void setComments(String comments) {
        this.comments = comments;
    }


    public ArrayList<DataBaseT0Connection.DirStruct> getDirStructs() {
        return dirStructs;
    }

    public void setDirStructs(ArrayList<DataBaseT0Connection.DirStruct> dirStructs) {
        this.dirStructs = dirStructs;
    }

    public ArrayList<Node> getNodeArrayList() {
        return nodeArrayList;
    }

    public void setNodeArrayList(ArrayList<Node> nodeArrayList) {
        this.nodeArrayList = nodeArrayList;
    }
}

