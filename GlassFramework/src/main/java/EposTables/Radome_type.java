package EposTables;

/**
* RADOME_TYPE
* This class represents the radome_type table of the EPOS database.
*/
public class Radome_type extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int  id;                // Id field of the radome_type table
    private String name;            // Name field of the radome_type table
    private String igs_defined;     // Igs_name field of the radome_type table
    private String description;     // Description field of the radome_type table
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * RADOME_TYPE
    * This is the default constructor for the Radome_type class.
    */
    public Radome_type() {
        id = 0;
        name = "";
        igs_defined = "";
        description = "";
    }
    
    /**
    * RADOME_TYPE
    * This constructor for the Radome_type class accepts values as parameters.
    */
    public Radome_type(int id, String name, String igs_defined, String description) {
        this.id = id;
        this.name = name;
        this.igs_defined = igs_defined;
        this.description = description;
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }
    
    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }
    
    /**
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
    * GET IGS_DEFINED
    * Returns the igs_defined value.
    */
    public String getIgs_defined() {
        return igs_defined;
    }
    
    /**
    * SET IGS_DEFINED
    * Sets the value for the igs_defined.
    */
    public void setIgs_defined(String igs_defined) {
        this.igs_defined = igs_defined;
    }
    
    /**
    * GET DESCRIPTION
    * Returns the description value.
    */
    public String getDescription() {
        return description;
    }
    
    /**
    * SET DESCRIPTION
    * Sets the value for the description.
    */
    public void setDescription(String description) {
        this.description = description;
    }
}
