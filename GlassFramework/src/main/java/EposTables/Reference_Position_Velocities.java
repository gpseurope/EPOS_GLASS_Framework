package EposTables;

/**
 * Created by arodrigues on 7/26/17.
 */
public class Reference_Position_Velocities extends EposTablesMasterClass {
    int id;
    double velx;
    double vely;
    double velz;
    double velx_sigma;
    double vely_sigma;
    double velz_sigma;
    double vel_rho_xy;
    double vel_rho_xz;
    double vel_rho_yz;
    double reference_position_x;
    double reference_position_y;
    double reference_position_z;
    double reference_position_x_sigma;
    double reference_position_y_sigma;
    double reference_position_z_sigma;
    double reference_position_rho_xy;
    double reference_position_rho_xz;
    double reference_position_rho_yz;
    double version;
    String start_epoch;
    String end_epoch;
    String ref_epoch;
    int id_method_identification;
    int id_station;
    Sinex_Files sinex_files;
    Method_identification method_identification;
    Reference_frame reference_frame;
    Product_files product_files;


    public Reference_Position_Velocities(int id, double velx, double vely, double velz, double velx_sigma, double vely_sigma, double velz_sigma, double vel_rho_xy, double vel_rho_xz, double vel_rho_yz, double reference_position_x, double reference_position_y, double reference_position_z, double reference_position_x_sigma, double reference_position_y_sigma, double reference_position_z_sigma, double reference_position_rho_xy, double reference_position_rho_xz, double reference_position_rho_yz, String start_epoch, String end_epoch, String ref_epoch, int id_method_identification, int id_station, Sinex_Files sinex_files, Method_identification method_identification, Reference_frame reference_frame) {
        this.id = id;
        this.velx = velx;
        this.vely = vely;
        this.velz = velz;
        this.velx_sigma = velx_sigma;
        this.vely_sigma = vely_sigma;
        this.velz_sigma = velz_sigma;
        this.vel_rho_xy = vel_rho_xy;
        this.vel_rho_xz = vel_rho_xz;
        this.vel_rho_yz = vel_rho_yz;
        this.reference_position_x = reference_position_x;
        this.reference_position_y = reference_position_y;
        this.reference_position_z = reference_position_z;
        this.reference_position_x_sigma = reference_position_x_sigma;
        this.reference_position_y_sigma = reference_position_y_sigma;
        this.reference_position_z_sigma = reference_position_z_sigma;
        this.reference_position_rho_xy = reference_position_rho_xy;
        this.reference_position_rho_xz = reference_position_rho_xz;
        this.reference_position_rho_yz = reference_position_rho_yz;
        this.start_epoch = start_epoch;
        this.end_epoch = end_epoch;
        this.ref_epoch = ref_epoch;
        this.id_method_identification = id_method_identification;
        this.id_station = id_station;
        this.sinex_files = sinex_files;
        this.method_identification = method_identification;
        this.reference_frame = reference_frame;

        //--- We have enough information to obtain the version information as well
        //this.version = method_identification.getVersion();
    }

    public Reference_Position_Velocities() {
        this.id = id;
        this.velx = velx;
        this.vely = vely;
        this.velz = velz;
        this.velx_sigma = velx_sigma;
        this.vely_sigma = vely_sigma;
        this.velz_sigma = velz_sigma;
        this.vel_rho_xy = vel_rho_xy;
        this.vel_rho_xz = vel_rho_xz;
        this.vel_rho_yz = vel_rho_yz;
        this.reference_position_x = reference_position_x;
        this.reference_position_y = reference_position_y;
        this.reference_position_z = reference_position_z;
        this.reference_position_x_sigma = reference_position_x_sigma;
        this.reference_position_y_sigma = reference_position_y_sigma;
        this.reference_position_z_sigma = reference_position_z_sigma;
        this.reference_position_rho_xy = reference_position_rho_xy;
        this.reference_position_rho_xz = reference_position_rho_xz;
        this.reference_position_rho_yz = reference_position_rho_yz;
        this.start_epoch = start_epoch;
        this.end_epoch = end_epoch;
        this.ref_epoch = ref_epoch;
        this.id_method_identification = id_method_identification;
        this.id_station = id_station;
        this.sinex_files = sinex_files;
        this.method_identification = method_identification;
        this.reference_frame = reference_frame;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getVelx() {
        return velx;
    }

    public void setVelx(double velx) {
        this.velx = velx;
    }

    public double getVely() {
        return vely;
    }

    public void setVely(double vely) {
        this.vely = vely;
    }

    public double getVelz() {
        return velz;
    }

    public void setVelz(double velz) {
        this.velz = velz;
    }

    public double getVelx_sigma() {
        return velx_sigma;
    }

    public void setVelx_sigma(double velx_sigma) {
        this.velx_sigma = velx_sigma;
    }

    public double getVely_sigma() {
        return vely_sigma;
    }

    public void setVely_sigma(double vely_sigma) {
        this.vely_sigma = vely_sigma;
    }

    public double getVelz_sigma() {
        return velz_sigma;
    }

    public void setVelz_sigma(double velz_sigma) {
        this.velz_sigma = velz_sigma;
    }

    public double getVel_rho_xy() {
        return vel_rho_xy;
    }

    public void setVel_rho_xy(double vel_rho_xy) {
        this.vel_rho_xy = vel_rho_xy;
    }

    public double getVel_rho_xz() {
        return vel_rho_xz;
    }

    public void setVel_rho_xz(double vel_rho_xz) {
        this.vel_rho_xz = vel_rho_xz;
    }

    public double getVel_rho_yz() {
        return vel_rho_yz;
    }

    public void setVel_rho_yz(double vel_rho_yz) {
        this.vel_rho_yz = vel_rho_yz;
    }

    public double getReference_position_x() {
        return reference_position_x;
    }

    public void setReference_position_x(double reference_position_x) {
        this.reference_position_x = reference_position_x;
    }

    public double getReference_position_y() {
        return reference_position_y;
    }

    public void setReference_position_y(double reference_position_y) {
        this.reference_position_y = reference_position_y;
    }

    public double getReference_position_z() {
        return reference_position_z;
    }

    public void setReference_position_z(double reference_position_z) {
        this.reference_position_z = reference_position_z;
    }

    public double getReference_position_x_sigma() {
        return reference_position_x_sigma;
    }

    public void setReference_position_x_sigma(double reference_position_x_sigma) {
        this.reference_position_x_sigma = reference_position_x_sigma;
    }

    public double getReference_position_y_sigma() {
        return reference_position_y_sigma;
    }

    public void setReference_position_y_sigma(double reference_position_y_sigma) {
        this.reference_position_y_sigma = reference_position_y_sigma;
    }

    public double getReference_position_z_sigma() {
        return reference_position_z_sigma;
    }

    public void setReference_position_z_sigma(double reference_position_z_sigma) {
        this.reference_position_z_sigma = reference_position_z_sigma;
    }

    public double getReference_position_rho_xy() {
        return reference_position_rho_xy;
    }

    public void setReference_position_rho_xy(double reference_position_rho_xy) {
        this.reference_position_rho_xy = reference_position_rho_xy;
    }

    public double getReference_position_rho_xz() {
        return reference_position_rho_xz;
    }

    public void setReference_position_rho_xz(double reference_position_rho_xz) {
        this.reference_position_rho_xz = reference_position_rho_xz;
    }

    public double getReference_position_rho_yz() {
        return reference_position_rho_yz;
    }

    public void setReference_position_rho_yz(double reference_position_rho_yz) {
        this.reference_position_rho_yz = reference_position_rho_yz;
    }

    public String getStart_epoch() {
        return start_epoch;
    }

    public void setStart_epoch(String start_epoch) {
        this.start_epoch = start_epoch;
    }

    public String getEnd_epoch() {
        return end_epoch;
    }

    public void setEnd_epoch(String end_epoch) {
        this.end_epoch = end_epoch;
    }

    public String getRef_epoch() {
        return ref_epoch;
    }

    public void setRef_epoch(String ref_epoch) {
        this.ref_epoch = ref_epoch;
    }

    public int getId_method_identification() {
        return id_method_identification;
    }

    public void setId_method_identification(int id_method_identification) {
        this.id_method_identification = id_method_identification;
    }

    public int getId_station() {
        return id_station;
    }

    public void setId_station(int id_station) {
        this.id_station = id_station;
    }

    public Sinex_Files getSinex_files() {
        return sinex_files;
    }

    public void setSinex_files(Sinex_Files sinex_files) {
        this.sinex_files = sinex_files;
    }

    public Method_identification getMethod_identification() {
        return method_identification;
    }

    public void setMethod_identification(Method_identification method_identification) {
        this.method_identification = method_identification;
    }

    public Reference_frame getReference_frame() {
        return reference_frame;
    }

    public void setReference_frame(Reference_frame reference_frame) {
        this.reference_frame = reference_frame;
    }

    public void setMethod_identification(String creation_date) {
        this.method_identification.setCreation_date(creation_date);
    }

    public void setReference_frame(String epoch) {
        this.reference_frame.setEpoch(epoch);
    }

    public Product_files getProduct_files() {
        return product_files;
    }

    public void setProduct_files(Product_files product_files) {
        this.product_files = product_files;
    }
}

