package EposTables;

/*
* OTHER FILE
* This class represents the other files table of the EPOS database.
*/
public class Other_files extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the other files table
    private String name;                // Name field of the other files table
    private Data_center data_center;    // Data center field of the other files table
    private int file_size;              // File size field of the other files table
    private File_type file_type;        // File type field of the other files table
    private String relative_path;       // Relative path field of the other files table
    private String creation_date;       // Date field of the other files table
    private String revision_time;       // Timestamp field of the other files table
    private String md5checksum;         // Md5Checksum field of the other files table
    private int status;                 // Status field of the other files table
    private int id_station;             // Id station field of the other files table
    //private String station_marker;      // Marker of the station

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * OTHER_FILES
    * This is the default constructor for the Other_files class.
    */
    public Other_files() {
        this.id = 0;
        this.name = "";
        this.data_center = new Data_center();
        this.file_size = 0;
        this.file_type = new File_type();
        this.relative_path = "";
        this.creation_date = "";
        this.revision_time = "";
        this.md5checksum = "";
        this.status = 0;
        this.id_station = 0;
       // this.station_marker = "";
    }

    /*
    * OTHER_FILES
    * This constructor for the Other_files class accepts values as parameters.
    */
    public Other_files(int id, String name, int file_size, String relative_path, 
            String creation_date, String revision_time, String md5checksum, int status, int id_station) {
        this.id = id;
        this.name = name;
        this.data_center = new Data_center();
        this.file_size = file_size;
        this.file_type = new File_type();
        this.relative_path = relative_path;
        this.creation_date = creation_date;
        this.revision_time = revision_time;
        this.md5checksum = md5checksum;
        this.status = status;
        this.id_station = id_station;
       // this.station_marker = "";
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET Name
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /*
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }

    /*
    * GET DATA CENTER
    * Returns the data center value.
    */
    public Data_center getData_center() {
        return data_center;
    }

    /*
    * SET DATA CENTER
    * Sets the value for the data center.
    */
    public void setData_center(int id, String data_center_accronym, String hostname, 
            String protocol) {
        this.data_center.setId(id);
        this.data_center.setProtocol(protocol);
        this.data_center.setHostname(hostname);
        this.data_center.setProtocol(protocol);
    }

    /*
    * GET FILE SIZE
    * Returns the file size value.
    */
    public int getFile_size() {
        return file_size;
    }

    /*
    * SET FILE SIZE
    * Sets the value for the file size.
    */
    public void setFile_size(int file_size) {
        this.file_size = file_size;
    }

    /*
    * GET FILE TYPE
    * Returns the file type value.
    */
    public File_type getFile_type() {
        return file_type;
    }

    /*
    * SET FILE TYPE
    * Sets the value for the file type.
    */
    public void setFile_type(int id, String format, String sampling_window,
            String sampling_frequency) {
        this.file_type.setId(id);
        this.file_type.setFormat(format);
        this.file_type.setSampling_window(sampling_window);
        this.file_type.setSampling_frequency(sampling_frequency);
    }

    /*
    * GET RELATIVE PATH
    * Returns the relative path value.
    */
    public String getRelative_path() {
        return relative_path;
    }

    /*
    * SET RELATIVE PATH
    * Sets the value for the relative path.
    */
    public void setRelative_path(String relative_path) {
        this.relative_path = relative_path;
    }

    /*
    * GET DATE
    * Returns the date value.
    */
    public String getCreation_date() {
        return creation_date;
    }

    /*
    * SET DATE
    * Sets the value for the date.
    */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /*
    * GET ID
    * Returns the id value.
    */
    public String getRevision_time() {
        return revision_time;
    }

    /*
    * SET TIMESTAMP
    * Sets the value for the timestamp.
    */
    public void setRevision_time(String revision_time) {
        this.revision_time = revision_time;
    }

    /*
    * GET MD5CHECKSUM
    * Returns the md5checksum value.
    */
    public String getMd5checksum() {
        return md5checksum;
    }

    /*
    * SET MD5CHECKSUM
    * Sets the value for the md5checksum.
    */
    public void setMd5checksum(String md5checksum) {
        this.md5checksum = md5checksum;
    }

    /*
    * GET STATUS
    * Returns the status value.
    */
    public int getStatus() {
        return status;
    }

    /*
    * SET STATUS
    * Sets the value for the status.
    */
    public void setStatus(int status) {
        this.status = status;
    }

    /*
    * GET ID STATION
    * Returns the id station value.
    */
    public int getId_station() {
        return id_station;
    }

    /*
    * SET ID STATION
    * Sets the value for the id station.
    */
    public void setId_station(int id_station) {
        this.id_station = id_station;
    }

    /*
    * GET STATION MARKER
    * Returns the station marker value.
    *
    public String getStation_marker() {
        return station_marker;
    }

    *
    * SET STATION MARKER
    * Sets the value for the station marker.
    *
    public void setStation_marker(String station_marker) {
        this.station_marker = station_marker;
    }

    */
}
