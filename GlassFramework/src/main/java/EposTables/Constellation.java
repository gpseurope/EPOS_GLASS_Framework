/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EposTables;

/**
 *
 * @author kngo
 */
public class Constellation extends EposTablesMasterClass {
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     
    private String identifier_1ch;                
    private String identifier_3ch;             
    private String name;      
    private boolean official;



    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * Constellation
    * This is the default constructor for the Constellation class.
    */
    public Constellation() {
        id = 0;
        identifier_1ch = "";                
        identifier_3ch = "";             
        name = "";      
        official = false;
    }

    /*
    * Constellation
    * This constructor for the Constellation class accepts values as parameters.
    */
    public Constellation(int id, String identifier_1ch, String identifier_3ch, String name, boolean official) 
    {
        this.id = id;
        this.identifier_1ch = identifier_1ch;                
        this.identifier_3ch = identifier_3ch;             
        this.name = name;      
        this.official = official;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET IDENTIFIER 1CH
    * Returns the identifier_1ch value.
    */
    public String getIdentifier1Ch() {
        return identifier_1ch;
    }

    /*
    * SET IDENTIFIER 1CH
    * Sets the value for the identifier_1ch.
    */
    public void setIdentifier1Ch(String identifier_1ch) {
        this.identifier_1ch = identifier_1ch;
    }

    /*
    * GET IDENTIFIER 3CH
    * Returns the identifier_3ch value.
    */
    public String getIdentifier3Ch() {
        return identifier_3ch;
    }

    /*
    * SET IDENTIFIER 3CH
    * Sets the value for the identifier_3ch.
    */
    public void setIdentifier3Ch(String identifier_3ch) {
        this.identifier_3ch = identifier_3ch;
    }
    
    /*
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /*
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }
    
    /*
    * GET OFFICIAL
    * Returns the official value.
    */
    public boolean getOfficial() {
        return official;
    }

    /*
    * SET OFFICIAL
    * Sets the value for the official.
    */
    public void setOfficial(boolean official) {
        this.official = official;
    }
}
