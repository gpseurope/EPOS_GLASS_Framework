/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EposTables;

/**
 *
 * @author kngo
 */
public class QC_Parameters extends EposTablesMasterClass  {
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     
    private String software_version;                
    private int elevation_cutoff;             
    private boolean gps;      
    private boolean glo;
    private boolean gal;              
    private boolean bds;        
    private boolean qzs;       
    private boolean sbs;      
    private boolean nav_gps;       
    private boolean nav_glo;
    private boolean nav_gal;              
    private boolean nav_bds;        
    private boolean nav_qzs;       
    private boolean nav_sbs;
    private String software_name;


    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * QC_Parameters
    * This is the default constructor for the QC_Parameters class.
    */
    public QC_Parameters() {
        id = 0;
        software_version = "";                
        elevation_cutoff = 0;             
        gps = false;      
        glo = false;
        gal = false;              
        bds = false;        
        qzs = false;       
        sbs = false;      
        nav_gps = false;       
        nav_glo = false;
        nav_gal = false;              
        nav_bds = false;        
        nav_qzs = false;       
        nav_sbs = false;
        software_name = "";
    }

    /*
    * QC_Report_Summary
    * This constructor for the QC_Report_Summary class accepts values as parameters.
    */
    public QC_Parameters(int id, String software_version, int elevation_cutoff, boolean gps, boolean glo, boolean gal, boolean bds, boolean qzs,       
                         boolean sbs, boolean nav_gps, boolean nav_glo, boolean nav_gal, boolean nav_bds, boolean nav_qzs, boolean nav_sbs, String software_name) 
    {
        this.id = id;
        this.software_version = software_version;                
        this.elevation_cutoff = elevation_cutoff;             
        this.gps = gps;      
        this.glo = glo;
        this.gal = gal;              
        this.bds = bds;        
        this.qzs = qzs;       
        this.sbs = sbs;      
        this.nav_gps = nav_gps;       
        this.nav_glo = nav_glo;
        this.nav_gal = nav_gal;              
        this.nav_bds = nav_bds;        
        this.nav_qzs = nav_qzs;       
        this.nav_sbs = nav_sbs;
        this.software_name = software_name;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET SOFTWARE VERSION
    * Returns the software_version value.
    */
    public String getSoftwareVersion() {
        return software_version;
    }

    /*
    * SET SOFTWARE VERSION
    * Sets the value for the software_version.
    */
    public void setSoftwareVersion(String software_version) {
        this.software_version = software_version;
    }

    /*
    * GET elevation cutoff
    * Returns the elevation_cutoff value.
    */
    public int getElevationCutoff() {
        return elevation_cutoff;
    }

    /*
    * SET elevation cutoff
    * Sets the value for the elevation_cutoff.
    */
    public void setElevationCutoff(int elevation_cutoff) {
        this.elevation_cutoff = elevation_cutoff;
    }
    
    /*
    * GET GPS
    * Returns the gps value.
    */
    public boolean getGPS() {
        return gps;
    }

    /*
    * SET GPS
    * Sets the value for the gps.
    */
    public void setGPS(boolean gps) {
        this.gps = gps;
    }
    
    /*
    * GET GLO
    * Returns the glo value.
    */
    public boolean getGLO() {
        return glo;
    }

    /*
    * SET GLO
    * Sets the value for the glo.
    */
    public void setGLO(boolean glo) {
        this.glo = glo;
    }
    /*
    * GET GAL
    * Returns the gal value.
    */
    public boolean getGAL() {
        return gal;
    }

    /*
    * SET GAL
    * Sets the value for the gal.
    */
    public void setGAL(boolean gal) {
        this.gal = gal;
    }
    /*
    * GET BDS
    * Returns the bds value.
    */
    public boolean getBDS() {
        return bds;
    }

    /*
    * SET BDS
    * Sets the value for the bds.
    */
    public void setBDS(boolean bds) {
        this.bds = bds;
    }
    /*
    * GET QZS
    * Returns the qzs value.
    */
    public boolean getQZS() {
        return qzs;
    }

    /*
    * SET QZS
    * Sets the value for the qzs.
    */
    public void setQZS(boolean qzs) {
        this.qzs = qzs;
    }
    /*
    * GET SBS
    * Returns the sbs value.
    */
    public boolean getSBS() {
        return sbs;
    }

    /*
    * SET SBS
    * Sets the value for the sbs.
    */
    public void setSBS(boolean sbs) {
        this.sbs = sbs;
    }
    /*
    * GET NAV GPS
    * Returns the nav gps value.
    */
    public boolean getNavGPS() {
        return nav_gps;
    }

    /*
    * SET NAV GPS
    * Sets the value for the nav gps.
    */
    public void setNavGPS(boolean nav_gps) {
        this.nav_gps = nav_gps;
    }
    
    /*
    * GET NAV GLO
    * Returns the nav glo value.
    */
    public boolean getNavGLO() {
        return nav_glo;
    }

    /*
    * SET NAV GLO
    * Sets the value for the nav glo.
    */
    public void setNavGLO(boolean nav_glo) {
        this.nav_glo = nav_glo;
    }
    /*
    * GET NAV GAL
    * Returns the nav gal value.
    */
    public boolean getNavGAL() {
        return nav_gal;
    }

    /*
    * SET NAV GAL
    * Sets the value for the nav gal.
    */
    public void setNavGAL(boolean nav_gal) {
        this.nav_gal = nav_gal;
    }
    /*
    * GET NAV BDS
    * Returns the nav bds value.
    */
    public boolean getNavBDS() {
        return nav_bds;
    }

    /*
    * SET NAV BDS
    * Sets the value for the nav bds.
    */
    public void setNavBDS(boolean nav_bds) {
        this.nav_bds = nav_bds;
    }
    /*
    * GET NAV QZS
    * Returns the nav qzs value.
    */
    public boolean getNavQZS() {
        return nav_qzs;
    }

    /*
    * SET NAV QZS
    * Sets the value for the nav qzs.
    */
    public void setNavQZS(boolean nav_qzs) {
        this.nav_qzs = nav_qzs;
    }
    /*
    * GET NAV SBS
    * Returns the nav sbs value.
    */
    public boolean getNavSBS() {
        return nav_sbs;
    }

    /*
    * SET NAV SBS
    * Sets the value for the nav sbs.
    */
    public void setNavSBS(boolean nav_sbs) {
        this.nav_sbs = nav_sbs;
    }
    
    /*
    * GET SOFTWARE NAME
    * Returns the software_name value.
    */
    public String getSoftwareName() {
        return software_name;
    }

    /*
    * SET SOFTWARE Name
    * Sets the value for the software_name.
    */
    public void setSoftwareName(String software_name) {
        this.software_name = software_name;
    }
}
