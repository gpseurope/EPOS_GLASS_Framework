package EposTables;

/*
* CONNECTIONS
* This class represents the connections table of the EPOS database.
*/
public class Connections extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the connections table
    private Node source;                // Source node of the connection
    private Node destiny;               // Destiny node of the connection
    private Station station;            // Station related to the queries
    private String metadata;            // Type of metadata (T1 or T2)

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * CONNECTIONS
    * This is the default constructor for the Connections class.
    */
    public Connections() {
        this.source = new Node();
        this.destiny = new Node();
        this.station = new Station();
    }

    /*
    * CONNECTIONS
    * This constructor for the Connections class accepts values as parameters.
    */
    public Connections(int id, Node source, Node destiny, Station station, 
            String metadata) {
        this.id = id;
        this.source = source;
        this.destiny = destiny;
        this.station = station;
        this.metadata = metadata;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET SOURCE
    * Returns the source node.
    */
    public Node getSource() {
        return source;
    }

    /*
    * SET SOURCE
    * Sets the value for the source node.
    */
    public void setSource(Node source) {
        this.source.setId(source.getId());
        this.source.setName(source.getName());
        this.source.setHost(source.getHost());
        this.source.setPort(source.getPort());
        this.source.setDbname(source.getDbname());
        this.source.setUsername(source.getUsername());
        this.source.setPassword(source.getPassword());
        this.source.setUrl(source.getUrl());
        this.source.setEmail(source.getEmail());
        this.source.setContact_name(source.getContact_name());
        if(source.getData_centers() != null)
            this.source.setData_centers(source.getData_centers());

    }

    /*
    * GET DESTINY
    * Returns the destiny node.
    */
    public Node getDestiny() {
        return destiny;
    }

    /*
    * SET DESTINY
    * Sets the value for the destiny node.
    */
    public void setDestiny(Node destiny) {
        this.destiny.setId(destiny.getId());
        this.destiny.setName(destiny.getName());
        this.destiny.setHost(destiny.getHost());
        this.destiny.setPort(destiny.getPort());
        this.destiny.setDbname(destiny.getDbname());
        this.destiny.setUsername(destiny.getUsername());
        this.destiny.setPassword(destiny.getPassword());
        this.destiny.setContact_name(destiny.getContact_name());
        this.destiny.setEmail(destiny.getEmail());
        this.destiny.setUrl(destiny.getUrl());
        if(destiny.getData_centers() != null)
            this.destiny.setData_centers(destiny.getData_centers());
    }

    /*
    * GET STATION
    * Returns the station.
    */
    public Station getStation() {
        return station;
    }

    /*
    * SET STATION
    * Sets the value for the station.
    */
    public void setStation(Station station) {
        this.station.setId(station.getId());
        this.station.setName(station.getName());
        this.station.setMarker(station.getMarker());
        this.station.setDescription(station.getDescription());
        this.station.setDate_from(station.getDate_from());
        this.station.setDate_to(station.getDate_to());
        this.station.setComment(station.getComment());
        this.station.setIers_domes(station.getIers_domes());
        this.station.setCpd_num(station.getCpd_num());
        this.station.setMonument_num(station.getMonument_num());
        this.station.setReceiver_num(station.getReceiver_num());
        this.station.setCountry_code(station.getCountry_code());
    }
    public void setStation1(Station station){
        this.station.setId(station.getId());
        this.station.setName(station.getName());
    }

    /*
    * GET METADATA
    * Returns the metadata value.
    */
    public String getMetadata() {
        return metadata;
    }

    /*
    * SET METADATA
    * Sets the value for the metadata.
    */
    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }
    
    
}
