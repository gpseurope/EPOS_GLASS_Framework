package EposTables;

/*
* RINEX ERRORS FILES
* This class represents the rinex_errors table of the EPOS database.
*/
public class Rinex_errors extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int  id;                // Id field of the rinex_errors table
    private int id_rinex_file;      // Rinex file field of the rinex_errors table
    private int id_error_type;      // Error type file field of the rinex_errors table

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * RINEX_ERRORS
    * This is the default constructor for the Rinex_errors class.
    */
    public Rinex_errors() {
        this.id = 0;
        this.id_rinex_file = 0;
        this.id_error_type = 0;
    }

    /*
    * RINEX_ERRORS
    * This constructor for the Rinex_errors class accepts values as parameters.
    */
    public Rinex_errors(int id, int id_rinex_file, int id_error_type) {
        this.id = id;
        this.id_rinex_file = id_rinex_file;
        this.id_error_type = id_error_type;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET ID RINEX FILE
    * Returns the id rinex file value.
    */
    public int getId_rinex_file() {
        return id_rinex_file;
    }

    /*
    * SET ID RINEX FILE
    * Sets the value for the id rinex file.
    */
    public void setId_rinex_file(int id_rinex_file) {
        this.id_rinex_file = id_rinex_file;
    }

    /*
    * GET ID ERROR TYPE
    * Returns the id error type value.
    */
    public int getId_error_type() {
        return id_error_type;
    }

    /*
    * SET ID ERROR TYPE
    * Sets the value for the id error type.
    */
    public void setId_error_type(int id_error_type) {
        this.id_error_type = id_error_type;
    }
}
