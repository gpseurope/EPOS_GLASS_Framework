package EposTables;

/*
* QUALITY FILE
* This class represents the quality_file table of the EPOS database.
*/
public class Quality_file extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                         // Id field of the quality file table
    private String name;                    // Name field of the quality file table
    private int id_rinexfile;          // Rinex file field of the quality file table
    private Data_center_structure data_center_structure;    // Data center_structure field of the quality  file table
    private int file_size;                  // File size field of the quality file table
    private File_type file_type;            // File type field of the quality file table
    private String relative_path;           // Relative path field of the quality file table
    private String creation_date;           // Creation date field of the quality file table
    private String revision_time;           // Revision time field of the quality file table
    private String md5checksum;             // md5checksum field of the quality file table
    private int status;                     // Status field of the quality file table
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * QUALITY FILE
    * This is the default constructor for the Quality_file class.
    */
    public Quality_file() {
        id = 0;
        name = "";
        id_rinexfile = 0;
        data_center_structure = new Data_center_structure();
        file_size = 0;
        file_type = new File_type();
        relative_path = "";
        creation_date = "";
        revision_time = "";
        md5checksum = "";
        status = 0;
    }

    /*
    * QUALITY_FILE
    * This constructor for the Quality_file class accepts values as parameters.
    */
    public Quality_file(int id, String name, int id_rinexfile, int file_size, String relative_path, 
            String creation_date, String revision_time, String md5checksum, int status) {
        this.id = id;
        this.name = name;
        this.id_rinexfile = id_rinexfile;
        this.data_center_structure = new Data_center_structure();
        this.file_size = file_size;
        this.file_type = new File_type();
        this.relative_path = relative_path;
        this.creation_date = creation_date;
        this.revision_time = revision_time;
        this.md5checksum = md5checksum;
        this.status = status;
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /*
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }

    /*
    * GET ID RINEX FILE
    * Returns the id rinex file value.
    */
    public int getRinex_file() {
        return id_rinexfile;
    }

    /*
    * SET ID RINEX FILE
    * Sets the value for the id rinex.
    */
    public void setRinex_file(int id_rinexfile) {
        this.id_rinexfile =id_rinexfile;
        
    }

    /*
    * GET DATA CENTER STRUCTURE
    * Returns the data center structure value.
    */
    public Data_center_structure getData_center_structure() {
        return data_center_structure;
    }

    /*
    * SET DATA CENTER STRUCTURE
    * Sets the value for the data center structure.
    */
    public void setData_center_structure(int id, String directory_naming) {
        this.data_center_structure.setId(id);
        this.data_center_structure.setDirectory_naming(directory_naming);
    }
    
    /*
    * SET DATA CENTER STRUCTURE
    * Sets the value for the data center structure.
    */
    public void setData_center_structure(int id, String directory_naming, Data_center data_center) {
        this.data_center_structure.setId(id);
        this.data_center_structure.setDirectory_naming(directory_naming);
        this.data_center_structure.setDataCenter(data_center);
    }

    /*
    * GET FILE SIZE
    * Returns the file size value.
    */
    public int getFile_size() {
        return file_size;
    }

    /*
    * SET FILE SIZE
    * Sets the value for the file size.
    */
    public void setFile_size(int file_size) {
        this.file_size = file_size;
    }

    /*
    * GET FILE TYPE
    * Returns the file type value.
    */
    public File_type getFile_type() {
        return file_type;
    }

    /*
    * SET FILE TYPE
    * Sets the value for the file type.
    */
    public void setFile_type(int id, String format, String samplig_window,
            String sampling_frequency) {
        this.file_type.setId(id);
        this.file_type.setFormat(format);
        this.file_type.setSampling_window(samplig_window);
        this.file_type.setSampling_frequency(sampling_frequency);
    }

    /*
    * GET RELATIVE PATH
    * Returns the relative path value.
    */
    public String getRelative_path() {
        return relative_path;
    }

    /*
    * SET RELATIVE PATH
    * Sets the value for the relative path.
    */
    public void setRelative_path(String relative_path) {
        this.relative_path = relative_path;
    }

    /*
    * GET CREATION DATE
    * Returns the creation date value.
    */
    public String getCreation_date() {
        return creation_date;
    }

    /*
    * SET CREATION DATE
    * Sets the value for the creation date.
    */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /*
    * GET REVISION TIME
    * Returns the revision time value.
    */
    public String getRevision_time() {
        return revision_time;
    }

    /*
    * SET REVISION TIME
    * Sets the value for the revision time.
    */
    public void setRevision_time(String revision_time) {
        this.revision_time = revision_time;
    }

    /*
    * GET MD5CHECKSUM
    * Returns the md5 checksum value.
    */
    public String getMd5checksum() {
        return md5checksum;
    }

    /*
    * SET MD5CHECKSUM
    * Sets the value for the md5 checksum.
    */
    public void setMd5checksum(String md5checksum) {
        this.md5checksum = md5checksum;
    }

    /*
    * GET STATUS
    * Returns the status value.
    */
    public int getStatus() {
        return status;
    }

    /*
    * SET STATUS
    * Sets the value for the status.
    */
    public void setStatus(int status) {
        this.status = status;
    }
}
