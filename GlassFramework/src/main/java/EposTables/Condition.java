package EposTables;

/**
* CONDITION
* This class represents the condition table of the EPOS database.
*/
public class Condition extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the condition table
    private int id_station;             // Id_station field of the condition table
    private Effects effect;             // Effect field of the condition table
    private String date_from;           // Date_from field of the condition table
    private String date_to;             // Date_to field of the condition table
    private String degradation;         // Degradation field of the condition table
    private String comments;            // Comments field of the condition table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * CONDITION
    * This is the default constructor for the Condition class.
    */
    public Condition() {
        id = 0;
        id_station = 0;
        effect = new Effects();
        date_from = "";
        date_to = "";
        degradation = "";
        comments = "";
    }

    /**
    * CONDITION
    * This constructor for the Condition class accepts values as parameters.
    */
    public Condition(int id, int id_station, Effects effect, String date_from, String date_to,
            String degradation, String comments) {
        this.id = id;
        this.id_station = id_station;
        this.effect = effect;
        this.date_from = date_from;
        this.date_to = date_to;
        this.degradation = degradation;
        this.comments = comments;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET ID_STATION
    * Returns the id_station value.
    */
    public int getId_station() {
        return id_station;
    }

    /**
    * SET ID_STATION
    * Sets the value for the id_station.
    */
    public void setId_station(int id_station) {
        this.id_station = id_station;
    }

    /**
    * GET EFFECT
    * Returns the effect object.
    */
    public Effects getEffect() {
        return effect;
    }

    /**
    * SET EFFECT
    * Sets the value for the effect.
    */
    public void setEffect(int id_effect, String type) {
        this.effect.setId(id_effect);
        this.effect.setType(type);
    }

    /**
    * GET DATE_FROM
    * Returns the date_from value.
    */
    public String getDate_from() {
        return date_from;
    }

    /**
    * SET DATE_FROM
    * Sets the value for the date_from.
    */
    public void setDate_from(String date_from) {
        this.date_from = date_from;
    }

    /**
    * GET DATE_TO
    * Returns the date_to value.
    */
    public String getDate_to() {
        return date_to;
    }

    /**
    * SET DATE_TO
    * Sets the value for the date_to.
    */
    public void setDate_to(String date_to) {
        this.date_to = date_to;
    }

    /**
    * GET DEGRADATION
    * Returns the degradation value.
    */
    public String getDegradation() {
        return degradation;
    }

    /**
    * SET DEGRADATION
    * Sets the value for the degradation.
    */
    public void setDegradation(String degradation) {
        this.degradation = degradation;
    }

    /**
    * GET COMMENTS
    * Returns the comments value.
    */
    public String getComments() {
        return comments;
    }

    /**
    * SET COMMENTS
    * Sets the value for the comments.
    */
    public void setComments(String comments) {
        this.comments = comments;
    } 
}
