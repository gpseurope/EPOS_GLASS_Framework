package EposTables;

/*
* JUMP METHOD IDENTIFICATION
* This class represents the Jump Method Identification table of the EPOS product database.
*/
public class Jump_method_identification extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                         // Id field of the jump method indentification table
    private Agency agency;                  // Agency field of the jump method indentification table
    private Software software;              // Software field of the jump method indentification table
    private String processing_scheme;       // Prcoessing Scheme field of the jump method indentification table
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * JUMP METHOD IDENTIFICATION
    * This is the default constructor for the Jump Method Identification class.
    */
    public Jump_method_identification() {
        this.id = 0;
        this.agency = new Agency();
        this.software = new Software();
        this.processing_scheme = "";
    }

    /*
    * JUMP METHOD IDENTIFICATION
    * This constructor for the Jump Method Identification class accepts values as parameters.
    */
    public Jump_method_identification(int id, String processing_scheme) {
        this.id = id;
        this.agency = new Agency();
        this.software = new Software();
        this.processing_scheme = processing_scheme;
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET AGENCY
    * Returns the agency value.
    */
    public Agency getAgency() {
        return agency;
    }

    /*
    * SET AGENCY
    * Sets the value for the agency.
    */
    public void setAgency(int id, String name, String abbreviation, String address, 
            String www, String infos) {
        this.agency.setId(id);
        this.agency.setName(name);
        this.agency.setAbbreviation(abbreviation);
        this.agency.setAddress(address);
        this.agency.setWww(www);
        this.agency.setInfos(infos);
    }

    /*
    * GET SOFTWARE
    * Returns the software value.
    */
    public Software getSoftware() {
        return software;
    }

    /*
    * SET SOFTWARE
    * Sets the value for the software.
    */
    public void setSoftware(int id, String name) {
        this.software.setId(id);
        this.software.setName(name);
    }

    /*
    * GET PROCESSING SCHEME
    * Returns the processing scheme value.
    */
    public String getProcessing_scheme() {
        return processing_scheme;
    }

    /*
    * SET PROCESSING SCHEME
    * Sets the value for the processing scheme.
    */
    public void setProcessing_scheme(String processing_scheme) {
        this.processing_scheme = processing_scheme;
    }
    
}
