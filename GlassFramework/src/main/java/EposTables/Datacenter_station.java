package EposTables;

/*
* DATACENTER STATION
* This class represents the datacenter station table of the EPOS database.
*/
public class Datacenter_station extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the datacenter station table
    private Station station;            // Station field of the datacenter station table
    private Data_center datacenter;     // Datacenter field of the datacenter station table
    private String datacenter_type;     // Datacenter type field of the datacenter station table
    private String markerLongName;

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * DATACENTER STATION
    * This is the default constructor for the Datacenter station class.
    */
    public Datacenter_station() {
        this.id = 0;
        this.station = new Station();
        this.datacenter = new Data_center();
        this.datacenter_type = "";
    }

    /*
    * DATACENTER STATION
    * This constructor for the Datacenter station class accepts values as parameters.
    */
    public Datacenter_station(int id, String datacenter_type) {
        this.id = id;
        this.station = new Station();
        this.datacenter = new Data_center();
        this.datacenter_type = datacenter_type;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET STATION
    * Returns the station value.
    */
    public Station getStation() {
        return station;
    }

    /*
    * SET STATION
    * Sets the value for the station.
    */
    /*public void setStation(int id, String name, String marker, String description,
            String date_from, String date_to, String comment, String iers_domes, 
            String cpd_num, int monument_num, int receiver_num, String country_code) {
        this.station.setId(id);
        this.station.setName(name);
        this.station.setMarker(marker);
        this.station.setDescription(description);
        this.station.setDate_from(date_from);
        this.station.setDate_to(date_to);
        this.station.setComment(comment);
        this.station.setIers_domes(iers_domes);
        this.station.setCpd_num(cpd_num);
        this.station.setMonument_num(monument_num);
        this.station.setReceiver_num(receiver_num);
        this.station.setCountry_code(country_code);
    }*/

    public void setStation(Station station) {
        this.station = station;
    }



    /*
    * GET DATACENTER
    * Returns the datacenter value.
    */
    public Data_center getDatacenter() {
        return datacenter;
    }

    /*
    * SET DATACENTER
    * Sets the value for the datacenter.
    */
    /*public void setDatacenter(int id, String acronym, String hostname,
            String protocol) {
        this.datacenter.setId(id);
        this.datacenter.setAcronym(acronym);
        this.datacenter.setHostname(hostname);
        this.datacenter.setProtocol(protocol);
    }*/
    public void setDatacenter(Data_center datacenter) {
        this.datacenter = datacenter;
    }






    /*
    * GET DATACENTER TYPE
    * Returns the data center value.
    */
    public String getDatacenter_type() {
        return datacenter_type;
    }

    /*
    * SET DATACENTER TYPE
    * Sets the value for the datacenter type.
    */
    public void setDatacenter_type(String datacenter_type) {
        this.datacenter_type = datacenter_type;
    }

    public String getMarkerLongName() {
        return markerLongName;
    }

    public void setMarkerLongName(String markerLongName) {
        this.markerLongName = markerLongName;
    }
}
