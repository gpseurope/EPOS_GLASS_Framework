package EposTables;

/*
*****************************************************
**                                                 **
**      Created by Andre Rodrigues on 10/17/17     **
**                                                 **
**      andre.rodrigues@ubi.pt                     **
**                                                 **
**      Covilha, Portugal                          **
**                                                 **
*****************************************************
*/

public class Analysis_Centers extends EposTablesMasterClass  {

    int id;
    String abbreviation;
    String name;
    String contact;
    String url;
    String email;


    public Analysis_Centers() {
        this.id = id;
        this.abbreviation = abbreviation;
        this.name = name;
        this.contact = contact;
        this.url = url;
        this.email = email;
    }

    public Analysis_Centers(int id, String abbreviation, String name, String contact, String url, String email) {
        this.id = id;
        this.abbreviation = abbreviation;
        this.name = name;
        this.contact = contact;
        this.url = url;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
