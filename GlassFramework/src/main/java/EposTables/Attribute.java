package EposTables;

/*
* ATTRIBUTE
* This class represents the attribute table of the EPOS database.
*/
public class Attribute extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                 // Id field of the attribute table
    private String name;            // Name filed of the attribute table
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * ATTRIBUTE
    * This is the default constructor for the Attribute class.
    */
    public Attribute() {
        id = 0;
        name = "";
    }
    
    /*
    * ATTRIBUTE
    * This constructor for the Attribute class accepts values as parameters.
    */
    public Attribute(int id, String name) {
        this.id = id;
        this.name = name;
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }
    
    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }
    
    /*
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }
    
    /*
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }
}