package EposTables;

/**
* LOCATION
* This class represents the location table of the EPOS database.
*/
public class Location extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the location table
    private City city;                  // City field of the location table
    private Coordinates coordinates;    // Coordinates field of the location table
    private Tectonic tectonic;          // Tectonic field of the location table
    private String description;         // Description field of the location table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * LOCATION
    * This is the default constructor for the Location class.
    */
    public Location() {
        id = 0;
        city = new City();
        coordinates = new Coordinates();
        tectonic = new Tectonic();
        description = "";
    }

    /**
    * LOCATION
    * This constructor for the Location class accepts values as parameters.
    */
    public Location(int id, City city, Coordinates coordinates, Tectonic tectonic, String description) {
        this.id = id;
        this.city = city;
        this.coordinates = coordinates;
        this.tectonic = tectonic;
        this.description = description;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET CITY
    * Returns the city object.
    */
    public City getCity() {
        return city;
    }

    /**
    * SET CITY
    * Sets the values for the city.
    */
    public void setCity(int id_city, State state, String name) {
        this.city.setId(id_city);
        this.city.setState(state.getId(), state.getCountry(),state.getName());
        this.city.setName(name);
    }

    /**
    * GET COORDINATES
    * Returns the coordinates object.
    */
    public Coordinates getCoordinates() {
        return coordinates;
    }

    /**
    * SET COORDINATES
    * Sets the value for the id_coordinates.
    */
    public void setCoordinates(int id_coordinates, double x, double y, double z, 
            double lat, double lon, double altitude) {
        this.coordinates.setId(id_coordinates);
        this.coordinates.setX(x);
        this.coordinates.setY(y);
        this.coordinates.setZ(z);
        this.coordinates.setLat(lat);
        this.coordinates.setLon(lon);
        this.coordinates.setAltitude(altitude);
    }

    /**
    * GET TECTONIC
    * Returns the tectonic object.
    */
    public Tectonic getTectonic() {
        return tectonic;
    }

    /**
    * SET TECTONIC
    * Sets the values for the tectonic.
    */
    public void setTectonic(int id_tectonic, String plate_name) {
        this.tectonic.setId(id_tectonic);
        this.tectonic.setPlate_name(plate_name);
    }

    /**
    * GET DESCRIPTION
    * Returns the description value.
    */
    public String getDescription() {
        return description;
    }

    /**
    * SET DESCRIPTION
    * Sets the value for the description.
    */
    public void setDescription(String description) {
        this.description = description;
    }
}
