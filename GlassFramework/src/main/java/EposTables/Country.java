package EposTables;

/**
* COUNTRY
* This class represents the country table of the EPOS database.
*/
public class Country extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the country table
    private String name;                // Name field of the country table
    private String iso_code;            // ISo_code field of the country table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * COUNTRY
    * This is the default constructor for the Country class.
    */
    public Country() {
        id = 0;
        name = "";
        iso_code = "";
    }

    /**
    * COUNTRY
    * This constructor for the Country class accepts values as parameters.
     * @param id ID
     * @param name Name
     * @param iso_code ISO Code
    */
    public Country(int id, String name, String iso_code) {
        this.id = id;
        this.name = name;
        this.iso_code = iso_code;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /**
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }

    /**
    * GET ISO_CODE
    * Returns the iso_code value.
    */
    public String getIso_code() {
        return iso_code;
    }

    /**
    * SET ISO_CODE
    * Sets the value for the iso_code.
    */
    public void setIso_code(String iso_code) {
        this.iso_code = iso_code;
    }
}
