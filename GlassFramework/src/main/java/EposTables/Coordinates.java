package EposTables;

/**
* COORDINATES
* This class represents the coordinates table of the EPOS database.
*/
public class Coordinates extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the coordinates table
    private double x;                   // X field of the coordinates table
    private double y;                   // Y field of the coordinates table
    private double z;                    // Z field of the coordinates table
    private double lat;                  // Latitude field of the coordinates table
    private double lon;                  // Longitude field of the coordinates table
    private double altitude;             // Altitude field of the coordinates table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * COORDINATES
    * This is the default constructor for the Coordinates class.
    */
    public Coordinates() {
        id = 0;
        x = 0;
        y = 0;
        z = 0;
        lat = 0;
        lon = 0;
        altitude = 0;
    }

    /**
    * COORDINATES
    * This constructor for the Coordinates class accepts values as parameters.
    */
    public Coordinates(int id, double x, double y, double z, double lat, double lon, double altitude) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.z = z;
        this.lat = lat;
        this.lon = lon;
        this.altitude = altitude;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET X
    * Returns the x value.
    */
    public double getX() {
        return x;
    }

    /**
    * SET X
    * Sets the value for the x.
    */
    public void setX(double x) {
        this.x = x;
    }

    /**
    * GET Y
    * Returns the y value.
    */
    public double getY() {
        return y;
    }

    /**
    * SET Y
    * Sets the value for the y.
    */
    public void setY(double y) {
        this.y = y;
    }

    /**
    * GET Z
    * Returns the z value.
    */
    public double getZ() {
        return z;
    }

    /**
    * SET Z
    * Sets the value for the z.
    */
    public void setZ(double z) {
        this.z = z;
    }

    /**
    * GET LAT
    * Returns the lat value.
    */
    public double getLat() {
        return lat;
    }

    
    /**
    * SET LAT
    * Sets the value for the lat.
    */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
    * GET LON
    * Returns the lon value.
    */
    public double getLon() {
        return lon;
    }

    /**
    * SET LON
    * Sets the value for the lon.
    */
    public void setLon(double lon) {
        this.lon = lon;
    }

    /**
    * GET ALTITUDE
    * Returns the altitude value.
    */
    public double getAltitude() {
        return altitude;
    }

    /**
    * SET ALTITUDE
    * Sets the value for the altitude.
    */
    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }
}
