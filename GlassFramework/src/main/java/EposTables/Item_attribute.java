package EposTables;

import java.util.ArrayList;

/**
* ITEM_ATTRIBUTE
* This class represents the item_attribute table of the EPOS database.
*/
public class Item_attribute extends EposTablesMasterClass {

    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                                         // Id field of the item_attribute table
    private int id_item;                                    // Id_item field of the item_attribute table
    private Attribute attribute;                            // Attribute field of the item_attribute table
    private String date_from;                               // Date_from field of the item_attribute table
    private String date_to;                                 // Date_to field of the item_attribute table
    private String value_varchar;                           // Value_varchar field of the item_attribute table
    private String value_date;                              // Value_date field of the item_attribute table
    private float value_numeric;                            // Value_numeric field of the item_attribute table
    private ArrayList<Filter_receiver> filter_receivers;    // List of filter receivers
    private ArrayList<Filter_antenna> filter_antennas;      // List of filter antennas
    private ArrayList<Filter_radome> filter_radomes;         // List of filter radomes

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * ITEM_ATTRIBUTE
    * This is the default constructor for the Item_attribute class.
    */
    public Item_attribute() {
        id = 0;
        id_item = 0;
        attribute = new Attribute();
        date_from = "";
        date_to = "";
        value_varchar = "";
        value_date = "";
        value_numeric = 0;
        filter_receivers = new ArrayList();
        filter_antennas = new ArrayList();
        filter_radomes = new ArrayList();
    }

    /**
    * ITEM_ATTRIBUTE
    * This constructor for the Item_attribute class accepts values as parameters.
    */
    public Item_attribute(int id, int id_item, Attribute attribute, String date_from, String date_to, String value_varchar, String value_date, float value_numeric) {
        this.id = id;
        this.id_item = id_item;
        this.attribute = attribute;
        this.date_from = date_from;
        this.date_to = date_to;
        this.value_varchar = value_varchar;
        this.value_date = value_date;
        this.value_numeric = value_numeric;
        this.filter_receivers = new ArrayList();
        this.filter_antennas = new ArrayList();
        this.filter_radomes = new ArrayList();
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================

    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the id value.
    */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
    * GET ID_ITEM
    * Returns the id_item value.
    */
    public int getId_item() {
        return id_item;
    }

    /**
    * SET ID_ITEM
    * Sets the value for the id_item.
    */
    public void setId_item(int id_item) {
        this.id_item = id_item;
    }

    /**
    * GET ATTRIBUTE
    * Returns the attribute value.
    */
    public Attribute getAttribute() {
        return attribute;
    }

    /**
    * SET ATRIBUTE
    * Sets the value for the attribute.
    */
    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    /**
    * GET DATE_FROM
    * Returns the date_from value.
    */
    public String getDate_from() {
        return date_from;
    }

    /**
    * SET DATE_FROM
    * Sets the value for the date_from.
    */
    public void setDate_from(String date_from) {
        this.date_from = date_from;
    }

    /**
    * GET DATE_TO
    * Returns the date_to value.
    */
    public String getDate_to() {
        return date_to;
    }

    /**
    * SET DATE_TO
    * Sets the value for the date_to.
    */
    public void setDate_to(String date_to) {
        this.date_to = date_to;
    }

    /**
    * GET VALUE_VARCHAR
    * Returns the value_varchar value.
    */
    public String getValue_varchar() {
        return value_varchar;
    }

    /**
    * SET VALUE_VARCHAR
    * Sets the value for the vaue_varchar.
    */
    public void setValue_varchar(String value_varchar) {
        this.value_varchar = value_varchar;
    }

    /**
    * GET VALUE_DATE
    * Returns the value_date value.
    */
    public String getValue_date() {
        return value_date;
    }

    /**
    * SET VALUE_DATE
    * Sets the value for the value_date.
    */
    public void setValue_date(String value_date) {
        this.value_date = value_date;
    }

    /**
    * GET VALUE_NUMERIC
    * Returns the value_numeric value.
    */
    public float getValue_numeric() {
        return value_numeric;
    }

    /**
    * SET VALUE_NUMERIC
    * Sets the value for the value_numeric.
    */
    public void setValue_numeric(float value_numeric) {
        this.value_numeric = value_numeric;
    }
    
    /**
    * GET FILTER_RECEIVERS
    * Returns the list of filter_receivers.
    */
    public ArrayList<Filter_receiver> getFilterReceivers() {
        return filter_receivers;
    }
    
    /**
    * ADD FILTER_RECEIVER
    * Add a filter_receiver to the list.
    */
    public void addFilterReceiver(Filter_receiver filter_receiver) {
        this.filter_receivers.add(filter_receiver);
    }
    
    /**
    * GET FILTER_ANTENNAS
    * Returns the list of filter_antennas.
    */
    public ArrayList<Filter_antenna> getFilterAntennas() {
        return filter_antennas;
    }
    
    /**
    * ADD FILTER_ANTENNA
    * Add a filter_antenna to the list.
    */
    public void addFilterAntenna(Filter_antenna filter_antenna) {
        this.filter_antennas.add(filter_antenna);
    }
    
    /**
    * GET FILTER_RADOMES
    * Returns the list of filter_radomes.
    */
    public ArrayList<Filter_radome> getFilterRadomes() {
        return filter_radomes;
    }
    
    /**
    * ADD FILTER_RADOME
    * Add a filter_radome to the list.
    */
    public void addFilterRadome(Filter_radome filter_radome) {
        this.filter_radomes.add(filter_radome);
    }
}
