package EposTables;

/**
* USER_GROUP
* This class represents the user_group table of the EPOS database.
*/
public class User_group extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the user_group table
    private String name;                // Name field of the user_group table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * USER_GROUP
    * This is the default constructor for the User_group class.
    */
    public User_group() {
        id = 0;
        name = "";
    }

    /**
    * USER_GROUP
    * This constructor for the User_group class accepts values as parameters.
    */
    public User_group(int id, String name) {
        this.id = id;
        this.name = name;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /**
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }
}
