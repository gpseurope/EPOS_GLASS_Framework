package EposTables;

import java.util.ArrayList;

/*
* NODE
* This class represents the node table of the EPOS database.
*/
public class Node extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                 // Id field of the node table
    private String name;            // Name of the node
    private String host;            // IP address of the databse host
    private String port;            // Port used by postgreSQL
    private String dbname;          // Name of the database
    private String username;        // Username of the database
    private String password;        // Password of the username
    private String contact_name;
    private String url;
    private String email;
    private ArrayList<Data_center> data_centers;

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * NODE
    * This is the default constructor for the Node class.
    */
    public Node() {
        this.id = 0;
    }

    /*
    * NODE
    * This constructor for the Node class accepts values as parameters.
    */
    public Node (int id, String name, String host, String port, String dbname,
            String username, String password)
    {
        this.id = id;
        this.name = name;
        this.host = host;
        this.port = port;
        this.dbname = dbname;
        this.username = username;
        this.password = password;

    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /*
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }

    /*
    * GET HOST
    * Returns the host value.
    */
    public String getHost() {
        return host;
    }

    /*
    * SET HOST
    * Sets the value for the host.
    */
    public void setHost(String host) {
        this.host = host;
    }

    /*
    * GET PORT
    * Returns the port value.
    */
    public String getPort() {
        return port;
    }

    /*
    * SET PORT
    * Sets the value for the port.
    */
    public void setPort(String port) {
        this.port = port;
    }

    /*
    * GET DBNAME
    * Returns the dbname value.
    */
    public String getDbname() {
        return dbname;
    }

    /*
    * SET DBNAME
    * Sets the value for the dbname.
    */
    public void setDbname(String dbname) {
        this.dbname = dbname;
    }

    /*
    * GET USERNAME
    * Returns the username value.
    */
    public String getUsername() {
        return username;
    }

    /*
    * SET USERNAME
    * Sets the value for the username.
    */
    public void setUsername(String username) {
        this.username = username;
    }

    /*
    * GET PASSWORD
    * Returns the password value.
    */
    public String getPassword() {
        return password;
    }

    /*
    * SET PASSWORD
    * Sets the value for the password.
    */
    public void setPassword(String password) {
        this.password = password;
    }


    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public ArrayList<Data_center> getData_centers() {
        return data_centers;
    }

    public void setData_centers(ArrayList<Data_center> data_centers) {
        this.data_centers = data_centers;
    }

    @Override
    public String toString(){
        return "ID: " + this.id +
                " Name " + this.name +
                " Host " + this.host +
                " Port " + this.port +
                " DbName " + this.dbname +
                " User " + this.username +
                " Pass " +this.password;
    }
    
}
