package EposTables;

/**
* MONUMENT
* This class represents the monument table of the EPOS database.
*/
public class Monument extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the monument table
    private String description;         // Description field of the monument table
    private String inscription;         // Inscription field of the monument table
    private String height;              // Height field of the monument table
    private String foundation;           // Fondation field of the monument table
    private String foundation_depth;     // Fondation_depth field of the monument table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * MONUMENT
    * This is the default constructor for the Monument class.
    */
    public Monument() {
        id = 0;
        description = "";
        inscription = "";
        height = "";
        foundation = "";
        foundation_depth = "";
    }

    /**
    * MONUMENT
    * This constructor for the Monument class accepts values as parameters.
    */
    public Monument(int id, String description, String inscription, String height, String foundation, String foundation_depth) {
        this.id = id;
        this.description = description;
        this.inscription = inscription;
        this.height = height;
        this.foundation = foundation;
        this.foundation_depth = foundation_depth;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET DESCRIPTION
    * Returns the description value.
    */
    public String getDescription() {
        return description;
    }

    /**
    * SET DESCRIPTION
    * Sets the value for the description.
    */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
    * GET INSCRIPTION
    * Returns the inscription value.
    */
    public String getInscription() {
        return inscription;
    }

    /**
    * SET INSCRIPTION
    * Sets the value for the inscription.
    */
    public void setInscription(String inscription) {
        this.inscription = inscription;
    }

    /**
    * GET HEIGHT
    * Returns the height value.
    */
    public String getHeight() {
        return height;
    }

    /**
    * SET HEIGHT
    * Sets the value for the height.
    */
    public void setHeight(String height) {
        this.height = height;
    }

    /**
    * GET FOUNDATION
    * Returns the foUndation value.
    */
    public String getFoundation() {
        return foundation;
    }

    /**
    * SET FOUNDATION
    * Sets the value for the foundation.
    */
    public void setFoundation(String foundation) {
        this.foundation = foundation;
    }

    /**
    * GET FOUNDATION_DEPTH
    * Returns the foundation_depth value.
    */
    public String getFoundation_depth() {
        return foundation_depth;
    }

    /**
    * SET FOUNDATION_DEPTH
    * Sets the value for the foundation_depth.
    */
    public void setFoundation_depth(String foundation_depth) {
        this.foundation_depth = foundation_depth;
    } 
}
