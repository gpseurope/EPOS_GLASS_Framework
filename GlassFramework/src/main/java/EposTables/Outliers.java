package EposTables;

/*
* OUTLIERS
* This class represents the Outliers table of the EPOS product database.
*/
public class Outliers extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the outliers table
    private String type;                // Type field of the outliers table
    private String description;         // Description field of the outliers table
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * OUTLIERS
    * This is the default constructor for the Outliers class.
    */
    public Outliers() {
    }

    /*
    * OUTLIERS
    * This constructor for the Outliers class accepts values as parameters.
    */
    public Outliers(int id, String type, String description) {
        this.id = id;
        this.type = type;
        this.description = description;
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET TYPE
    * Returns the type value.
    */
    public String getType() {
        return type;
    }

    /*
    * SET TYPE
    * Sets the value for the type.
    */
    public void setType(String type) {
        this.type = type;
    }

    /*
    * GET DESCRIPTION
    * Returns the description value.
    */
    public String getDescription() {
        return description;
    }

    /*
    * SET DESCRIPTION
    * Sets the value for the description.
    */
    public void setDescription(String description) {
        this.description = description;
    }
}
