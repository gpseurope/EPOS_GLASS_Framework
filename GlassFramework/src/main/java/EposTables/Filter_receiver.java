package EposTables;

/*
* FILTER_RECEIVER
* This class represents the filter_receiver table of the EPOS database.
*/
public class Filter_receiver extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                             // Id field of the filter_receiver table
    private int id_item;                        // Id_item field of the filter_receiver table
    private Attribute attribute;                // Attribute field of the filter_receiver table
    private Receiver_type receiver_type;        // Receiver_type field of the filter_receiver table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * FILTER_RECEIVER
    * This is the default constructor for the Filter_receiver class.
    */
    public Filter_receiver() {
        id = 0;
        id_item = 0;
        attribute = new Attribute();
        receiver_type = new Receiver_type();
    }

    /*
    * FILTER_RECEIVER
    * This constructor for the Filter_receiver class accepts values as parameters.
    */
    public Filter_receiver(int id, int id_item) {
        this.id = id;
        this.id_item = id_item;
        this.attribute = new Attribute();
        this.receiver_type = new Receiver_type();
    }
    
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET ID_ITEM
    * Returns the id_item value.
    */
    public int getId_Item() {
        return id_item;
    }

    /*
    * SET ID_ITEM
    * Sets the value for the id_item.
    */
    public void setId_Item(int id_item) {
        this.id_item = id_item;
    }

    /*
    * GET ATTRIBUTE
    * Returns the attribute value.
    */
    public Attribute getAttribute() {
        return attribute;
    }

    /*
    * SET ATTRIBUTE
    * Sets the values for the attribute.
    */
    public void setAttribute(int id_attribute, String name) {
        this.attribute.setId(id_attribute);
        this.attribute.setName(name);
    }

    /*
    * GET RECEIVER_TYPE
    * Returns the receiver_type value.
    */
    public Receiver_type getReceiver_type() {
        return receiver_type;
    }

    /*
    * SET ID_RECEIVER_TYPE
    * Sets the value for the id_receiver_type.
    */
    public void setReceiver_type(int id_receiver_type, String name, 
            String igs_defined, String model) {
        this.receiver_type.setId(id_receiver_type);
        this.receiver_type.setName(name);
        this.receiver_type.setIgs_defined(igs_defined);
        this.receiver_type.setModel(model);
    }
}
